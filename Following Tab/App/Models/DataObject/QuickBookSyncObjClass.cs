﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace CloudElement.Models.DataObject
{
    public class SharePointSyncObjClass
    {
        public SharePointSyncObjClass()
        {
            Scope = new SPSScopeClass();
            Criteria = new QBSCriteriaClass();
        }
        public SPSScopeClass Scope { get; set; }
        public QBSCriteriaClass Criteria { get; set; }
    }
    public class QBSScopeClass
    {
        public QBSScopeClass()
        {
            Fields = new QBSFieldsClass();
        }
        public QBSFieldsClass Fields { get; set; }
    }

	public class SPSScopeClass
	{
		public SPSScopeClass()
		{
			Fields = new SPSFieldsClass();
		}
		public SPSFieldsClass Fields { get; set; }
	}
	public class QBSFieldsClass
    {
        public QBSFieldsClass()
        {
            FullName = 1;
            MXMYOBQID = 1;
            MXMYOBSYNC = 1;
            MXMYOBERROR = 1;
        }

        public int FullName  { get; set; }
        [JsonProperty(PropertyName = "Udf/$TAG(3DATAPORT_MYOBID)")]
        public int MXMYOBQID { get; set; }

        [JsonProperty(PropertyName = "Udf/$TAG(3DATAPORT_MYOB_SYNC)")]
        public int MXMYOBSYNC { get; set; }

        [JsonProperty(PropertyName = "Udf/$TAG(3DATAPORT_MYOB_ERROR)")]
        public int MXMYOBERROR { get; set; }
    }

	public class SPSFieldsClass
	{
		public SPSFieldsClass()
		{
			FullName = 1;
		}
		public int FullName { get; set; }
	}
	public class QBSCriteriaClass
    {
        public QBSCriteriaClass()
        {
            SearchQuery = new QBSSearchClass();
        }
        public QBSSearchClass SearchQuery { get; set; }
    }
    public class QBSSearchClass
    {
        public QBSSearchClass()
        {
            Key = new Dictionary<string, string>();
        }  
        public Dictionary<string,string> Key { get; set; }
    }
    public class ResponseAbEntryDataFromMaximizer
    {
        public int Code { get; set; }
        public List<string> Msg { get; set; }
        public AbEntryData AbEntry { get; set; }
    }
    public class AbEntryData
    {
        public List<GenericObject> Data { get; set; }
    }
    public class GenericObject
    {
        public string FullName { get; set; }
        [JsonProperty(PropertyName = "Udf/$TAG(3DATAPORT_MYOBID)")]
        public string MXMYOBQID { get; set; }

        [JsonProperty(PropertyName = "Udf/$TAG(3DATAPORT_MYOB_SYNC)")]
        public List<string> MXMYOBSYNC { get; set; }

        [JsonProperty(PropertyName = "Udf/$TAG(3DATAPORT_MYOB_ERROR)")]
        public string MXMYOBERROR { get; set; }
    }
}