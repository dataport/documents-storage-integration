﻿using Newtonsoft.Json;
using SharePoint.Class;
using System;
using System.Resources;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using SharePoint.Resource;
using System.IO;

namespace CloudElement.Models.DataObject
{
	public class SharePointMapper
	{
		private string QBAppID = null;
		private DataObjectModel Model;
		private string CloudElementProductionURL = "https://console.cloud-elements.com/";
		//private string CloudElmenProductionURL = "https://staging.cloud-elements.com/";
		private string CloudElementStagingURL = "https://staging.cloud-elements.com/";
		private string CloudElementInvoiceURL = "elements/api-v2/hubs/finance/myobinvoiceresource";
		private string CloudElementDocumnetsURL = "elements/api-v2/hubs/documents/folders/contents";
		private string CloudElementDocumnetMetadataURL = "elements/api-v2/hubs/documents/files";
		private string CloudElementsFileMetaDataURL = "elements/api-v2/hubs/documents/files";
		private string CloudElementInvoicePdfURL = "elements/api-v2/hubs/finance/renderAsPdf";
		private string CloudElementOrderURL = "elements/api-v2/hubs/finance/myoborderresource";
		private string CloudElementCustomerPaymentURL = "elements/api-v2/hubs/finance/myobcustomerpaymentresource";
		private string CloudElementCreditRefundURL = "elements/api-v2/hubs/finance/myobcreditrefundresource";
		private string CloudElementCreditSettlementURL = "elements/api-v2/hubs/finance/myobcreditsettlementresource";
        private string CloudElementConfigurationURL = "https://api.cloud-elements.com/elements/api-v2/instances?hydrate=true";

        private string CloudElementPaymentsURL = "elements/api-v2/hubs/finance/payments";
		private string CloudElementSalesReceiptsURL = "elements/api-v2/hubs/finance/sales-receipts";

		private string InvoiceContentDisposition = "inline; filename=Invoice.pdf";
		//private string OrderContentDisposition = "attachment; filename=Order.pdf";
		private string SalesReceiptContentDisposition = "attachment; filename=SalesReceipt.pdf";

		public SharePointMapper()
		{
			QBAppID = GetQBAppID();
		}
		public string GetQBAppID()
		{
			string qbid = "";
			if (ConfigurationManager.AppSettings["ApplicationID"] != null)
			{
				qbid = ConfigurationManager.AppSettings["ApplicationID"] as string;
			}

			return qbid;
		}
		public string GetQBUser()
		{
			string user = "";
			if (ConfigurationManager.AppSettings["CloudElementUser"] != null)
			{
				user = ConfigurationManager.AppSettings["CloudElementUser"] as string;
			}

			return user;
		}
		public bool ChekQBEnvironment()
		{
			string IsProduction = "";
			bool IsQBProduction = true;
			if (ConfigurationManager.AppSettings["CloudElementProduction"] != null)
			{
				IsProduction = ConfigurationManager.AppSettings["CloudElementProduction"] as string;
			}
			if (IsProduction.ToLower() == "false")
			{
				IsQBProduction = false;
			}
			return IsQBProduction;
		}
		public string GetQBOrg()
		{
			string org = "";
			if (ConfigurationManager.AppSettings["CloudElementOrg"] != null)
			{
				org = ConfigurationManager.AppSettings["CloudElementOrg"] as string;
			}

			return org;
		}
		public DataObjectModel Load(DataObjectModel model)
		{
			Model = model;
			GeneratingString();
			PopulateDropDownList();
			return Model;
		}
		public void GeneratingString()
		{
			MetaString strs = new MetaString();
			strs.StrConnectBtn = DataObjectResource.GEN_CONNECT_TXT;
			strs.StrDisConnectBtn = DataObjectResource.GEN_DISCONNECT_TXT;
			strs.StrSync = DataObjectResource.GEN_SYNC_TXT;
			strs.StrSystemErrorTitle = DataObjectResource.SYSTEM_ERROR_TITLE;
			strs.StrSystemErrorMsg = DataObjectResource.SYSTEM_ERROR_MSG;

			JavaScriptSerializer jss = new JavaScriptSerializer();
			Dictionary<string, string> JsStrs = new Dictionary<string, string>();

            JsStrs.Add("Col_Date", DataObjectResource.SP_TYPE_DATE);
            //JsStrs.Add("Col_FileType", DataObjectResource.SP_TYPE_FILE_TYPE);
            JsStrs.Add("Col_FileName", DataObjectResource.SP_TYPE_FILE_NAME);
            //JsStrs.Add("Col_Title", DataObjectResource.SP_TYPE_TITLE);
            JsStrs.Add("Col_Path", DataObjectResource.SP_TYPE_PATH);
            //JsStrs.Add("Col_LastModifiedBy", DataObjectResource.SP_TYPE_LAST_MODIFIED_BY);
            JsStrs.Add("Col_CreatedOn", DataObjectResource.SP_TYPE_CREATED_ON);

            Dictionary<string, string> JSErrorStrs = new Dictionary<string, string>();
			JSErrorStrs.Add("Code:400(3)", DataObjectResource.SYSTEM_ERROR_BAD_REQUEST);
			JSErrorStrs.Add("Code:401(3)", DataObjectResource.SYSTEM_ERROR_UNAUTHORIZED);
			JSErrorStrs.Add("Code:403(3)", DataObjectResource.SYSTEM_ERROR_FORBIDDEN);
			JSErrorStrs.Add("Code:404(3)", DataObjectResource.SYSTEM_ERROR_NOT_FOUND);
			JSErrorStrs.Add("Code:405(3)", DataObjectResource.SYSTEM_ERROR_METHOD_NOT_ALLOWED);
			JSErrorStrs.Add("Code:406(3)", DataObjectResource.SYSTEM_ERROR_NOT_ACCEPTABLE);
			JSErrorStrs.Add("Code:409(3)", DataObjectResource.SYSTEM_ERROR_CONFLICT);
			JSErrorStrs.Add("Code:415(3)", DataObjectResource.SYSTEM_ERROR_UNSUPPORTED_MEDIA_TYPE);
			JSErrorStrs.Add("Code:500(3)", DataObjectResource.SYSTEM_ERROR_SERVER_ERROR);
			JSErrorStrs.Add("Code:502(3)", DataObjectResource.SYSTEM_ERROR_PROVIDER_SERVER_ERROR);

			Model.StrQb = strs;
			Model.JSStrings = jss.Serialize(JsStrs);
			Model.JSErrorStrings = jss.Serialize(JSErrorStrs);
		}
		public void PopulateDropDownList()
		{

			JavaScriptSerializer jss = new JavaScriptSerializer();
			List<DropdownItemsClass> items = new List<DropdownItemsClass>();
			items.Add(new DropdownItemsClass
			{
				Value = "",
				DisplayValue = DataObjectResource.TYPE_ALL,
				IsSelected = true
			});
			items.Add(new DropdownItemsClass
			{
				Value = "1",
				DisplayValue = DataObjectResource.TYPE_INVOICE,
				IsSelected = false
			});
			items.Add(new DropdownItemsClass
			{
				Value = "2",
				DisplayValue = DataObjectResource.TYPE_ORDER,
				IsSelected = false
			});
			//items.Add(new DropdownItemsClass
			//{
			//    Value = "3",
			//    DisplayValue = Resource.DataObjectResource.TYPE_PAYMENT,
			//    IsSelected = false
			//});
			//items.Add(new DropdownItemsClass
			//{
			//    Value = "4",
			//    DisplayValue = Resource.DataObjectResource.TYPE_SALES_RECEIPT,
			//    IsSelected = false
			//});
			items.Add(new DropdownItemsClass
			{
				Value = "5",
				DisplayValue = DataObjectResource.TYPE_CUSTOMER_PAYMENT,
				IsSelected = false
			});
			items.Add(new DropdownItemsClass
			{
				Value = "6",
				DisplayValue = DataObjectResource.TYPE_CREDIT_REFUND,
				IsSelected = false
			});
			items.Add(new DropdownItemsClass
			{
				Value = "7",
				DisplayValue = DataObjectResource.TYPE_CREDIT_SETTLEMENT,
				IsSelected = false
			});


			Model.DropListItems = jss.Serialize(items);
		}

        public JObject GetConfiguration(MaxApiModel MxObj)
        {
            string InstanceToken = "";
            ResponsDataWithOnRequest maxResponse = GetConfigureAndInstance(MxObj);
            JObject response = new JObject();
            if (maxResponse.Code != -2)
            {
                if (HasInstanceToken(maxResponse))
                {
                    var configurationSetting = maxResponse.ConfigurationSetting.Data[0];
                    InstanceToken = configurationSetting.TextValue;

                    JObject requestJObj = new JObject();
                    try
                    {
                        try
                        {
                            string url = CloudElementConfigurationURL;
                            string qbuser = GetQBUser();
                            string qbuorg = GetQBOrg();

                            Dictionary<string, string> header = new Dictionary<string, string>()
                                                            {
                                                                {   "Authorization",
                                                                    "User " + qbuser + ", " + "Organization " + qbuorg + ", " + "Element " + InstanceToken
                                                                }
                                                            };

                            requestJObj.Add(new JProperty("url", url));
                            requestJObj.Add(new JProperty("header", JsonConvert.DeserializeObject<JObject>(JsonConvert.SerializeObject(header))));

                            string jsonRes = WebClient.CreateGetHttpResponse(url, "Content-Type: application/json", header);
                            jsonRes = jsonRes.TrimStart(new char[] { '[' }).TrimEnd(new char[] { ']' });
                            response = JObject.Parse(jsonRes);
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + ";" + ex.StackTrace);
                    }
                }
            }
            return response;
        }

        /*
        public JObject GetDocuments(MaxApiModel MxObj)
		{
			string InstanceToken = "";
			JObject documentsdata = new JObject();
			JObject tt = new JObject();

			ResponsDataWithOnRequest maxResponse = GetConfigureAndInstance(MxObj);
			try
			{
				if (maxResponse.Code != -2)
				{
					if (HasInstanceToken(maxResponse))
					{
						var configurationSetting = maxResponse.ConfigurationSetting.Data[0];
						InstanceToken = configurationSetting.TextValue;
						documentsdata = GetDocumentsData(InstanceToken);
						var spdocids = JsonConvert.DeserializeObject<List<string>>(MxObj.SPDocIds);

						JArray res = new JArray();
						JObject errMsgCE = new JObject();
						try
						{
							foreach (var spdocid in spdocids)
							{
								var documentMetaData = GetDocumentMetaData(InstanceToken, spdocid);
								res.Add(documentMetaData);
							}
						}
						catch (Exception ex)
						{
							errMsgCE.Add(new JProperty("errorMessage", ex.StackTrace));
						}

						tt.Add(new JProperty("response", res));
						tt.Add(new JProperty("error", errMsgCE));

					}
					maxResponse.DocumentsDataNew = tt.ToObject<DocumentsDataSetNew>();
				}
			}
			catch (Exception ex)
			{
				JObject returnObj = new JObject();
				returnObj.Add("error", ex.Message);
				returnObj.Add("Trace", ex.StackTrace);
				return returnObj;
			}
			return JsonConvert.DeserializeObject<JObject>(JsonConvert.SerializeObject(maxResponse));
		}
        */
        public JObject GetDocuments(MaxApiModel MxObj)
        {
            string InstanceToken = "";
            JObject documentsdata = new JObject();
            JObject tt = new JObject();

            ResponsDataWithOnRequest maxResponse = GetConfigureAndInstance(MxObj);
            try
            {
                if (maxResponse.Code != -2)
                {
                    if (HasInstanceToken(maxResponse))
                    {
                        var configurationSetting = maxResponse.ConfigurationSetting.Data[0];
                        InstanceToken = configurationSetting.TextValue;
                        documentsdata = GetDocumentsData(InstanceToken);
                        var spdocids = JsonConvert.DeserializeObject<List<string>>(MxObj.SPDocIds);

                        JArray res = new JArray();
                        JObject errMsgCE = new JObject();
                        try
                        {
                            foreach (var spdocid in spdocids)
                            {
                                try
                                {
                                    var documentMetaData = GetDocumentMetaData(InstanceToken, spdocid, MxObj.Subsite);
                                    res.Add(documentMetaData);
                                }catch (Exception ex)
                                {

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            errMsgCE.Add(new JProperty("errorMessage", ex.StackTrace));
                        }

                        tt.Add(new JProperty("response", res));
                        tt.Add(new JProperty("error", errMsgCE));

                    }
                    maxResponse.DocumentsDataNew = JsonConvert.DeserializeObject<DocumentsDataSetNew>(tt.ToString());
                    //maxResponse.DocumentsDataNew = tt.ToObject<DocumentsDataSetNew>();
                }
            }
            catch (Exception ex)
            {
                JObject returnObj = new JObject();
                returnObj.Add("error", ex.Message);
                returnObj.Add("Trace", ex.StackTrace);
                return returnObj;
            }
            return JsonConvert.DeserializeObject<JObject>(JsonConvert.SerializeObject(maxResponse));
        }

        private bool HasInstanceTokenAndQbID(ResponsDataWithOnRequest maxResponse)
		{
			bool CanloadQuickBooksData = false;
			bool IsSync = false;
			bool HasQbID = false;
			bool HasInstance = false;
			if (maxResponse.AbEntry.Data != null && maxResponse.AbEntry.Data.Count > 0)
			{
				var abEntryItem = maxResponse.AbEntry.Data[0];
				if (abEntryItem.MXMYOBSYNC != null)
				{
					if (abEntryItem.MXMYOBSYNC[0] == "2")
					{
						IsSync = true;
					}
				}
			}
			if (IsSync)
			{
				if (maxResponse.AbEntry.Data.Count > 0)
				{
					var abEntryItem = maxResponse.AbEntry.Data[0];
					if (!String.IsNullOrEmpty(abEntryItem.MXMYOBQID))
					{
						HasQbID = true;
					}
				}
				if (maxResponse.ConfigurationSetting.Data.Count > 0)
				{
					var configurationSetting = maxResponse.ConfigurationSetting.Data[0];
					if (!String.IsNullOrEmpty(configurationSetting.TextValue))
					{
						HasInstance = true;
					}
				}
				if (HasQbID && HasInstance)
				{
					CanloadQuickBooksData = true;
				}
			}
			return CanloadQuickBooksData;
		}

		private bool HasInstanceToken(ResponsDataWithOnRequest maxResponse)
		{
			return true;
		}

		private JObject GetInvoiceData(string InstanceToken, string QbID)
		{
			JObject response = new JObject();
			JArray responseJObj = new JArray();
			JObject requestJObj = new JObject();
			JObject errObj = new JObject();
			try
			{

				try
				{

					string queryString = "filter=Customer/UID eq guid'" + QbID + "'&pageSize=100&page=1";
					string url = (ChekQBEnvironment()) ? CloudElementProductionURL : CloudElementStagingURL;
					string qbuser = GetQBUser();
					string qbuorg = GetQBOrg();

					Dictionary<string, string> header = new Dictionary<string, string>()
			{
				 {"Authorization",
					"User " + qbuser + ", " +
					"Organization " + qbuorg + ", " +
					"Element " + InstanceToken}
			};

					url += CloudElementInvoiceURL + "?" + queryString;
					requestJObj.Add(new JProperty("url", url));
					requestJObj.Add(new JProperty("header", JsonConvert.DeserializeObject<JObject>(JsonConvert.SerializeObject(header))));

					//var response = JsonConvert.DeserializeObject<List<InvoiceDataClass>>(WebClient.CreateGetHttpResponse(url, "Content-Type: application/json", header));
					responseJObj = JArray.Parse(WebClient.CreateGetHttpResponse(url, "Content-Type: application/json", header));

				}
				catch (Exception ex)
				{
					errObj.Add(new JProperty("message", ex.Message));
					errObj.Add(new JProperty("trace", ex.StackTrace));
				}
				//response.Add(new JProperty("request", requestJObj));
				response.Add(new JProperty("response", responseJObj));
				response.Add(new JProperty("error", errObj));
				return response;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message + ";" + ex.StackTrace);
			}
		}
		private JObject GetDocumentsData(string InstanceToken)
		{
			JObject response = new JObject();
			JArray responseJObj = new JArray();
			JObject responseTest = new JObject();
			JObject requestJObj = new JObject();
			JObject errObj = new JObject();
			try
			{
				try
				{
					string queryString = "path=%2F";
					string url = (ChekQBEnvironment()) ? CloudElementProductionURL : CloudElementStagingURL;
					string qbuser = GetQBUser();
					string qbuorg = GetQBOrg();

					Dictionary<string, string> header = new Dictionary<string, string>()
															{
																{   "Authorization",
																	"User " + qbuser + ", " + "Organization " + qbuorg + ", " + "Element " + InstanceToken
																}
															};

					url += CloudElementDocumnetsURL + "?" + queryString;
					requestJObj.Add(new JProperty("url", url));
					requestJObj.Add(new JProperty("header", JsonConvert.DeserializeObject<JObject>(JsonConvert.SerializeObject(header))));

					responseJObj = JArray.Parse(WebClient.CreateGetHttpResponse(url, "Content-Type: application/json", header));
				}
				catch (Exception ex)
				{
					errObj.Add(new JProperty("message", ex.Message));
					errObj.Add(new JProperty("trace", ex.StackTrace));
				}
				response.Add(new JProperty("response", responseJObj));
				response.Add(new JProperty("error", errObj));
				return response;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message + ";" + ex.StackTrace);
			}
		}
        /*
		private JObject GetDocumentMetaData(string InstanceToken, string DocumentKey)
		{
			JObject response = new JObject();
			JObject requestJObj = new JObject();
			try
			{
				try
				{
					string queryString = DocumentKey + "/metadata";
					string url = (ChekQBEnvironment()) ? CloudElmenProductionURL : CloudElmenStagingURL;
					string qbuser = GetQBUser();
					string qbuorg = GetQBOrg();

					Dictionary<string, string> header = new Dictionary<string, string>()
															{
																{   "Authorization",
																	"User " + qbuser + ", " + "Organization " + qbuorg + ", " + "Element " + InstanceToken
																}
															};

					url += CloudElmentDocumnetMetadataURL + "/" + queryString;
					requestJObj.Add(new JProperty("url", url));
					requestJObj.Add(new JProperty("header", JsonConvert.DeserializeObject<JObject>(JsonConvert.SerializeObject(header))));


					response = JObject.Parse(WebClient.CreateGetHttpResponse(url, "Content-Type: application/json", header));
				}
				catch (Exception ex)
				{
					throw ex;
				}
				return response;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message + ";" + ex.StackTrace);
			}
		}
        */

        private JObject GetDocumentMetaData(string InstanceToken, string DocumentKey, string subsite)
        {
            JObject response = new JObject();
            JObject requestJObj = new JObject();
            try
            {
                try
                {
                    string queryString = DocumentKey + "/metadata";
                    string url = (ChekQBEnvironment()) ? CloudElementProductionURL : CloudElementStagingURL;
                    string qbuser = GetQBUser();
                    string qbuorg = GetQBOrg();

                    Dictionary<string, string> header = new Dictionary<string, string>();
                    if (subsite == "")
                    {
                        header = new Dictionary<string, string>()
                                                            {
                                                                {   "Authorization",
                                                                    "User " + qbuser + ", " + "Organization " + qbuorg + ", " + "Element " + InstanceToken
                                                                }
                                                            };
                    }else
                    {
                        header = new Dictionary<string, string>()
                                                            {
                                                                {   "Authorization",
                                                                    "User " + qbuser + ", " + "Organization " + qbuorg + ", " + "Element " + InstanceToken
                                                                },
                                                                {   "Subsite",
                                                                    subsite
                                                                }
                                                            };
                    }
                    url += CloudElementDocumnetMetadataURL + "/" + queryString;
                    requestJObj.Add(new JProperty("url", url));
                    requestJObj.Add(new JProperty("header", JsonConvert.DeserializeObject<JObject>(JsonConvert.SerializeObject(header))));
                    response = JObject.Parse(WebClient.CreateGetHttpResponse(url, "Content-Type: application/json", header));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + ";" + ex.StackTrace);
            }
        }

        private JObject GetFileMetaData(string InstanceToken, string refId)
		{
			JObject response = new JObject();
			JArray responseJObj = new JArray();
			JObject responseTest = new JObject();
			JObject requestJObj = new JObject();
			JObject errObj = new JObject();
			try
			{
				try
				{
					string url = (ChekQBEnvironment()) ? CloudElementProductionURL : CloudElementStagingURL;
					string qbuser = GetQBUser();
					string qbuorg = GetQBOrg();

					Dictionary<string, string> header = new Dictionary<string, string>()
															{
																{   "Authorization",
																	"User " + qbuser + ", " + "Organization " + qbuorg + ", " + "Element " + InstanceToken
																}
															};

					url += CloudElementsFileMetaDataURL + "/" + refId + "/metadata";
					requestJObj.Add(new JProperty("url", url));
					requestJObj.Add(new JProperty("header", JsonConvert.DeserializeObject<JObject>(JsonConvert.SerializeObject(header))));

					responseJObj = JArray.Parse(WebClient.CreateGetHttpResponse(url, "Content-Type: application/json", header));
				}
				catch (Exception ex)
				{
					errObj.Add(new JProperty("message", ex.Message));
					errObj.Add(new JProperty("trace", ex.StackTrace));
				}
				response.Add(new JProperty("response", responseJObj));
				response.Add(new JProperty("error", errObj));
				return response;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message + ";" + ex.StackTrace);
			}
		}

		private JObject GetOrderData(string InstanceToken, string QbID)
		{
			JObject response = new JObject();
			JArray responseJObj = new JArray();
			JObject requestJObj = new JObject();
			JObject errObj = new JObject();
			try
			{

				try
				{

					string queryString = "filter=Customer/UID eq guid'" + QbID + "'&pageSize=100&page=1";
					string url = (ChekQBEnvironment()) ? CloudElementProductionURL : CloudElementStagingURL;
					string qbuser = GetQBUser();
					string qbuorg = GetQBOrg();

					Dictionary<string, string> header = new Dictionary<string, string>()
			{
				 {"Authorization",
					"User " + qbuser + ", " +
					"Organization " + qbuorg + ", " +
					"Element " + InstanceToken}
			};

					url += CloudElementOrderURL + "?" + queryString;
					requestJObj.Add(new JProperty("url", url));
					requestJObj.Add(new JProperty("header", JsonConvert.DeserializeObject<JObject>(JsonConvert.SerializeObject(header))));

					//var response = JsonConvert.DeserializeObject<List<InvoiceDataClass>>(WebClient.CreateGetHttpResponse(url, "Content-Type: application/json", header));
					responseJObj = JArray.Parse(WebClient.CreateGetHttpResponse(url, "Content-Type: application/json", header));

				}
				catch (Exception ex)
				{
					errObj.Add(new JProperty("message", ex.Message));
					errObj.Add(new JProperty("trace", ex.StackTrace));
				}
				//response.Add(new JProperty("request", requestJObj));
				response.Add(new JProperty("response", responseJObj));
				response.Add(new JProperty("error", errObj));
				return response;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message + ";" + ex.StackTrace);
			}
		}
		private JObject GetCustomerPaymentData(string InstanceToken, string QbID)
		{
			JObject response = new JObject();
			JArray responseJObj = new JArray();
			JObject requestJObj = new JObject();
			JObject errObj = new JObject();
			try
			{

				try
				{

					string queryString = "filter=Customer/UID eq guid'" + QbID + "'&pageSize=100&page=1";
					string url = (ChekQBEnvironment()) ? CloudElementProductionURL : CloudElementStagingURL;
					string qbuser = GetQBUser();
					string qbuorg = GetQBOrg();

					Dictionary<string, string> header = new Dictionary<string, string>()
			{
				 {"Authorization",
					"User " + qbuser + ", " +
					"Organization " + qbuorg + ", " +
					"Element " + InstanceToken}
			};

					url += CloudElementCustomerPaymentURL + "?" + queryString;
					requestJObj.Add(new JProperty("url", url));
					requestJObj.Add(new JProperty("header", JsonConvert.DeserializeObject<JObject>(JsonConvert.SerializeObject(header))));

					//var response = JsonConvert.DeserializeObject<List<InvoiceDataClass>>(WebClient.CreateGetHttpResponse(url, "Content-Type: application/json", header));
					responseJObj = JArray.Parse(WebClient.CreateGetHttpResponse(url, "Content-Type: application/json", header));

				}
				catch (Exception ex)
				{
					errObj.Add(new JProperty("message", ex.Message));
					errObj.Add(new JProperty("trace", ex.StackTrace));
				}
				//response.Add(new JProperty("request", requestJObj));
				response.Add(new JProperty("response", responseJObj));
				response.Add(new JProperty("error", errObj));
				return response;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message + ";" + ex.StackTrace);
			}
		}
		private JObject GetCreditRefundData(string InstanceToken, string QbID)
		{
			JObject response = new JObject();
			JArray responseJObj = new JArray();
			JObject requestJObj = new JObject();
			JObject errObj = new JObject();
			try
			{

				try
				{

					string queryString = "filter=Customer/UID eq guid'" + QbID + "'&pageSize=100&page=1";
					string url = (ChekQBEnvironment()) ? CloudElementProductionURL : CloudElementStagingURL;
					string qbuser = GetQBUser();
					string qbuorg = GetQBOrg();

					Dictionary<string, string> header = new Dictionary<string, string>()
			{
				 {"Authorization",
					"User " + qbuser + ", " +
					"Organization " + qbuorg + ", " +
					"Element " + InstanceToken}
			};

					url += CloudElementCreditRefundURL + "?" + queryString;
					requestJObj.Add(new JProperty("url", url));
					requestJObj.Add(new JProperty("header", JsonConvert.DeserializeObject<JObject>(JsonConvert.SerializeObject(header))));

					//var response = JsonConvert.DeserializeObject<List<InvoiceDataClass>>(WebClient.CreateGetHttpResponse(url, "Content-Type: application/json", header));
					responseJObj = JArray.Parse(WebClient.CreateGetHttpResponse(url, "Content-Type: application/json", header));

				}
				catch (Exception ex)
				{
					errObj.Add(new JProperty("message", ex.Message));
					errObj.Add(new JProperty("trace", ex.StackTrace));
				}
				//response.Add(new JProperty("request", requestJObj));
				response.Add(new JProperty("response", responseJObj));
				response.Add(new JProperty("error", errObj));
				return response;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message + ";" + ex.StackTrace);
			}
		}
		private JObject GetCreditSettlementData(string InstanceToken, string QbID)
		{
			JObject response = new JObject();
			JArray responseJObj = new JArray();
			JObject requestJObj = new JObject();
			JObject errObj = new JObject();
			try
			{

				try
				{

					string queryString = "filter=Customer/UID eq guid'" + QbID + "'&pageSize=100&page=1";
					string url = (ChekQBEnvironment()) ? CloudElementProductionURL : CloudElementStagingURL;
					string qbuser = GetQBUser();
					string qbuorg = GetQBOrg();

					Dictionary<string, string> header = new Dictionary<string, string>()
			{
				 {"Authorization",
					"User " + qbuser + ", " +
					"Organization " + qbuorg + ", " +
					"Element " + InstanceToken}
			};

					url += CloudElementCreditSettlementURL + "?" + queryString;
					requestJObj.Add(new JProperty("url", url));
					requestJObj.Add(new JProperty("header", JsonConvert.DeserializeObject<JObject>(JsonConvert.SerializeObject(header))));

					//var response = JsonConvert.DeserializeObject<List<InvoiceDataClass>>(WebClient.CreateGetHttpResponse(url, "Content-Type: application/json", header));
					responseJObj = JArray.Parse(WebClient.CreateGetHttpResponse(url, "Content-Type: application/json", header));

				}
				catch (Exception ex)
				{
					errObj.Add(new JProperty("message", ex.Message));
					errObj.Add(new JProperty("trace", ex.StackTrace));
				}
				//response.Add(new JProperty("request", requestJObj));
				response.Add(new JProperty("response", responseJObj));
				response.Add(new JProperty("error", errObj));
				return response;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message + ";" + ex.StackTrace);
			}
		}
		private JObject GetPaymentsData(string InstanceToken, string QbID)
		{
			JObject requestJObj = new JObject();
			JArray responseJObj = new JArray();
			JObject errorJObj = new JObject();
			JObject response = new JObject();
			try
			{
				string queryString = "where=customerRef='" + QbID + "'";
				string url = (ChekQBEnvironment()) ? CloudElementProductionURL : CloudElementStagingURL;
				string qbuser = GetQBUser();
				string qbuorg = GetQBOrg();

				Dictionary<string, string> header = new Dictionary<string, string>()
				{
					 {"Authorization",
						"User " + qbuser + ", " +
						"Organization " + qbuorg + ", " +
						"Element " + InstanceToken}
				};

				url += CloudElementPaymentsURL + "?" + queryString;

				requestJObj.Add(new JProperty("url", url));
				requestJObj.Add(new JProperty("header", JArray.FromObject(header)));
				//var response = JsonConvert.DeserializeObject<List<PaymentsDataClass>>(WebClient.CreateGetHttpResponse(url, "Content-Type: application/json", header));
				responseJObj = JArray.Parse(WebClient.CreateGetHttpResponse(url, "Content-Type: application/json", header));

			}
			catch (Exception ex)
			{
				errorJObj.Add(new JProperty("Msg", ex.Message));
				errorJObj.Add(new JProperty("Trace", ex.StackTrace));
			}
			//response.Add(new JProperty("request", requestJObj));
			response.Add(new JProperty("response", responseJObj));
			response.Add(new JProperty("error", errorJObj));
			return response;
		}
		private List<SalesReceiptsDataClass> GetSalesReceiptData(string InstanceToken, string QbID)
		{
			string queryString = "where=customerRef='" + QbID + "'";
			string url = (ChekQBEnvironment()) ? CloudElementProductionURL : CloudElementStagingURL;
			string qbuser = GetQBUser();
			string qbuorg = GetQBOrg();

			Dictionary<string, string> header = new Dictionary<string, string>()
			{
				 {"Authorization",
					"User " + qbuser + ", " +
					"Organization " + qbuorg + ", " +
					"Element " + InstanceToken}
			};

			url += CloudElementSalesReceiptsURL + "?" + queryString;

			var response = JsonConvert.DeserializeObject<List<SalesReceiptsDataClass>>(WebClient.CreateGetHttpResponse(url, "Content-Type: application/json", header));
			if (response == null)
			{
				response = new List<SalesReceiptsDataClass>();
			}
			return response;
		}
		public ResponsDataWithOnRequest GetConfigureAndInstance(MaxApiModel MxObj)
		{
			var requestobj = "";

			var obj = new RequestObj.RequestTokenObj();
			obj.ConfigurationSetting = RequestConfiguSettingObj();
			obj.Token = MxObj.MXAPIToken;
			requestobj = JsonConvert.SerializeObject(obj);
			var url = MxObj.MXAPIURL + "/Json/Read";
			var responObj = JsonConvert.DeserializeObject<ResponsDataWithOnRequest>(WebClient.CreatePostHttpResponse(url, null, null, requestobj));

			return responObj;
		}
		private string GetCloudElementInstance(MaxApiModel MxObj)
		{
			string InstanceToken = "";
			var requestobj = "";

			var obj = new RequestObj.RequestTokenObj();


			obj.ConfigurationSetting = RequestConfiguSettingObj();

			obj.Token = MxObj.MXAPIToken;

			requestobj = JsonConvert.SerializeObject(obj);
			var url = MxObj.MXAPIURL + "/Json/Read";
			var responObj = JsonConvert.DeserializeObject<ResponseConfigureSettingDataFromMaximizer>(WebClient.CreatePostHttpResponse(url, null, null, requestobj));
			if (responObj.Code == 0)
			{
				if (responObj.ConfigurationSetting != null)
				{
					if (responObj.ConfigurationSetting.Data.Count > 0)
					{
						InstanceToken = responObj.ConfigurationSetting.Data[0].TextValue;
					}
				}
			}
			return InstanceToken;
		}
		private ConfigurationObjClass RequestConfiguSettingObj()
		{
			var config = new ConfigurationObjClass();
			var scope = new CFScopeClass();
			var fields = new CFFieldsClass();
			var criteria = new CFCriteriaClass();
			var search = new CFSearchClass();
			var searchCode1Option = new SearchCode1Option();
			var searchCode2Option = new SearchCode2Option();
			var searchCode3Option = new SearchCode3Option();
			var searchCode4Option = new SearchCode4Option();

			searchCode1Option.Code1.Add("$EQ", QBAppID);
			searchCode3Option.Code3.Add("$EQ", "SHAREPOINT_INT");
			searchCode4Option.Code4.Add("$EQ", "MX_SHAREPOINT_TOKEN");


			search.AND.Add(searchCode3Option);

			search.AND.Add(searchCode4Option);

			criteria.SearchQuery = search;

			scope.Fields = fields;

			config.Scope = scope;
			config.Criteria = criteria;

			return config;
		}
		private SharePointSyncObjClass RequestQuickBookSyncObj(string parentKey)
		{
			var config = new SharePointSyncObjClass();
			var scope = new SPSScopeClass();
			var fields = new SPSFieldsClass();
			var criteria = new QBSCriteriaClass();
			var search = new QBSSearchClass();
			search.Key.Add("$EQ", parentKey);

			criteria.SearchQuery = search;

			scope.Fields = fields;

			config.Scope = scope;
			config.Criteria = criteria;

			return config;
		}
		public void DownLoadQuickTransaction(MaxApiModel MxObj)
		{
			DownloadSP(MxObj);
		}

		private void QuickBookInvoicePDF(MaxApiModel MxObj)
		{
			string InstanceToken = "";
			string url = (ChekQBEnvironment()) ? CloudElementProductionURL : CloudElementStagingURL;
			InstanceToken = GetCloudElementInstance(MxObj);

			string qbuser = GetQBUser();
			string qbuorg = GetQBOrg();

			Dictionary<string, string> header = new Dictionary<string, string>()
			{
				  {"Authorization",
					"User " + qbuser + ", " +
					"Organization " + qbuorg + ", " +
					"Element " + InstanceToken},
			};

			url += CloudElementInvoicePdfURL + "/" + MxObj.DocumentID;
			if (!String.IsNullOrEmpty(MxObj.DocumentNumber))
				InvoiceContentDisposition = InvoiceContentDisposition.Replace("Invoice.pdf", "Invoice_" + MxObj.DocumentNumber + ".pdf");
			WebClient.CreateDownLoadPDF(url, "application/pdf", header, InvoiceContentDisposition);
		}
		private void DownloadSP(MaxApiModel MxObj)
		{
			string InstanceToken = "";
			string url = (ChekQBEnvironment()) ? CloudElementProductionURL : CloudElementStagingURL;
			InstanceToken = GetCloudElementInstance(MxObj);

			string qbuser = GetQBUser();
			string qbuorg = GetQBOrg();

            Dictionary<string, string> header = new Dictionary<string, string>();

            if (MxObj.Subsite == "") {
                header = new Dictionary<string, string>()
                {
				    {"Authorization", "User "+ qbuser +", " + "Organization " + qbuorg + ", " + "Element " + InstanceToken}
			    };
            }else
            {
                header = new Dictionary<string, string>()
                {
                    {"Authorization", "User "+ qbuser +", " + "Organization " + qbuorg + ", " + "Element " + InstanceToken},
                    {"Subsite", MxObj.Subsite}
                };
            }

			url += CloudElementDocumnetMetadataURL + "/" + MxObj.DocumentNumber;
			WebClient.DownloadFile(url, "application/json", header, MxObj.DocumentID);
		}
		private void QuickBookOrderPDF(MaxApiModel MxObj)
		{
			string InstanceToken = "";
			string url = (ChekQBEnvironment()) ? CloudElementProductionURL : CloudElementStagingURL;
			InstanceToken = GetCloudElementInstance(MxObj);

			string qbuser = GetQBUser();
			string qbuorg = GetQBOrg();

			Dictionary<string, string> header = new Dictionary<string, string>()
			{
				  {"Authorization",
					"User " + qbuser + ", " +
					"Organization " + qbuorg + ", " +
					"Element " + InstanceToken}
			};

			url += CloudElementInvoicePdfURL + "/" + MxObj.DocumentID;
			url += "?order=true";
			if (!String.IsNullOrEmpty(MxObj.DocumentNumber))
				InvoiceContentDisposition = InvoiceContentDisposition.Replace("Invoice.pdf", "Invoice_" + MxObj.DocumentNumber + ".pdf");
			WebClient.CreateDownLoadPDF(url, "application/pdf", header, InvoiceContentDisposition);
		}
		private void QuickBookSalesReceiptPDF(MaxApiModel MxObj)
		{
			string InstanceToken = "";
			string url = (ChekQBEnvironment()) ? CloudElementProductionURL : CloudElementStagingURL;
			InstanceToken = GetCloudElementInstance(MxObj);

			string qbuser = GetQBUser();
			string qbuorg = GetQBOrg();

			Dictionary<string, string> header = new Dictionary<string, string>()
			{
				 {"Authorization",
					"User " + qbuser + ", " +
					"Organization " + qbuorg + ", " +
					"Element " + InstanceToken}
			};

			url += CloudElementSalesReceiptsURL + "/" + MxObj.DocumentID;

			WebClient.CreateDownLoadPDF(url, "application/pdf", header, SalesReceiptContentDisposition);
		}
	}
}