﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloudElement.Models.DataObject
{
    public class DataObjectModel
    {
        public MetaString StrQb { get; set; }
        public string JSStrings { get; set; }
        public string JSErrorStrings { get; set; }
        public string DropListItems { get; set; }
    }
    public class MetaString
    {
        public string StrConnectBtn { get; set; }
        public string StrDisConnectBtn { get; set; }
        public string StrSync { get; set; }
        public string StrSystemErrorTitle { get; set; }
        public string StrSystemErrorMsg { get; set; }

    }
    public class DropdownItemsClass
    {
        public string Value { get; set; }
        public string DisplayValue { get; set; }
        public bool IsSelected { get; set; }
    }
    public class ResponsDataWithOnRequest
    {
        public ResponsDataWithOnRequest()
        {
            Code = 0;
            AbEntry = new AbEntryData();
            ConfigurationSetting = new ConfigurationData();
            //InvoiceData = new List<InvoiceDataClass>();
            //OrderData = new List<OrderDataClass>();
            //CustomerPaymentData = new List<CustomerPaymentDataClass>();
            //CreditRefundData = new List<CreditRefundDataClass>();
            //CreditSettlementData = new List<CreditSettlementDataClass>();
            InvoiceData = new InvoiceDataSet();
            OrderData = new OrderDataSet();
            CustomerPaymentData = new CustomerPaymentDataSet();
            CreditRefundData = new CreditRefundDataSet();
            CreditSettlementData = new CreditSettlementDataSet();
			DocumentsData = new DocumentsDataSet();
        }
        public int Code { get; set; }
        public AbEntryData AbEntry { get; set; }
        public ConfigurationData ConfigurationSetting { get; set; }
        public InvoiceDataSet InvoiceData { get; set; }
        public OrderDataSet OrderData { get; set; }
        public CustomerPaymentDataSet CustomerPaymentData { get; set; }
        public CreditRefundDataSet CreditRefundData { get; set; }
        public CreditSettlementDataSet CreditSettlementData { get; set; }
		public DocumentsDataSet DocumentsData { get; set; }
		public DocumentsDataSetNew DocumentsDataNew { get; set; }
	}
    public class MaxApiModel
    {
        public MaxApiModel()
        {
            MXAPIURL = "";
            MXAPIToken = "";
            SPDocIds = "";
            DocumentID = "";
            DocumentNumber = "";
            CurrentParentKey = "";
            TypeNumber = "";
            Subsite = "";
        }
        public string MXAPIURL { get; set; }
        public string MXAPIToken { get; set; }
        public string SPDocIds { get; set; }
        public string DocumentID { get; set; }
        public string DocumentNumber { get; set; }
        public string CurrentParentKey { get; set; }
        public string TypeNumber { get; set; }
        public string Subsite { get; set; }
    }
    public class RequestObj
    {
        public class RequestTokenObj
        {
            public RequestTokenObj()
            {
                Token = "";
                ConfigurationSetting = new ConfigurationObjClass();
                AbEntry = new SharePointSyncObjClass();
            }
            public string Token { get; set; }
            public ConfigurationObjClass ConfigurationSetting { get; set; }
            public SharePointSyncObjClass AbEntry { get; set; }
        }
       
    }
    public class ResponseDataFromCloudElement
    {
        public int Code { get; set; }
        public List<string> Msg { get; set; }
        public ConfigurationData ConfigurationSetting { get; set; }
    }
    public class InvoiceObjClass
    {
        public List<InvoiceDataClass> Invoice { get; set; }
    }
    public class InvoiceDataClass
    {
        public InvoiceDataClass()
        {
            type = "Invoice";
            typeNumber = 1;
            dueDate = "";
            creationDate = "";
        }
        public int typeNumber { get; set; }
        public string systemId { get; set; }
        public string docNumber { get; set; }
        public string type { get; set; }
        public string creationDate { get; set; }
        public string dueDate { get; set; }
        public double balance { get; set; }
        public double totalAmt { get; set; }
        //public CurrencyClass currencyRef { get; set; }
    }
    public class OrderDataClass
    {
        public OrderDataClass()
        {
            type = "Order";
            balance = 0;
            typeNumber = 2;
            dueDate = "";
            creationDate = "";
        }
        public int typeNumber { get; set; }
        public string systemId { get; set; }
        public string docNumber { get; set; }
        public string type { get; set; }
        public string creationDate { get; set; }
        public string dueDate
        {
            get
            {
                return this.creationDate;
            }
            set
            {
                value = this.creationDate;
            }
        }
        public double balance { get; set; }
        public double totalAmt { get; set; }
        //public CurrencyClass currencyRef { get; set; }
    }
    public class PaymentsDataClass
    {
        public PaymentsDataClass()
        {
            type = "Payment";
            balance = 0;
            docNumber = "";
            typeNumber = 3;
            dueDate = "";
            creationDate = "";
        }
        public int typeNumber { get; set; }
        public string systemId { get; set; }
        public string docNumber { get; set; }
        public string type { get; set; }
        [JsonProperty(PropertyName = "txnDate")]
        public string creationDate { get; set; }
        public string dueDate {
            get
            {
                return this.creationDate;
            }
            set
            {
                value = this.creationDate;
            }
        }

        public double balance { get; set; }
        public double totalAmt { get; set; }
        public CurrencyClass currencyRef { get; set; }
    }
    public class SalesReceiptsDataClass
    {
        public SalesReceiptsDataClass()
        {
            type = "Sales Receipt";
            balance = 0;
            dueDate = "";
            typeNumber = 4;
            creationDate = "";
        }
        public int typeNumber { get; set; }
        public string systemId { get; set; }
        public string docNumber { get; set; }
        public string type { get; set; }
        [JsonProperty(PropertyName = "txnDate")]
        public string creationDate { get; set; }
        public string dueDate { get; set; }
        public double balance { get; set; }
        public double totalAmt { get; set; }
        public CurrencyClass currencyRef { get; set; }
    }
	public class CustomerPaymentDataClass
	{
		public CustomerPaymentDataClass()
		{
			type = "Customer Payment";
			balance = "";
			dueDate = "";
			typeNumber = 5;
			creationDate = "";
		}
		public int typeNumber { get; set; }
		public string systemId { get; set; }
		public string docNumber { get; set; }
		public string type { get; set; }
		public string creationDate { get; set; }
		public string dueDate { get; set; }
		public string balance { get; set; }
		public double? totalAmt { get; set; }
	}
	public class CreditRefundDataClass
	{
		public CreditRefundDataClass()
		{
			type = "Credit Refund";
			balance = "";
			dueDate = "";
			typeNumber = 6;
			creationDate = "";
		}
		public int typeNumber { get; set; }
		public string systemId { get; set; }
		public string docNumber { get; set; }
		public string type { get; set; }
		public string creationDate { get; set; }
		public string dueDate { get; set; }
		public string balance { get; set; }
		public double? totalAmt { get; set; }
	}
	public class CreditSettlementDataClass
	{
		public CreditSettlementDataClass()
		{
			type = "Credit Settlement";
			balance = "";
			dueDate = "";
			typeNumber = 7;
			creationDate = "";
		}
		public int typeNumber { get; set; }
		public string systemId { get; set; }
		public string docNumber { get; set; }
		public string type { get; set; }
		public string creationDate { get; set; }
		public string dueDate { get; set; }
		public string balance { get; set; }
		public double? totalAmt { get; set; }
	}
	public class DocumentsDataClass
	{
		public string path { get; set; }
		public string createdDate { get; set; }
		public int size { get; set; }
		public string parentFolderId { get; set; }
		public string name { get; set; }
		public string modifiedDate { get; set; }
		public string id { get; set; }
		public string refId { get; set; }
		public Boolean directory { get; set; }
		public JObject properties { get; set; }

	}
	public class DocumentMetaDataClass
	{
		public string path { get; set; }
		public string createdDate { get; set; }
		public int size { get; set; }
		public string name { get; set; }
		public string modifiedDate { get; set; }
		public string id { get; set; }
		public string refId { get; set; }
		public Boolean directory { get; set; }
		public JObject properties { get; set; }

	}
	public class CurrencyClass
    {
        public string value { get; set; }
    }

	public class TermClass
	{
		public string DueDate { get; set; }

	}
    public class InvoiceDataSet
    {
        public JObject request { set; get; }
        public List<InvoiceDataClass> response { set; get; }
        public JObject error { set; get; }
    }
    public class OrderDataSet
    {
        public JObject request { set; get; }
        public List<OrderDataClass> response { set; get; }
        public JObject error { set; get; }
    }
    public class CustomerPaymentDataSet
    {
        public JObject request { set; get; }
        public List<CustomerPaymentDataClass> response { set; get; }
        public JObject error { set; get; }
    }
    public class CreditRefundDataSet
    {
        public JObject request { set; get; }
        public List<CreditRefundDataClass> response { set; get; }
        public JObject error { set; get; }
    }
    public class CreditSettlementDataSet
    {
        public JObject request { set; get; }
        public List<CreditSettlementDataClass> response { set; get; }
        public JObject error { set; get; }
    }
	public class DocumentsDataSet
	{
		public JObject request { set; get; }
		public List<DocumentsDataClass> response { set; get; }
		public JObject error { set; get; }
	}
	public class DocumentsDataSetNew
	{
		public JObject request { set; get; }
		public List<DocumentMetaDataClass> response { set; get; }
		public JObject error { set; get; }
	}
}