﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloudElement.Models.DataObject
{
    public class DataObjectModel
    {
        public MetaString MetaStr { get; set; }
        public string JSStrings { get; set; }
        public string JSErrorStrings { get; set; }
    }
    public class MetaString
    {
        public string StrConnectBtn { get; set; }
        public string StrDisConnectBtn { get; set; }
        public string StrSync { get; set; }
        public string StrSystemErrorTitle { get; set; }
        public string StrSystemErrorMsg { get; set; }

    }
    public class ResponsDataWithOnRequest
    {
        public ResponsDataWithOnRequest()
        {
            Code = 0;
            AbEntry = new AbEntryData();
            ConfigurationSetting = new ConfigurationData();
            DocumentsData = new DocumentsDataSet();
        }
        public int Code { get; set; }
        public 
            AbEntryData AbEntry { get; set; }
        public ConfigurationData ConfigurationSetting { get; set; }
        public DocumentsDataSet DocumentsData { get; set; }
        public DocumentsDataSetNew DocumentsDataNew { get; set; }
    }
    public class MaxApiModel
    {
        public MaxApiModel()
        {
            MXAPIURL = "";
            MXAPIToken = "";
            SPDocIds = "";
            DocumentID = "";
            DocumentNumber = "";
            CurrentParentKey = "";
            TypeNumber = "";
            Subsite = "";
            CloudDocumentName = "";
        }
        public string MXAPIURL { get; set; }
        public string MXAPIToken { get; set; }
        public string SPDocIds { get; set; }
        public string DocumentID { get; set; }
        public string DocumentNumber { get; set; }
        public string CurrentParentKey { get; set; }
        public string TypeNumber { get; set; }
        public string Subsite { get; set; }
        public string CloudDocumentName { get; set; }
    }
    public class RequestObj
    {
        public class RequestTokenObj
        {
            public RequestTokenObj()
            {
                Token = "";
                ConfigurationSetting = new ConfigurationObjClass();
            }
            public string Token { get; set; }
            public ConfigurationObjClass ConfigurationSetting { get; set; }
        }

    }
    public class ResponseDataFromCloudElement
    {
        public int Code { get; set; }
        public List<string> Msg { get; set; }
        public ConfigurationData ConfigurationSetting { get; set; }
    }
    public class DocumentsDataClass
    {
        public string path { get; set; }
        public string createdDate { get; set; }
        public int size { get; set; }
        public string parentFolderId { get; set; }
        public string name { get; set; }
        public string modifiedDate { get; set; }
        public string id { get; set; }
        public string refId { get; set; }
        public Boolean directory { get; set; }
        public JObject properties { get; set; }

    }
    public class DocumentMetaDataClass
    {
        public string path { get; set; }
        public string createdDate { get; set; }
        public int size { get; set; }
        public string name { get; set; }
        public string modifiedDate { get; set; }
        public string id { get; set; }
        public string refId { get; set; }
        public Boolean directory { get; set; }
        public JObject properties { get; set; }
        public string error { get; set; }
        public string statusCode { get; set; }
    }
    public class CurrencyClass
    {
        public string value { get; set; }
    }

    public class TermClass
    {
        public string DueDate { get; set; }

    }
    public class DocumentsDataSet
    {
        public JObject request { set; get; }
        public List<DocumentsDataClass> response { set; get; }
        public JObject error { set; get; }
    }
    public class DocumentsDataSetNew
    {
        public JObject request { set; get; }
        public List<DocumentMetaDataClass> response { set; get; }
        public JObject error { set; get; }
    }
}