﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloudElement.Models.DataObject
{

    public class ConfigurationObjClass
    {
        public ConfigurationObjClass()
        {
            Scope = new CFScopeClass();
            Criteria = new CFCriteriaClass();
        }
        public CFScopeClass Scope { get; set; }
        public CFCriteriaClass Criteria { get; set; }
    }
    public class CFScopeClass
    {
        public CFScopeClass()
        {
            Fields = new CFFieldsClass();
        }
        public CFFieldsClass Fields { get; set; }
    }
    public class CFFieldsClass
    {
        public CFFieldsClass()
        {
            TextValue = 1;
            Description = 1;
        }
        public int TextValue { get; set; }
        public int Description { get; set; }
    }
    public class CFCriteriaClass
    {
        public CFCriteriaClass()
        {
            SearchQuery = new CFSearchClass();
        }
        public CFSearchClass SearchQuery { get; set; }
    }
    public class CFSearchClass
    {
        public CFSearchClass()
        {
            AND = new List<object>();
        }
        [JsonProperty(PropertyName = "$AND")]
        public List<object> AND { get; set; }

    }
    public class SearchCode1Option
    {
        public SearchCode1Option()
        {
            Code1 = new Dictionary<string, string>();
        }

        public Dictionary<string, string> Code1 { get; set; }
    }
    public class SearchCode2Option
    {
        public SearchCode2Option()
        {
            Code2 = new Dictionary<string, string>();
        }

        public Dictionary<string, string> Code2 { get; set; }

    }
    public class SearchCode3Option
    {
        public SearchCode3Option()
        {
            Code3 = new Dictionary<string, string>();
        }
        public Dictionary<string, string> Code3 { get; set; }
    }
    public class SearchCode4Option
    {
        public SearchCode4Option()
        {

            Code4 = new Dictionary<string, string>();
        }

        public Dictionary<string, string> Code4 { get; set; }
    }
    public class ResponseConfigureSettingDataFromMaximizer
    {
        public int Code { get; set; }
        public ConfigurationData ConfigurationSetting { get; set; }
    }
    public class ConfigurationData
    {
        public List<DataFieldsObj> Data { get; set; }
    }
    public class DataFieldsObj
    {
        public string TextValue { get; set; }
        public string Description { get; set; }
    }

}