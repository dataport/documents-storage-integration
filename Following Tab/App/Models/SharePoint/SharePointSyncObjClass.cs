﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace CloudElement.Models.DataObject
{
    public class SharePointSyncObjClass
    {
        public SharePointSyncObjClass()
        {
            Scope = new SPSScopeClass();
            Criteria = new SPSCriteriaClass();
        }
        public SPSScopeClass Scope { get; set; }
        public SPSCriteriaClass Criteria { get; set; }
    }
    public class SPSScopeClass
    {
        public SPSScopeClass()
        {
            Fields = new SPSFieldsClass();
        }
        public SPSFieldsClass Fields { get; set; }
    }
    public class SPSFieldsClass
    {
        public SPSFieldsClass()
        {
            FullName = 1;
        }
        public int FullName { get; set; }
    }
    public class SPSCriteriaClass
    {
        public SPSCriteriaClass()
        {
            SearchQuery = new SPSSearchClass();
        }
        public SPSSearchClass SearchQuery { get; set; }
    }
    public class SPSSearchClass
    {
        public SPSSearchClass()
        {
            Key = new Dictionary<string, string>();
        }
        public Dictionary<string, string> Key { get; set; }
    }
    public class ResponseAbEntryDataFromMaximizer
    {
        public int Code { get; set; }
        public List<string> Msg { get; set; }
        public AbEntryData AbEntry { get; set; }
    }
    public class AbEntryData
    {
        public List<GenericObject> Data { get; set; }
    }
    public class GenericObject
    {
        public string FullName { get; set; }
    }
}