﻿using Newtonsoft.Json;
using SharePoint.Class;
using System;
using System.Resources;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using CloudElement.Resource;
using System.IO;
using System.Web.Configuration;
namespace CloudElement.Models.DataObject
{
	public class SharePointMapper
	{
		private string AppID = null;
		private DataObjectModel Model;
		private string CloudElementProductionURL = "https://console.cloud-elements.com/";
		private string CloudElementStagingURL = "https://staging.cloud-elements.com/";
        public string CloudElementURL = "";
		private string CloudElementDocumnetsURL = "elements/api-v2/hubs/documents/folders/contents";
		private string CloudElementDocumnetMetadataURL = "elements/api-v2/hubs/documents/files";
        private string CloudElementConfigurationURL = "elements/api-v2/instances?hydrate=true";

		public SharePointMapper()
		{
			AppID = GetAppID();
            CloudElementURL = (CheckEnvironment()) ? CloudElementProductionURL : CloudElementStagingURL;
        }
		public string GetAppID()
		{
			string qbid = "";
			if (ConfigurationManager.AppSettings["ApplicationID"] != null)
			{
				qbid = ConfigurationManager.AppSettings["ApplicationID"] as string;
			}

			return qbid;
		}
		public string GetUser()
		{
			string user = "";
			if (ConfigurationManager.AppSettings["CloudElementUser"] != null)
			{
				user = ConfigurationManager.AppSettings["CloudElementUser"] as string;
			}

			return user;
		}
		public bool CheckEnvironment()
		{
			string IsProduction = "";
			bool IsQBProduction = true;
			if (ConfigurationManager.AppSettings["CloudElementProduction"] != null)
			{
				IsProduction = ConfigurationManager.AppSettings["CloudElementProduction"] as string;
			}
			if (IsProduction.ToLower() == "false")
			{
				IsQBProduction = false;
			}
			return IsQBProduction;
		}
		public string GetOrg()
		{
			string org = "";
			if (ConfigurationManager.AppSettings["CloudElementOrg"] != null)
			{
				org = ConfigurationManager.AppSettings["CloudElementOrg"] as string;
			}

			return org;
		}
		public DataObjectModel Load(DataObjectModel model)
		{
			Model = model;
			GeneratingString();
			//PopulateDropDownList();
			return Model;
		}
		public void GeneratingString()
		{
			MetaString strs = new MetaString();
			strs.StrConnectBtn = DataObjectResource.GEN_CONNECT_TXT;
			strs.StrDisConnectBtn = DataObjectResource.GEN_DISCONNECT_TXT;
			strs.StrSync = DataObjectResource.GEN_SYNC_TXT;
			strs.StrSystemErrorTitle = DataObjectResource.SYSTEM_ERROR_TITLE;
			strs.StrSystemErrorMsg = DataObjectResource.SYSTEM_ERROR_MSG;

			JavaScriptSerializer jss = new JavaScriptSerializer();
			Dictionary<string, string> JsStrs = new Dictionary<string, string>();

            JsStrs.Add("Col_Date", DataObjectResource.TYPE_DATE);
            //JsStrs.Add("Col_FileType", DataObjectResource.TYPE_FILE_TYPE);
            JsStrs.Add("Col_FileName", DataObjectResource.TYPE_FILE_NAME);
            //JsStrs.Add("Col_Title", DataObjectResource.TYPE_TITLE);
            JsStrs.Add("Col_Path", DataObjectResource.TYPE_PATH);
            //JsStrs.Add("Col_LastModifiedBy", DataObjectResource.TYPE_LAST_MODIFIED_BY);
            JsStrs.Add("Col_CreatedOn", DataObjectResource.TYPE_CREATED_ON);

            Dictionary<string, string> JSErrorStrs = new Dictionary<string, string>();
			JSErrorStrs.Add("Code:400(3)", DataObjectResource.SYSTEM_ERROR_BAD_REQUEST);
			JSErrorStrs.Add("Code:401(3)", DataObjectResource.SYSTEM_ERROR_UNAUTHORIZED);
			JSErrorStrs.Add("Code:403(3)", DataObjectResource.SYSTEM_ERROR_FORBIDDEN);
			JSErrorStrs.Add("Code:404(3)", DataObjectResource.SYSTEM_ERROR_NOT_FOUND);
			JSErrorStrs.Add("Code:405(3)", DataObjectResource.SYSTEM_ERROR_METHOD_NOT_ALLOWED);
			JSErrorStrs.Add("Code:406(3)", DataObjectResource.SYSTEM_ERROR_NOT_ACCEPTABLE);
			JSErrorStrs.Add("Code:409(3)", DataObjectResource.SYSTEM_ERROR_CONFLICT);
			JSErrorStrs.Add("Code:415(3)", DataObjectResource.SYSTEM_ERROR_UNSUPPORTED_MEDIA_TYPE);
			JSErrorStrs.Add("Code:500(3)", DataObjectResource.SYSTEM_ERROR_SERVER_ERROR);
			JSErrorStrs.Add("Code:502(3)", DataObjectResource.SYSTEM_ERROR_PROVIDER_SERVER_ERROR);

			Model.MetaStr = strs;
			Model.JSStrings = jss.Serialize(JsStrs);
			Model.JSErrorStrings = jss.Serialize(JSErrorStrs);
		}
        public JObject GetConfiguration(MaxApiModel MxObj)
        {
            string InstanceToken = "";
            JObject response = new JObject();
            try
            {
                ResponsDataWithOnRequest maxResponse = GetConfigureAndInstance(MxObj);
                if (maxResponse.Code != -2)
                {
                    if (HasInstanceToken(maxResponse) && maxResponse.ConfigurationSetting!=null && maxResponse.ConfigurationSetting.Data.Count != 0)
                    {
                        var configurationSetting = maxResponse.ConfigurationSetting.Data[0];
                        InstanceToken = configurationSetting.TextValue;

                        JObject requestJObj = new JObject();
                        try
                        {
                            try
                            {
                                string url = CloudElementURL + CloudElementConfigurationURL;
                                string user = GetUser();
                                string uorg = GetOrg();

                                Dictionary<string, string> header = new Dictionary<string, string>()
                                                            {
                                                                {   "Authorization",
                                                                    "User " + user + ", " + "Organization " + uorg + ", " + "Element " + InstanceToken
                                                                }
                                                            };

                                requestJObj.Add(new JProperty("url", url));
                                requestJObj.Add(new JProperty("header", JsonConvert.DeserializeObject<JObject>(JsonConvert.SerializeObject(header))));

                                string jsonRes = WebClient.CreateGetHttpResponse(url, "Content-Type: application/json", header);
                                jsonRes = jsonRes.TrimStart(new char[] { '[' }).TrimEnd(new char[] { ']' });
                                response = JObject.Parse(jsonRes);
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message + ";" + ex.StackTrace);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message + ";" + ex.StackTrace);
            }
            
            return response;
        }
        public JObject GetDocuments(MaxApiModel MxObj)
        {
            string InstanceToken = "";
            JObject documentsdata = new JObject();
            JObject tt = new JObject();

            ResponsDataWithOnRequest maxResponse = GetConfigureAndInstance(MxObj);
            DocumentsDataSetNew returnDocuments = null;
            try
            {
                if (maxResponse.Code != -2)
                {
                    if (HasInstanceToken(maxResponse))
                    {
                        var configurationSetting = maxResponse.ConfigurationSetting.Data[0];
                        InstanceToken = configurationSetting.TextValue;
                        var Team_Member_Id = "";
                        var cloudDocumentName = WebConfigurationManager.AppSettings["CloudDocumentName"];
                        if (cloudDocumentName.Equals("DROPBOXBIZ"))
                        {
                            Team_Member_Id = configurationSetting.Description;
                        }
                        documentsdata = GetDocumentsData(InstanceToken);
                        var spdocids = JsonConvert.DeserializeObject<List<string>>(MxObj.SPDocIds);

                        JArray res = new JArray();
                        JObject errMsgCE = new JObject();
                        try
                        {
                            if (spdocids != null)
                            {
                                foreach (var spdocid in spdocids)
                                {
                                    var documentMetaData = GetDocumentMetaData(InstanceToken, Team_Member_Id, spdocid, MxObj.Subsite);

                                    //var json = JsonConvert.DeserializeObject(documentMetaData.ToString());
                                    if (documentMetaData["message"] != null)
                                    {
                                        JObject jobj = new JObject();
                                        jobj.Add(new JProperty("statusCode", documentMetaData["StatusCode"]));
                                        jobj.Add(new JProperty("refId", spdocid));
                                        jobj.Add(new JProperty("error", documentMetaData["message"]));
                                        res.Add(jobj);
                                    }
                                    else
                                    {
                                        res.Add(documentMetaData);
                                    }
                                }

                            }
                            
                        }
                        catch (Exception ex)
                        {
                            errMsgCE.Add(new JProperty("errorMessage", ex.StackTrace));
                        }

                        tt.Add(new JProperty("response", res));
                        tt.Add(new JProperty("error", errMsgCE));

                    }
                    returnDocuments = JsonConvert.DeserializeObject<DocumentsDataSetNew>(tt.ToString());
                }
            }
            catch (Exception ex)
            {
                JObject returnObj = new JObject();
                returnObj.Add("error", ex.Message);
                returnObj.Add("Trace", ex.StackTrace);
                return returnObj;
            }
            return JsonConvert.DeserializeObject<JObject>(JsonConvert.SerializeObject(returnDocuments));
        }

		private bool HasInstanceToken(ResponsDataWithOnRequest maxResponse)
		{
			return true;
		}
		private JObject GetDocumentsData(string InstanceToken)
		{
			JObject response = new JObject();
			JArray responseJObj = new JArray();
			JObject responseTest = new JObject();
			JObject requestJObj = new JObject();
			JObject errObj = new JObject();
			try
			{
				try
				{
					string queryString = "path=%2F";
					string url = (CheckEnvironment()) ? CloudElementProductionURL : CloudElementStagingURL;
					string user = GetUser();
					string uorg = GetOrg();

					Dictionary<string, string> header = new Dictionary<string, string>()
															{
																{   "Authorization",
																	"User " + user + ", " + "Organization " + uorg + ", " + "Element " + InstanceToken
																}
															};

					url += CloudElementDocumnetsURL + "?" + queryString;
					requestJObj.Add(new JProperty("url", url));
					requestJObj.Add(new JProperty("header", JsonConvert.DeserializeObject<JObject>(JsonConvert.SerializeObject(header))));

					responseJObj = JArray.Parse(WebClient.CreateGetHttpResponse(url, "Content-Type: application/json", header));
				}
				catch (Exception ex)
				{
					errObj.Add(new JProperty("message", ex.Message));
					errObj.Add(new JProperty("trace", ex.StackTrace));
				}
				response.Add(new JProperty("response", responseJObj));
				response.Add(new JProperty("error", errObj));
				return response;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message + ";" + ex.StackTrace);
			}
		}

        private JObject GetDocumentMetaData(string InstanceToken, string Team_Member_Id, string DocumentKey, string subsite)
        {
            JObject response = new JObject();
            JObject requestJObj = new JObject();
            try
            {
                try
                {
                    string queryString = DocumentKey + "/metadata";
                    string url = (CheckEnvironment()) ? CloudElementProductionURL : CloudElementStagingURL;
                    string user = GetUser();
                    string uorg = GetOrg();
                    Dictionary<string, string> header = new Dictionary<string, string>();
                    if (subsite == "")
                    {
                        header = new Dictionary<string, string>()
                                                            {
                                                                {   "Authorization",
                                                                    "User " + user + ", " + "Organization " + uorg + ", " + "Element " + InstanceToken
                                                                }
                                                            };
                    }else
                    {
                        header = new Dictionary<string, string>()
                                                            {
                                                                {   "Authorization",
                                                                    "User " + user + ", " + "Organization " + uorg + ", " + "Element " + InstanceToken
                                                                },
                                                                {   "Subsite",
                                                                    subsite
                                                                }
                                                            };
                    }
                    var cloudDocumentName = WebConfigurationManager.AppSettings["CloudDocumentName"];
                    if (cloudDocumentName.Equals("DROPBOXBIZ"))
                    {
                        header.Add("Elements-As-Team-Member", Team_Member_Id);
                    }
                    url += CloudElementDocumnetMetadataURL + "/" + queryString;

                    //For Dropbox Business, request should provide namespaceId and includeTeadSpace=true
                    if (WebConfigurationManager.AppSettings["CloudDocumentName"] == "DROPBOXBIZ")
                    {

                        string namsspaceUrl = ((CheckEnvironment()) ? CloudElementProductionURL : CloudElementStagingURL) + "elements/api-v2/namespaces";
                        string nameSpaceResponse = WebClient.CreateGetHttpResponse(namsspaceUrl, "Content-Type: application/json", header);
                        JArray namespaceArray = JArray.Parse(nameSpaceResponse);
                        string teamSpaceId = "";
                        for (var i = 0; i < namespaceArray.Count; i++)
                        {
                            if (((string)namespaceArray[i]["namespace_type"][".tag"]) == "team_folder")
                            {
                                teamSpaceId = (string)namespaceArray[i]["namespace_id"];
                                break;
                            }
                        }
                        url += "?includeTeamSpaces=true&teamSpaceId=" + teamSpaceId;
                    }

                    requestJObj.Add(new JProperty("url", url));
                    requestJObj.Add(new JProperty("header", JsonConvert.DeserializeObject<JObject>(JsonConvert.SerializeObject(header))));
                    response = JObject.Parse(WebClient.CreateGetHttpResponse(url, "Content-Type: application/json", header));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + ";" + ex.StackTrace);
            }
        }
		public ResponsDataWithOnRequest GetConfigureAndInstance(MaxApiModel MxObj)
		{
			var requestobj = "";
            //var cloudDocumentName = MxObj.CloudDocumentName;
            var cloudDocumentName = WebConfigurationManager.AppSettings["CloudDocumentName"];
			var obj = new RequestObj.RequestTokenObj();
            obj.ConfigurationSetting = RequestConfiguSettingObj(cloudDocumentName);
			obj.Token = MxObj.MXAPIToken;
            requestobj = JsonConvert.SerializeObject(obj);
			var url = MxObj.MXAPIURL + "/Json/Read";
			var responObj = JsonConvert.DeserializeObject<ResponsDataWithOnRequest>(WebClient.CreatePostHttpResponse(url, null, null, requestobj));

			return responObj;
		}
        public ResponsDataWithOnRequest GetConfigureInstanceID(MaxApiModel MxObj)
        {
            var requestobj = "";
            //var cloudDocumentName = MxObj.CloudDocumentName;
            var cloudDocumentName = WebConfigurationManager.AppSettings["CloudDocumentName"];
            var obj = new RequestObj.RequestTokenObj();
            obj.ConfigurationSetting = RequestConfiguSettingObj(cloudDocumentName,"ID");
            obj.Token = MxObj.MXAPIToken;
            requestobj = JsonConvert.SerializeObject(obj);
            var url = MxObj.MXAPIURL + "/Json/Read";
            var responObj = JsonConvert.DeserializeObject<ResponsDataWithOnRequest>(WebClient.CreatePostHttpResponse(url, null, null, requestobj));

            return responObj;
        }
		private string GetCloudElementInstance(MaxApiModel MxObj)
		{
			string InstanceToken = "";
			var requestobj = "";

			var obj = new RequestObj.RequestTokenObj();

            var cloudDocumentName = WebConfigurationManager.AppSettings["CloudDocumentName"];
            obj.ConfigurationSetting = RequestConfiguSettingObj(cloudDocumentName);

			obj.Token = MxObj.MXAPIToken;

			requestobj = JsonConvert.SerializeObject(obj);
			var url = MxObj.MXAPIURL + "/Json/Read";
			var responObj = JsonConvert.DeserializeObject<ResponseConfigureSettingDataFromMaximizer>(WebClient.CreatePostHttpResponse(url, null, null, requestobj));
			if (responObj.Code == 0)
			{
				if (responObj.ConfigurationSetting != null)
				{
					if (responObj.ConfigurationSetting.Data.Count > 0)
					{
						InstanceToken = responObj.ConfigurationSetting.Data[0].TextValue;
					}
				}
			}
			return InstanceToken;
		}

        private string GetTeamMemberId(MaxApiModel MxObj)
        {
            string TeamMemberId = "";
            var requestobj = "";

            var obj = new RequestObj.RequestTokenObj();

            var cloudDocumentName = WebConfigurationManager.AppSettings["CloudDocumentName"];
            obj.ConfigurationSetting = RequestConfiguSettingObj(cloudDocumentName);

            obj.Token = MxObj.MXAPIToken;

            requestobj = JsonConvert.SerializeObject(obj);
            var url = MxObj.MXAPIURL + "/Json/Read";
            var responObj = JsonConvert.DeserializeObject<ResponseConfigureSettingDataFromMaximizer>(WebClient.CreatePostHttpResponse(url, null, null, requestobj));
            if (responObj.Code == 0)
            {
                if (responObj.ConfigurationSetting != null)
                {
                    if (responObj.ConfigurationSetting.Data.Count > 0)
                    {
                        TeamMemberId = responObj.ConfigurationSetting.Data[0].Description;
                    }
                }
            }
            return TeamMemberId;
        }

        private ConfigurationObjClass RequestConfiguSettingObj(string cloudDocumentName, string option = "Token")
		{
			var config = new ConfigurationObjClass();
			var scope = new CFScopeClass();
			var fields = new CFFieldsClass();
			var criteria = new CFCriteriaClass();
			var search = new CFSearchClass();
			var searchCode1Option = new SearchCode1Option();
			var searchCode2Option = new SearchCode2Option();
			var searchCode3Option = new SearchCode3Option();
			var searchCode4Option = new SearchCode4Option();

            string code3Str = "";
            string code4Str = "";
            switch (cloudDocumentName.ToUpper())
            {
                case "BOX":
                    code3Str = "BOX_INT";
                    code4Str = "MX_BOX_TOKEN";
                    break;
                case "ONEDRIVE":
                    code3Str = "ONEDRIVE_INT";
                    code4Str = "MX_ONEDRIVE_TOKEN";
                    break;
                case "AMAZON":
                    code3Str = "AMAZONS3_INT";
                    code4Str = "MX_AMAZONS3_TOKEN";
                    break;
                case "DROPBOX":
                    code3Str = "DROPBOX_INT";
                    code4Str = "MX_DROPBOX_TOKEN";
                    break;
                case "GOOGLEDRIVE":
                    code3Str = "GOOGLEDRIVE_INT";
                    code4Str = "MX_GOOGLEDRIVE_TOKEN";
                    break;
                case "ONEDRIVEBIZ":
                    code3Str = "ONEDRIVEBIZ_INT";
                    code4Str = "MX_ONEDRIVEBIZ_TOKEN";
                    break;
                case "DROPBOXBIZ":
                    code3Str = "DROPBOXBIZ_INT";
                    code4Str = "MX_DROPBOXBIZ_TOKEN";
                    break;
            }
            if (!option.ToUpper().Equals("TOKEN"))
            {
                code4Str.Replace("_TOKEN", option.ToUpper());
            }
            searchCode1Option.Code1.Add("$EQ", AppID);
            searchCode3Option.Code3.Add("$EQ", code3Str);
            searchCode4Option.Code4.Add("$EQ", code4Str);

			search.AND.Add(searchCode3Option);

			search.AND.Add(searchCode4Option);

			criteria.SearchQuery = search;

			scope.Fields = fields;

			config.Scope = scope;
			config.Criteria = criteria;

			return config;
		}
		public void DownLoadQuickTransaction(MaxApiModel MxObj)
		{
			DownloadSP(MxObj);
		}
		public JObject DownloadSP(MaxApiModel MxObj)
		{
			string InstanceToken = "";
			string url = CloudElementURL;
			InstanceToken = GetCloudElementInstance(MxObj);

			string user = GetUser();
			string uorg = GetOrg();

            Dictionary<string, string> header = new Dictionary<string, string>();

            if (MxObj.Subsite == "") {
                header = new Dictionary<string, string>()
                {
				    {"Authorization", "User "+ user +", " + "Organization " + uorg + ", " + "Element " + InstanceToken}
			    };

                var Team_Member_Id = "";
                var cloudDocumentName = WebConfigurationManager.AppSettings["CloudDocumentName"];
                if (cloudDocumentName.Equals("DROPBOXBIZ"))
                {
                    Team_Member_Id = GetTeamMemberId(MxObj);
                    header.Add("Elements-As-Team-Member", Team_Member_Id);
                }
            }else
            {
                header = new Dictionary<string, string>()
                {
                    {"Authorization", "User "+ user +", " + "Organization " + uorg + ", " + "Element " + InstanceToken},
                    {"Subsite", MxObj.Subsite}
                };
            }

			url += CloudElementDocumnetMetadataURL + "/" + MxObj.DocumentNumber;
            //For Dropbox Business, request should provide namespaceId and includeTeadSpace=true
            if (WebConfigurationManager.AppSettings["CloudDocumentName"] == "DROPBOXBIZ")
            {

                string namsspaceUrl = (CheckEnvironment()) ? CloudElementProductionURL : CloudElementStagingURL;
                namsspaceUrl += "elements/api-v2/namespaces";
                string nameSpaceResponse = WebClient.CreateGetHttpResponse(namsspaceUrl, "Content-Type: application/json", header);
                JArray namespaceArray = JArray.Parse(nameSpaceResponse);
                string teamSpaceId = "";
                for (var i = 0; i < namespaceArray.Count; i++)
                {
                    if (((string)namespaceArray[i]["namespace_type"][".tag"]) == "team_folder")
                    {
                        teamSpaceId = (string)namespaceArray[i]["namespace_id"];
                        break;
                    }
                }
                url += "?includeTeamSpaces=true&teamSpaceId=" + teamSpaceId;
            }

			var result = WebClient.DownloadFile(url, "application/json", header, MxObj.DocumentID);
            if (result == "success") return new JObject(new JProperty("result", "Download Success"));
            else return new JObject(new JProperty("error", result));
		}
	}
}