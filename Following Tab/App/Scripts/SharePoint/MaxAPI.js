/************************************************************************************************
	
	Copyright ?1988 - 2013 Maximizer Software Inc.  

	This source code is provided as an example on how to use Maximizer's APIs.
	Anyone is free to copy, modify, publish, use, compile, sell, or distribute
	this source code, either in source code form or as a compiled binary, for
	any purpose, commercial or non-commercial, and by any means.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.

************************************************************************************************/

// *********************************************************************************************
// This file provides functions for performing the asynchronous requests to the
//  Maximizer Web Data API.
//
//  There is code that will automatically detect if the program is run inside Gmail or not.
//  In Gmail, it uses makeRequest() for HTTP requests.  Otherwise (i.e. using Gadget.html), 
//  it will use the normal XMLHttpRequest object to make the HTTP requests.
// *********************************************************************************************

//var MaxDataApiMode="async";		// synchronous ajax calls is only supported for local testing,
								// and not for calls through makeRequest()

// changed by suman
var MaxDataApiMode = "sync";

//
// URL for the web data api
//
var urlMaximizerWebDataAPI = "http://localhost/MaximizerWebData/Data.svc/json/";


															
								
//For simplicity, we will create a function that we can re-use
//to make calls to Maximizer.Web.Data synchronously.
function callMaximizerApiSynchronous(methodName, parameters) {
    //First, we create a variable to store the result of the call
    var returnValue = '';
    //Set up an XHR object with the URL of the method endpoint.
    //Note that this will only work in IE8 or later.
    //For best practice, use a library like jQuery for XHR requests.

    var httpRequest = new XMLHttpRequest();
    httpRequest.open('POST', urlMaximizerWebDataAPI + methodName, false);

    //Add a callback to the XHR to get the return value.
    httpRequest.onreadystatechange = function () {
        //When the HTTP request returns, if the status is not 200 (OK), 
        //then a transport-level error has occurred. We should also
        //handle network errors.
        if (httpRequest.readyState == 4 && httpRequest.status == 200) {
            //The method returns a JSON object which we need to parse
            returnValue = JSON.parse(httpRequest.responseText);
        }
    };

    //The Content-Type must be set to 'application/json'.
    httpRequest.setRequestHeader('Content-Type', 'text/plain');

    //Finally, send the request.
    //The JSON object is passed in the request body as a string.
    httpRequest.send(JSON.stringify(parameters));

    //When the callback returns, the return value is stored in 
    //the 'returnValue' variable.
    return returnValue;
}
	
	
/*
 * get System Information
 */
function maxGetSystemInfo() {
	return callMaximizerApiSynchronous('GetSystemInfo', '');
}


/*
 * get Session Information
 */
function maxGetSessionInfo(maxToken) {
	var request = {
		"Token": maxToken,
		"User": 1,
		"AddressBook": {
			"UDBID": 1,
			"Name": 1, 
			"DisplayValue": 1
		}
	};
	
	return callMaximizerApiSynchronous('GetSessionInfo', request);
}

	
	
/*
 * userRead for current User
 */ 
function maxGetCurrentUserInfo(maxToken) {
	var userReadRequest = {
		Token: maxToken,
		"User": {
			"Scope": {
			"Fields": {
				"Key": {
				"UID": 1
				},
				"DisplayName": 1,
				"Role": {
				"Administrator": 1,
				"SalesManager": 1,
				"SalesRepresentative": 1,
				"CustomerServiceManager": 1,
				"CustomerServiceRepresentative": 1,
				"KnowledgeBaseApprover": 1
				}
			}
			},
			"Criteria": {
			"SearchQuery": {
				"$EQ": {
				"Key": {
					"UID": "$CURRENTUSER()"
				}
				}
			}
			}
		}
	};
	
	var response = callMaximizerApiSynchronous('UserRead', userReadRequest);
	
	return response;
}
								
						
/*
 *  Get field info
 */
function maxGetFieldInfo(maxToken){
   
	
    // Get Field Info
    var gfiRequest = {
        "Token": maxToken,
        "AbEntry": {
            "Options": {
                "Complex": true
            }
        }
    };

    var gfiResponse = callMaximizerApiSynchronous('AbEntryGetFieldInfo', gfiRequest);

    var properties = gfiResponse.AbEntry.Data.properties;

    return properties;
}

/*
 *  Get schema data for AB Entry Fields
 */
function maxSchemaRead(maxToken){   
	
    // Get schema data
    var gsdRequest = {
        "Token": maxToken,
        "Schema": {
			"Scope": {
				"Fields": {
					"Key": 1,
					"Alias": 1,
					"AppliesTo": 1,
					"Type": 1,
					"Name": 1,
					"Folder": {
						"Key": 1,
						"Path": 1
					},
					"SecAccess": {
						"Write": 0,
						"Read": 0
					},
					"Creator": {
						"Key": {
							"Value": 0,
							"UID": 0
						},
						"DisplayName": 0
					},
					"CreationDate": 0,
					"RequestedBy": 0,
					"Items": [
						{
							"Key": 0,
							"Value": 0
						}
					],
					"Inactive": 1,
					"Mandatory": 0,
					"Formula": {
						"Rule": 0
					},
					"Attributes": 1
				}
			},
			"Criteria": {
				"SearchQuery": {
					"$EQ": {
						"AppliesTo": "AbEntry"
					}
				}
			}
		}
    };
	
    var gsdResponse = callMaximizerApiSynchronous('SchemaRead', gsdRequest);

    return gsdResponse;
}

/*
 *  Create Boolean UDF
 */
function maxSchemaCreateAbBooleanUDF(maxToken, name, folderKey){
    // Get Field Info
    var gfiRequest = {
        "Token": maxToken,
        "Schema": {
			"Data": {
				"Key": null,
				"AppliesTo": [
					"Contact",
					"Individual"
				],
				"Name": name,
				"Type": "EnumField<StringItem>",
				"Folder": {
					"Key": folderKey
				},
				"Items": [
					{
						"Key": null,
						"Value": "Yes"
					},
					{
						"Key": null,
						"Value": "No"
					}
				],
				"SecAccess": {
					"Write": [
					{
						"Key": {
							"UID": "*"
						}	
					}
					],
					"Read": [
					{
						"Key": {
							"UID": "*"
						}	
					}
					]
				},
				"Inactive": false,
				"Attributes": {
					"YesNo": true
				}
			}
        }    
    };
	
	// console.log(JSON.stringify(gfiRequest, null, 4));
	var gfiResponse = callMaximizerApiSynchronous('schemaCreate', gfiRequest);

	return gfiResponse;
}

/*
 *  Create String UDF
 */
function maxSchemaCreateAbStringUDF(maxToken, name, folderKey, length){
    var gfiRequest = {
        "Token": maxToken,
        "Schema": {
            "Data": {			
				"Key": null,
				"AppliesTo": [
					"Contact",
					"Individual"
				],
				"Name": name,
				"Type": "StringField",
				"Folder": {
					"Key": folderKey
				},
				"SecAccess": {
					"Write": [
					{
						"Key": {
							"UID": "*"
						}	
					}
					],
					"Read": [
					{
						"Key": {
							"UID": "*"
						}	
					}
					]
				},
				"Inactive": false,
				"Attributes": {
					"MaxLength": length
				}
			}
        }              
    };

    var gfiResponse = callMaximizerApiSynchronous('schemaCreate', gfiRequest);

    return gfiResponse;
}

/*
 *  Get Schema Folders
 */
function maxGetSchemaFolders(maxToken, searchString){
    // Get Field Info
    var gfiRequest = {
        "Token": maxToken,
        "SchemaFolder": {
            "Scope": {
				"Fields": {
					"Key":1,
					"Name": 1,
					"Path": 1,
					"AppliesTo": 1
				}
			},
			"Criteria":{
				"SearchString": searchString
			}  
        }                    
    };

    var gfiResponse = callMaximizerApiSynchronous('SchemaFolderRead', gfiRequest);

    return gfiResponse.SchemaFolder.Data;
}

function maxCreateSchemaFolder(maxToken, folderName, path){

	var request= {
		Token: maxToken,
		"SchemaFolder": {
			"Data": {
				"Key": null,
				"Name": folderName,
				"Path": path,
				"AppliesTo": [
					"Company",
					"Contact",
					"Individual"
				]
			}
		}
	};			

	var scResponse = callMaximizerApiSynchronous('SchemaFolderCreate', request);

	return scResponse;
}

/*
 *  Get MailChimp Connector mapping
 */
function getMailChimpConnectorMapping(maxToken) {

    var request = {
        Token: maxToken,
        "Custom": {
            "Scope": {
                "Fields": {
                    "Key": {
                        "ID": 1,
                        "Value": 1
                    },
                    "ApplicationId": 1,
                    "Name": 1,
                    "Description": 1,
                    "Text1": 1,
                    "Text2": 1,
                    "Text3": 1,
                    "Text4": 1
                }
            },
            "Criteria": {
                "SearchString": "AND(EQ(ApplicationId,\"MCConnector\"), EQ(Name, \"Mapping\"))"
            }
        }
    };

    var response = callMaximizerApiSynchronous('CustomRead', request);

    return response.Custom.Data;
}


								
								
// Web Data API  - Authenticate method
function MaxDataAPI_Authenticate(database, UserID, Password) {
    var parameters = {};
    parameters.Database = database;
    parameters.UID = UserID;
    parameters.Password = Password;
    return CallMaximizer("Authenticate", parameters);
}


// Web Data API  - TokenValid method
function MaxDataAPI_TokenValid(token) {
    var parameters = {};
    parameters.Token = token;
    return CallMaximizer('TokenValid', parameters);
}

// Web Data API  - AbEntryGetFieldInfo method
function MaxDataAPI_AbEntryGetFieldInfo(token)
{
    var parameters = {};
    parameters.Token = token;
	parameters.AbEntry = {Options: {Complex: true}};
    return CallMaximizer('AbEntryGetFieldInfo', parameters);
}

// Web Data API  - AbEntryGetFieldOptions method
function MaxDataAPI_AbEntryGetFieldOptions(token)
{
    var parameters = {};
    parameters.Token = token;
	parameters.AbEntry = {
		"Data": {"Key":null},
		"Options":{
			"SecAccess/Write":{"Key":{Value:1, "UID":1},"DisplayName":1},
			"SecAccess/Read":{"Key":{Value:1, "UID":1},"DisplayName":1}
			}
		};
    return CallMaximizer("AbEntryGetFieldOptions", parameters);
}

// Web Data API  - AbEntryRead method				
function MaxDataAPI_AbEntryRead(token, searchString, fields) {

    var parameters = {};
    parameters.Token = token;
    parameters.AbEntry = { Scope : 
    						{ Fields : {} } 
    					};
    parameters.AbEntry.Scope.Fields = fields;
	parameters.AbEntry.Criteria = {};
	parameters.AbEntry.Criteria.SearchString = searchString;

	//return callMaximizerApiSynchronous("AbEntryRead", parameters);

    return CallMaximizer("AbEntryRead", parameters);
}

// Web Data API  - AbEntryCreate method
function MaxDataAPI_AbEntryCreate(token, parentKey, type, fieldValues) {
	return MaxDataAPI_AbEntry_UpdateCreate("AbEntryCreate", token, null, parentKey, type, fieldValues);
}

// Web Data API  - AbEntryUpdate method
function MaxDataAPI_AbEntryUpdate(token, key, parentKey, fieldValues) {
	return MaxDataAPI_AbEntry_UpdateCreate("AbEntryUpdate", token, key, parentKey, null, fieldValues);
}
		
// Web Data API  - funtion to handle AbEntryCreate and AbEntryUpdate methods
function MaxDataAPI_AbEntry_UpdateCreate(method, token, key, parentKey, type, fieldValues) {
    var parameters = {};
    parameters.Token = token;
	parameters.AbEntry = {};
	parameters.AbEntry.Data = fieldValues;
	
	if (key)
		parameters.AbEntry.Data.Key = key;
	if (parentKey)
		parameters.AbEntry.Data.ParentKey = parentKey;
	if (type)
		parameters.AbEntry.Data.Type = type;

	return CallMaximizer(method, parameters);
}

// Web Data API  - AbEntryDelete method
function MaxDataAPI_AbEntryDelete(token, key) {
    var parameters = {};
    parameters.Token = token;
	parameters.AbEntry = {};
	parameters.AbEntry.Data = {};
	parameters.AbEntry.Data.Key = key;
	return CallMaximizer("AbEntryDelete", parameters);
}


// Web Data API  - NoteRead method
function MaxDataAPI_NoteRead(token, searchString, fields) {
    var parameters = {};
    parameters.Token = token;
    parameters.Note = { Scope : 
    						{ Fields : {} } 
    					};
    parameters.Note.Scope.Fields = fields;
	parameters.Note.Criteria = {};
	parameters.Note.Criteria.SearchString = searchString;
    return CallMaximizer("NoteRead", parameters);
}


// Web Data API  - NoteCreate method
// Create Note  (this function won't let you change: creator or text)
function MaxDataAPI_NoteCreate(token, parentKey, dateTime, type, category, richText, important, fullAccess, readAccess) {
	return MaxDataAPI_Note_UpdateCreate("NoteCreate", token, null, parentKey, dateTime, type, category, richText, important, fullAccess, readAccess);
}

// Web Data API  - NoteUpdate method
// Update Note  (this function won't let you change: creator or text)
function MaxDataAPI_NoteUpdate(token, key, parentKey, dateTime, type, category, richText, important, fullAccess, readAccess) {
	return MaxDataAPI_Note_UpdateCreate("NoteUpdate", token, key, parentKey, dateTime, type, category, richText, important, fullAccess, readAccess);
}

// Web Data API  - function to handle NoteUpdate and NoteCreate methods
function MaxDataAPI_Note_UpdateCreate(method, token, key, parentKey, dateTime, type, category, richText, important, fullAccess, readAccess) {
    var parameters = {};
    parameters.Token = token;
	parameters.Note = {};
	parameters.Note.Data = {};
	
	if (key)
		parameters.Note.Data.Key = key;
	if (parentKey)
		parameters.Note.Data.ParentKey = parentKey;
	if (dateTime)
		parameters.Note.Data.DateTime = dateTime;
	
	if (type!=null)
	{
		parameters.Note.Data.Type = type;
		
		if (type==0 && category)
			parameters.Note.Data.Category = category;
	}
	
	if (richText)
		parameters.Note.Data.RichText = richText;
		
	if (important!=null)
		parameters.Note.Data.Important = important;
		
	if (fullAccess || readAccess)
	{
		parameters.Note.Data.SecAccess = {};
		if (fullAccess)
		{
			parameters.Note.Data.SecAccess.Write = {Items:[]};
			parameters.Note.Data.SecAccess.Read = {Items:[]};

			parameters.Note.Data.SecAccess.Write.Items[0]=fullAccess;
			if (readAccess)
				parameters.Note.Data.SecAccess.Read.Items[0]=readAccess;
		}
	}

	return CallMaximizer(method, parameters);
}


// Web Data API  - NoteDelete method
function MaxDataAPI_NoteDelete(token, key) {
    var parameters = {};
    parameters.Token = token;
	parameters.Note = {};
	parameters.Note.Data = {};
	parameters.Note.Data.Key = key;
	return CallMaximizer("NoteDelete", parameters);
}


// Web Data API  - NoteGetFieldInfo method
function MaxDataAPI_NoteGetFieldInfo(token)
{
    var parameters = {};
    parameters.Token = token;
    return CallMaximizer("NoteGetFieldInfo", parameters);
}

// Web Data API  - NoteGetFieldOptions method
function MaxDataAPI_NoteGetOptions(token, Key, ParentKey)
{
    var parameters = {};
    parameters.Token = token;
	parameters.Note = {
		"Data":{"Key":null,"ParentKey":null},
		"Options":{
			"Creator":{"Key":{Value:1, "UID":1},"DisplayName":1},
			"Category":1,
			"Type":{"Key":1,"Value":1},
			"SecAccess/Write":{"Key":{Value:1, "UID":1},"DisplayName":1},
			"SecAccess/Read":{"Key":{Value:1, "UID":1},"DisplayName":1}
			}
		};
	if (Key)
		parameters.Note.Data.Key = Key;
	if (ParentKey)
		parameters.Note.Data.ParentKey = ParentKey;
	
	// this is only used for testing in our test database
	if (Key==null && ParentKey==null)
		parameters.Note.Data.ParentKey = "Q29tcGFueQkxMzAzMDcwMDQxNjA5NDcwMDQ0NTRDCTA=2";
			
    return CallMaximizer("NoteGetFieldOptions", parameters);
}

// Web Data API  - OpportunityGetFieldInfo method
function MaxDataAPI_OpportunityGetFieldInfo(token)
{
    var parameters = {};
    parameters.Token = token;
    return CallMaximizer("OpportunityGetFieldInfo", parameters);
}

// Web Data API  - TaskGetFieldInfo method
function MaxDataAPI_TaskGetFieldInfo(token)
{
    var parameters = {};
    parameters.Token = token;
    return CallMaximizer("TaskGetFieldInfo", parameters);
}

// Web Data API  - TaskGetFieldOptions method
function MaxDataAPI_TaskGetFieldOptions(token, Key)
{
    var parameters = {};
    parameters.Token = token;
	parameters.Task = {
		"Data":{"Key":null},
		"Options":{
			"AssignedTo":{"Key":{Value:1, "UID":1},"DisplayName":1},
			"Priority":1,
			"Activity":1,
			"SecAccess/Write":{"Key":{Value:1, "UID":1},"DisplayName":1}
			}
		};
	if (Key)
		parameters.Task.Data.Key = Key;

    return CallMaximizer("TaskGetFieldOptions", parameters);
}


// Web Data API  - TaskCreate method
// Create Task  (this function won't let you change: creator or icontype)
function MaxDataAPI_TaskCreate(token, abEntryKey, dateTime, alarm, assignedTo, snoozeUntil, priority, completed, activityText) {
	return MaxDataAPI_Task_UpdateCreate("TaskCreate", token, null, abEntryKey, dateTime, alarm, assignedTo, snoozeUntil, priority, completed, activityText);
}

// Web Data API  - TaskUpdate method
// Update Task  (this function won't let you change: creator or icontype)
function MaxDataAPI_TaskUpdate(token, key, abEntryKey, dateTime, alarm, assignedTo, snoozeUntil, priority, completed, activityText) {
	return MaxDataAPI_Task_UpdateCreate("TaskUpdate", token, key, abEntryKey, dateTime, alarm, assignedTo, snoozeUntil, priority, completed, activityText);
}

// Web Data API  - function to handle TaskUpdate and TaskCreate methods
function MaxDataAPI_Task_UpdateCreate(method, token, key, abEntryKey, dateTime, alarm, assignedTo, snoozeUntil, priority, completed, activityText) {
    var parameters = {};
    parameters.Token = token;
	parameters.Task = {};
	parameters.Task.Data = {};
	
	if (key)
		parameters.Task.Data.Key = key;
	if (abEntryKey)
		parameters.Task.Data.AbEntryKey = abEntryKey;
	if (dateTime)
		parameters.Task.Data.DateTime = dateTime;
	parameters.Task.Data.Alarm = alarm;
	if (snoozeUntil)
		parameters.Task.Data.SnoozeUntil = snoozeUntil;
	if (priority)
		parameters.Task.Data.Priority = priority;
	if (completed)
		parameters.Task.Data.Completed = completed;
	if (activityText)
		parameters.Task.Data.Activity = activityText;

	if (assignedTo)
		parameters.Task.Data.AssignedTo = assignedTo;
		
	return CallMaximizer(method, parameters);
}




//
// This is asynchronous function for performing the HTTP POST.
//
// It returns a jquery $deferred.promise() with either the
//	 resulting obj or errmsg passed to the done() or fail() callback function.
//  
function CallMaximizer(method, parameters) {
	var webApiUrl = urlMaximizerWebDataAPI + method;
	
	if (MaxDataApiMode=="async")
	{
		return CallMaximizerAsync(webApiUrl, parameters);
	}
	else
		return CallMaximizerSync(webApiUrl, parameters);
}


// This is version of CallMaximizer that uses google's makeRequest method to make call to Web API server
function CallMaximizerGadgets(url, parameters) {
	var deferred = $.Deferred();
	
	var params = {};
	params[gadgets.io.RequestParameters.METHOD] = gadgets.io.MethodType.POST;
	params[gadgets.io.RequestParameters.CONTENT_TYPE] = gadgets.io.ContentType.JSON;
	
	params[gadgets.io.RequestParameters.HEADERS] = {"Content-Type" : "text/plain"};
		
	params[gadgets.io.RequestParameters.POST_DATA]= gadgets.json.stringify(parameters);

	try {
		googleIoMakeRequestWithoutCache(url, function(obj) {
					if (obj.errors=="")
					   deferred.resolve(obj.data);
					else
					   deferred.reject(obj.errors);}, 
							params);
	} catch (err)
	{
	  alert ("Error posting to url"+url+":"+err.toString());
	  return false;
	}
	return deferred.promise();
}
	  

// CallMaximizer that uses local XMLHttp object to make call to Web API server "Asynchronously"
function CallMaximizerAsync(url, parameters) {
    var httpRequest;
    var deferred = $.Deferred();

    if (window.XMLHttpRequest) { // Mozilla, Safari, ...
        httpRequest = new XMLHttpRequest();
    } else if (window.ActiveXObject) { // IE
        try {
            httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e) { }
        }
    }

    if (!httpRequest) {
        alert('This browser does not support AJAX');
        return false;
    }
    
    try {
    	httpRequest.open('POST', url, true);
    	httpRequest.setRequestHeader('Content-Type', 'text/plain');
    		
    	httpRequest.onreadystatechange = function () {
        	if (httpRequest.readyState === 4) {
            	if (httpRequest.status === 200) {
            		var returnObj = JSON.parse(httpRequest.responseText);
            		
            		if (returnObj.Code == 0)
            			deferred.resolve(returnObj);
            		else
            		{
            			if (returnObj.hasOwnProperty("Msg"))
	            			deferred.reject(returnObj);
	            		else
	            		{
	            			var errObj = {};
	            			errObj.Code = returnObj.Code;
	            			errObj.Msg = "There was an unknown Maximizer Web Data API error.";
		          			deferred.reject(errObj);
		          		}
            		}
            		
            	} else {
            		var errObj = {};
	            	errObj.Code = httpRequest.status;
	            	errObj.Msg = "There was a HTTP problem with error code:"+httpRequest.status;
		          	deferred.reject(errObj);
            	}
        	}
    	}
    	httpRequest.send(JSON.stringify(parameters));
	} catch (err)
	{
	  alert ("Error posting to url"+url+":"+err.toString());
	  return false;
	}
	return deferred.promise();
}




// CallMaximizer that uses local XMLHttp object to make call to Web API server "Synchronously"
function CallMaximizerSync(url, parameters) {
    var httpRequest;
    var returnValue;
    if (window.XMLHttpRequest) { // Mozilla, Safari, ...
        httpRequest = new XMLHttpRequest();
    } else if (window.ActiveXObject) { // IE
        try {
            httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e) { }
        }
    }

    if (!httpRequest) {
        alert('This browser does not support AJAX');
        return false;
    }

    httpRequest.open('POST', url, false);
    httpRequest.onreadystatechange = function () {
        if (httpRequest.readyState === 4) {
            if (httpRequest.status === 200) {
					returnValue = httpRequest.responseText;
            } else {
                alert('There was a problem with the request.');
            }
        }
    }
    
	httpRequest.setRequestHeader('Content-Type', 'text/plain'); 		
    httpRequest.send(JSON.stringify(parameters));
    return JSON.parse(returnValue);
}






// MINIFIED JSON2.JS

var JSON; if (!JSON) { JSON = {}; }
(function () {
"use strict"; function f(n) { return n < 10 ? '0' + n : n; }
if (typeof Date.prototype.toJSON !== 'function') {
Date.prototype.toJSON = function (key) {
return isFinite(this.valueOf()) ? this.getUTCFullYear() + '-' + f(this.getUTCMonth() + 1) + '-' + f(this.getUTCDate()) + 'T' + f(this.getUTCHours()) + ':' + f(this.getUTCMinutes()) + ':' + f(this.getUTCSeconds()) + 'Z' : null;
}; String.prototype.toJSON = Number.prototype.toJSON = Boolean.prototype.toJSON = function (key) { return this.valueOf(); };}
var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g, escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g, gap, indent, meta = { '\b': '\\b', '\t': '\\t', '\n': '\\n', '\f': '\\f', '\r': '\\r', '"': '\\"', '\\': '\\\\' }, rep; function 
quote(string) { escapable.lastIndex = 0; return escapable.test(string) ? '"' + string.replace(escapable, function (a) { var c = meta[a]; return typeof c === 'string' ? c : '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4); }) + '"' : '"' + string + '"'; }
function str(key, holder) {
var i, k, v, length, mind = gap, partial, value = holder[key]; if (value && typeof value === 'object' && typeof value.toJSON === 'function') { value = value.toJSON(key); }
if (typeof rep === 'function') { value = rep.call(holder, key, value); }
switch (typeof value) {
case 'string': return quote(value); case 'number': return isFinite(value) ? String(value) : 'null'; case 'boolean': case 'null': return String(value); case 'object': if (!value) { return 'null'; }
gap += indent; partial = []; if (Object.prototype.toString.apply(value) === '[object Array]') {
length = value.length; for (i = 0; i < length; i += 1) { partial[i] = str(i, value) || 'null'; }
v = partial.length === 0 ? '[]' : gap ? '[\n' + gap + partial.join(',\n' + gap) + '\n' + mind + ']' : '[' + partial.join(',') + ']'; gap = mind; return v;}
if (rep && typeof rep === 'object') { length = rep.length; for (i = 0; i < length; i += 1) { if (typeof rep[i] === 'string') { k = rep[i]; v = str(k, value); if (v) { partial.push(quote(k) + (gap ? ': ' : ':') + v); } } } } else { for (k in value) { if (Object.prototype.hasOwnProperty.call(value, k)) { v = str(k, value); if (v) { partial.push(quote(k) + (gap ? ': ' : ':') + v); } } } }
v = partial.length === 0 ? '{}' : gap ? '{\n' + gap + partial.join(',\n' + gap) + '\n' + mind + '}' : '{' + partial.join(',') + '}'; gap = mind; return v; } }
if (typeof JSON.stringify !== 'function') {
JSON.stringify = function (value, replacer, space) {
var i; gap = ''; indent = ''; if (typeof space === 'number') { for (i = 0; i < space; i += 1) { indent += ' '; } } else if (typeof space === 'string') { indent = space; }
rep = replacer; if (replacer && typeof replacer !== 'function' && (typeof replacer !== 'object' || typeof replacer.length !== 'number')) { throw new Error('JSON.stringify'); }
return str('', { '': value });};}
if (typeof JSON.parse !== 'function') {
JSON.parse = function (text, reviver) {
var j; function walk(holder, key) {
var k, v, value = holder[key]; if (value && typeof value === 'object') {
for (k in value) {
if (Object.prototype.hasOwnProperty.call(value, k)) {
v = walk(value, k); if (v !== undefined) { value[k] = v; } else {
delete value[k];} } } }
return reviver.call(holder, key, value);}
text = String(text); cx.lastIndex = 0; if (cx.test(text)) {
text = text.replace(cx, function (a) {
return '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
});}
if (/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) { j = eval('(' + text + ')'); return typeof reviver === 'function' ? walk({ '': j }, '') : j; }
throw new SyntaxError('JSON.parse');};} } ());
