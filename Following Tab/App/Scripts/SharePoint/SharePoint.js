﻿var MyApp = {
    FwTabMgr: null,
    JssStrings: {},
    JssErrorStrings: {},
    DropListItems: [],
    CurrentParentKey: '',
    FollowingTabLastLoadTime: 0,    // for timing the last triggered reload time
    backgroundObj:{},
    SetAPIConfig: function () {
        CloudStorageData.WebAPIURL = MyApp.GetParameterByName("WebAPIURL");
        CloudStorageData.GetTokenURL = MyApp.GetParameterByName("GetTokenURL");
        CloudStorageData.IdentityToken = MyApp.GetParameterByName("IdentityToken");
    },
    InitLoad: function () {
        top.$page.FollowingWindow.showLoading();
        MyApp.SetAPIConfig();

        CloudStorageData.GetAPIToken(CloudStorageData.GetTokenURL, CloudStorageData.IdentityToken);
        MyApp.GetUserInfoAndGetCulture();
		MyApp.InitFwTabMgr();

        //top.$page.FollowingWindow.showLoading();
    },
    InitFwTabMgr: function () {
        MyApp.FwTabMgr = new maximizer.FollowingTab();

        MyApp.FwTabMgr.on("focus", function () {
            this.hasFocus = true;
            top.$page.FollowingWindow.showLoading();
            // console.log("focus");
        });
        MyApp.FwTabMgr.on("blur", function () {
            this.hasFocus = false;
            // console.log("blur");
        });
        MyApp.FwTabMgr.on("panelCollapse", function (e) {
            //console.log("panelCollapse");
        });
        // Expand following window after being collapsed
        MyApp.FwTabMgr.on("panelExpand", function (e) {
            var key = e.data.key;
            MyApp.CurrentParentKey = key;
            //   console.log("panelExpand");
        });
        MyApp.FwTabMgr.on("parentRecordChange", function (e) {  // select another abentry
            var key = e.data.key;
            MyApp.CurrentParentKey = key;
            setTimeout(function () { MyApp.LoadFollowingTab(); }, 10);
        });
        MyApp.FwTabMgr.on("parentRecordUpdate", function (e) {  //press reload button 
            var key = e.data.key;
            MyApp.CurrentParentKey = key;
            setTimeout(function () { MyApp.LoadFollowingTab(); }, 10);
        });
        
        MyApp.FwTabMgr.start();
    },
    LoadFollowingTab: function (number) {
        if (MyApp.FollowingTabLastLoadTime == 0) {  // first time
            MyApp.FollowingTabLastLoadTime = new Date() - 1001;
        }
        
        var now = new Date();
        var timeDiff = now - MyApp.FollowingTabLastLoadTime; 
        // do Load only if this function is called again more than within 0.1s later than last time
        if (timeDiff > 1000) {
            top.$page.FollowingWindow.showLoading();
            MyApp.HideAllPage();
            MyApp.FollowingTabLastLoadTime = new Date();
            MyApp.EnableFollowingTabs();
        }
    },
    GetLocalization: function (data) {
        if (data.Code !== -1 && typeof (data.Data) !== 'undefined' && typeof (data.Data.User) !== null) {
            var obj = new CloudStorageData.GetCultureLocalizationObject();
            var serachObj = new CloudStorageData.SearchQueryObject(data.Data.User.Key.UID);
            obj.ConfigurationSetting.Criteria.SearchQuery = serachObj;
            CloudStorageData.RequestAPI('Read', obj, MyApp.SetLocaliZation);
        }
    },
    SetLocaliZation: function (data) {
        if (data.Code !== -1 && typeof (data.ConfigurationSetting) !== 'undefined' && typeof (data.ConfigurationSetting) !== null) {
            if (data.ConfigurationSetting.Data.length > 0) {
                var culture = data.ConfigurationSetting.Data[0].TextValue;
                var script = document.createElement('script');
                script.src = "KendoCulture/Index?Culture=" + culture;
                document.getElementsByTagName('head')[0].appendChild(script);
            }
        }
    },
    GetUserInfoAndGetCulture: function () {
        var obj = new CloudStorageData.GetUserInfoObject();
        CloudStorageData.RequestAPI('GetSessionInfo', obj, MyApp.GetLocalization);
    },
    Item: function () {
        var obj = {
            'refId': "",
            'createdDate': "",
            'modifiedDate': "",
            'path': "",
            'name': "",
            'key': "",
            'brokenFlag': "",
            'parent_key': ""
        };
        return obj;
    },
    RecordFormatter: function (data, parentKey) {
        var returnObj = [];
        for (var i = 0; i < data.length; i++) {
			var item = MyApp.Item();

			// id path overflow
			item.refId = data[i]['Description'] == "Document ID of abEntry" ? data[i]['Text1'] : data[i]['Text1'] + data[i]['Description'];
            item.createdDate = data[i]['DateTime1'];
			item.modifiedDate = data[i]['DateTime2'];
			// folder path overflow
			item.path = data[i]['Text4'] == "" ? data[i]['Text3'] : data[i]['Text3'] + data[i]['Text4'];			
            item.name = data[i]['Text2'];
            item.key = data[i]['Key'];
			item.brokenFlag = data[i]['Number1'];
            item.parent_key = (parentKey == undefined ? data[i]['Parent'] : parentKey);
            returnObj.push(item);
        }
        return returnObj;
    },
    /*
    RecordsSetFormation: function (abEntryKeys, callback) {
        var dbDataSet = "";
        for (var i = 0; i < abEntryKeys['AbEntry']['Data']["length"]; i++) {
            var key = abEntryKeys['AbEntry']['Data'][i]['Key'];
            if (key != '') {
                callSpDocument(key).then(function (records) {
                    if (dbDataSet == "") {
                        dbDataSet = MyApp.RecordFormatter(records['CustomChild']['Data']);
                    } else {
                        var dbDataForThisEntry = MyApp.RecordFormatter(records['CustomChild']['Data']);
                        Array.prototype.push.apply(dbDataSet, dbDataForThisEntry);  // merge two array to dbDataSet
                    }

                    if (i == abEntryKeys['AbEntry']['Data']["length"]) { callback(dbDataSet); }
                })
            } else {
                //MyApp.BindGrid("");
                top.$page.FollowingWindow.hideLoading();
            }
        }
    },
    */
    DisplayTableRecords: function (abEntryKeys) {
        if (abEntryKeys != null) {   // show all function
            MyApp.HideErrorMsg();
            MyApp.HideSyncPage();

            /*
            MyApp.RecordsSetFormation(abEntryKeys, function (dbDataSet) {
                if (!$('#addDocument')[0].disabled)
                    MyApp.BindGrid(dbDataSet); // pass the metadata that is stored in DB to populate the table
                    top.$page.FollowingWindow.hideLoading();
                    //MyApp.BackgroundUpdateSPRecords(dbDataSet);
            })
            */
            var docIds = "";
            for (var i = 0; i < Object.keys(abEntryKeys['AbEntry']['Data']).length; i++) {
                if (i < Object.keys(abEntryKeys['AbEntry']['Data']).length - 1) {
                    docIds += abEntryKeys['AbEntry']['Data'][i]['Key'] + ',';
                } else {
                    docIds += abEntryKeys['AbEntry']['Data'][i]['Key'];
                }
            }
            docIds = docIds.split(',');
            getCustomChildFamily(docIds).then(function (records) {
                var dbDataSet = MyApp.RecordFormatter(records['CustomChild']['Data']);
                if (!$('#addDocument')[0].disabled)
                    MyApp.BindGrid(dbDataSet); // pass the metadata that is stored in DB to populate the table
                top.$page.FollowingWindow.hideLoading();
                MyApp.BackgroundUpdateSPRecords(dbDataSet);
            });
        } else {
            //app.initialize();
            MyApp.HideSyncPage();
            MyApp.ShowGridPage();
            MyApp.ClearGrid();

            if (MyApp.CurrentParentKey !== '') {
                callSpDocument(MyApp.CurrentParentKey).then(function (records) {
                    var dbDataSet = MyApp.RecordFormatter(records['CustomChild']['Data'], MyApp.CurrentParentKey);
                    if (!$('#addDocument')[0].disabled)
                        MyApp.BindGrid(dbDataSet); // pass the metadata that is stored in DB to populate the table
                    top.$page.FollowingWindow.hideLoading();
                    MyApp.BackgroundUpdateSPRecords(dbDataSet);
                })
            } else {
                //MyApp.BindGrid("");
                top.$page.FollowingWindow.hideLoading();
            }
        }
    },
    RefIdsArray: function (dbDataSet, callback) {
        var docIds = "";
        if (Object.keys(dbDataSet).length > 0) {
            if (dbDataSet[0]['Text1'] != undefined) {
                for (var i = 0; i < Object.keys(dbDataSet).length; i++) {
                    if (i < Object.keys(dbDataSet).length - 1) {
                        docIds += '"' + dbDataSet[i]['Text1'] + '",';
                    } else {
                        docIds += '"' + dbDataSet[i]['Text1'] + '"';
                    }
                }
                callback(docIds = '[' + docIds + ']');
            } else if (dbDataSet[0]['refId'] != undefined) {
                for (var i = 0; i < Object.keys(dbDataSet).length; i++) {
                    if (i < Object.keys(dbDataSet).length - 1) {
                        docIds += '"' + dbDataSet[i]['refId'] + '",';
                    } else {
                        docIds += '"' + dbDataSet[i]['refId'] + '"';
                    }
                }
                callback(docIds = '[' + docIds + ']');
            } else {
                jqAlert('Error', "MyApp.RefIdsArray: Invalid dataSet format!");
            }
        } else {
            callback("");
        }
    },
    BackgroundUpdateSPRecords: function (dbDataSet) {
        if (MyApp.CurrentParentKey !== '') {
            var obj = new CloudStorageData.RequestDataObj();
            obj.MXAPIURL = CloudStorageData.WebAPIURL;
            obj.CurrentParentKey = MyApp.CurrentParentKey;
            //CloudStorageData.SPDocIds(MyApp.CurrentParentKey, function (docIds) {
            MyApp.RefIdsArray(dbDataSet, function (docIds) {
                obj.SPDocIds = docIds;
				MyApp.backgroundObj = obj;
				CloudStorageData.GetMyAppData(obj, function (json) {
					if (json['error'][0] == undefined) {
						if (json.response != undefined) {
							var ceDataSet = json.response;
							$.each(dbDataSet, function (index, dbObj) {
								if (dbObj["brokenFlag"] != 1)
									MyApp.CompareAndUpdateRecord(dbObj, ceDataSet);
							})
						}
					}
                });
                //Remove the part getting document library and subsite
                /*getDefaultFolderPathAndSubsite().then(function (res) {
                    if (res['Code'] == 0) {
                        if (res['Custom']['Data'][0] != null) {
                            if (res['Custom']['Data'][0]['Text2'] != "") {
                                obj.Subsite = res['Custom']['Data'][0]['Text2'];    //get subsite
                            }
                        }
                        
                    } else {
                        console.log("MyApp.BackgroundUpdateSPRecords: " + res['Msg'][0]['Message']);
                    }
                }).catch(function (err) {
                    console.log("MyApp.BackgroundUpdateSPRecords: " + err);
                });*/
            });
            //});
        }
    },

    // 
	CompareAndUpdateRecord: function (dbObj, ceDataSet) {
        var id = dbObj['refId'];
        var ceObj=null;
        for (var i = 0; i < Object.keys(ceDataSet).length; i++) {   //get the matched data
            if (ceDataSet[i]['id'] == id) {
                ceObj = ceDataSet[i];
                break;
			}
			if (ceDataSet[i]['refId'] == id) {
				ceObj = ceDataSet[i];
				break;
			}
		}
		if (ceObj['statusCode'] != "429") {// broken link: when receive 429
			if (ceObj == null || ceObj['statusCode'] != null) {// broken link: when receive 404 from ce
				//if (ceObj['error'].indexOf('404') != -1) {
				if (ceObj == null || (ceObj['statusCode'] != null && ceObj['statusCode'].indexOf('NotFound') != -1)) {
					// set the row to red and set the database flag
					// update db record

					updateSpDocument(dbObj['key'], dbObj['refId'], dbObj['name'], dbObj['path'], dbObj["createdDate"], dbObj['modifiedDate'], true).then(function (response) {
						if (response['Code'] != 0) {
							jqAlert("Update Fail", "Update link record for document '" + dbObj['name'] + "' failed. Please reload the following tab.");
						} else {
							// update table row to red color
							MyApp.UpdateTableRow(dbObj['refId'], response['CustomChild']['Data']);
						}
					}).catch(function (err) {
						console.log("MyApp.CompareAndUpdateRecord: [ceObj['statusCode'] != null]: " + err);
					})
				} else {
					console.log("MyApp.CompareAndUpdateRecord: [ceObj['statusCode'] != null]: " + ceObj['error']);
				}
			} else {
				var isModified = false;


				if (ceObj['id'] != dbObj['refId']) isModified = true;
				if (currentCloudStorage == CloudStorageName.OneDrive || currentCloudStorage == CloudStorageName.OneDriveForBusiness) {

					if ((parseInt(ceObj['createdDate']) / 1000 | 0) * 1000 != new Date(dbObj['createdDate']).getTime()) isModified = true;
					if ((parseInt(ceObj['modifiedDate']) / 1000 | 0) * 1000 != new Date(dbObj['modifiedDate']).getTime()) isModified = true;
				}
				if (ceObj['path'] != dbObj['path']) isModified = true;
				if (ceObj['name'] != dbObj['name']) isModified = true;

				if (isModified) {
					// update db record
					updateSpDocument(dbObj['key'], ceObj['id'], ceObj['name'], ceObj['path'], ceObj["createdDate"], ceObj['modifiedDate'], false).then(function (response) {
						if (response['Code'] != 0) {
							jqAlert("Update Fail", "Update link record for document '" + ceObj['name'] + "' failed. Please reload the following tab.");
						} else {
							//update table row
							MyApp.UpdateTableRow(ceObj['id'], response['CustomChild']['Data']);
						}
					}).catch(function (err) {
						console.log("MyApp.CompareAndUpdateRecord: [ceObj['error'] == null]: " + err);
					});
				}
			}
		}
    },
    UpdateTableRow: function (currentRefId, response) {
        var gridDataSet = $("#app-grid").data("kendoGrid").dataSource.data();
        $.each(gridDataSet, function (index, obj) {
            if (obj['refId'] == currentRefId) {
                var grid = $("#app-grid").data("kendoGrid");
                var dataItem = grid.dataSource.data()[index];
                dataItem.set("refId", response['Text1']);
                dataItem.set("name", response['Text2']);
                dataItem.set("createdDate", response["DateTime1"]);
                dataItem.set("modifiedDate", response['DateTime2']);
                dataItem.set("path", response['Text3']);
                dataItem.set("key", response['Key'])
                dataItem.set("brokenFlag", response['Number1']);
            }
        })
     },
    EnableFollowingTabs: function () {
        var customRecord = getDefaultFolderPathAndSubsite()
            .then(function (res) {
				if (res['Code'] == 0) {
					if (userReadResponse.Code == 0) {
						username = userReadResponse.User.Data[0]['DisplayName'];
						if (userRole == '') userRole = userReadResponse.User.Data[0].Role.Administrator ? 'admin' : 'user';
						if (userRole == 'admin') { $('#setUpButton').css("display", "block"); }
						else { $('#setUpButton').css("display", "none"); }
					} else { alert("Error:" + userReadResponse['Msg'][0]['Message']); }
                    if (res['Custom']['Data'][0] == null || res['Custom']['Data'][0] == undefined || MyApp.CurrentParentKey == "") {
                        $('#addDocument')[0].disabled = true;
                        $('#addExisting')[0].disabled = true;
                        $('#delete')[0].disabled = true;
                        //$('#properties')[0].disabled = true;
						$('#folder')[0].disabled = true;
                        MyApp.ShowErrorPage();  // notify the admin setup the subsite and folder befor them are set.
                    } else {
                        $('#addDocument')[0].disabled = false;
                        $('#addExisting')[0].disabled = false;
                        $('#delete')[0].disabled = false;
                        //$('#properties')[0].disabled = false;
						$('#folder')[0].disabled = false;
						if (top.$page.FollowingWindow.currentModule != "opp") {
							getShowAll(userReadResponse.User.Data[0].Key.Value).then(function (res) {
								if (res['Custom']['Data'][0] != null) {
									document.getElementById("showAll").checked = (res['Custom']['Data'][0]['Text1'] == "true");
								}
								MyApp.HideErrorPage();
								showAll();
							});
						} else {
							MyApp.HideErrorPage();
							showAll();
						}
                    }
                }
            }).catch(function (err) {
                console.log("MyApp.EnableFollowingTabs : " + err);
            })
    },
    OpenDispForm: function (refId) {
        var obj = new CloudStorageData.RequestDataObj();
        obj.MXAPIURL = CloudStorageData.WebAPIURL;
        obj.CurrentParentKey = MyApp.CurrentParentKey;
        CloudStorageData.GetConfiguration1(obj, function (configRes) {
            if (configRes['StatusCode'] == null) {  // work normally
                var site = configRes['configuration']['sharepoint.site.address'];
                var subsite = "";
                var library = configRes['configuration']['sharepoint.document.library'];
                getDefaultFolderPathAndSubsite()
                    .then(function (res) {
                        if (res['Code'] == 0) {
                            if (res['Custom']['Data'][0] != null) {
                                if (res['Custom']['Data'][0]['Text2'] != "")
                                    subsite = "/" + res['Custom']['Data'][0]['Text2'];
                            }
                        }
                        $('body').removeClass('waiting');
                        var url = "https://" + site + subsite + "/" + library + "/Forms/DispForm.aspx?ID=" + refId;
                        window.open(url, '_blank');
                    }).catch(function (err) {
                        $('body').removeClass('waiting');
                        console.log("MyApp.OpenDispForm : " + err);
                    });
            } else {
                jqAlert(configRes['StatusCode'], 'MyApp.OpenDispForm: ' + configRes['message']);
                $('body').removeClass('waiting');
            }
        });
    },
    OpenFolder: function (refId) {
        var url = "";
        if (currentCloudStorage == CloudStorageName.OneDrive) {
            url = "https://onedrive.live.com/?id=";

            MyApp.GetFolderPathByRefId("", refId,
                function (json) {
                    if (json['StatusCode'] == null) {   // no error from CE
                        $('body').removeClass('waiting');
                        url = url + json["parentFolderId"];
                        window.open(url, '_blank');
                    } else { // CE return error
                        jqAlert("Error", json['message']);
                        $('body').removeClass('waiting');
                    }
                })
        } else if (currentCloudStorage == CloudStorageName.Box) {
            url = "https://app.box.com/folder/";

            MyApp.GetFolderPathByRefId("", refId,
                function (json) {
                    if (json['StatusCode'] == null) {   // no error from CE
                        $('body').removeClass('waiting');
                        url = url + json["parentFolderId"];
                        window.open(url, '_blank');
                    } else { // CE return error
                        jqAlert("Error", json['message']);
                        $('body').removeClass('waiting');
                    }
                })
        } else if (currentCloudStorage == CloudStorageName.OneDriveForBusiness) {
            MyApp.getSpDocMetaData(refId, function (docData) { //get doc's metadata to get parentId
                MyApp.getSpDocMetaData(docData.parentFolderId, function (parentData) {
                    $('body').removeClass('waiting');
                    window.open(parentData.webUrl, '_blank');
                    return;
                })
            })
        } else if (currentCloudStorage == CloudStorageName.GoogleDrive) {
            var url = "https://drive.google.com/drive/u/0/folders/";
            MyApp.GetFolderPathByRefId("", refId,
                function (json) {
                    if (json['StatusCode'] == null) {   // no error from CE
                        $('body').removeClass('waiting');
                        url = url + json["parentFolderId"];
                        window.open(url, '_blank');
                    } else { // CE return error
                        jqAlert("Error", json['message']);
                        $('body').removeClass('waiting');
                    }
                })
        } else if (currentCloudStorage == CloudStorageName.Dropbox) {
            var url = "https://www.dropbox.com/personal/[folderPath]";
            MyApp.GetFolderPathByRefId("", encodeURI(refId),
                function (json) {
                    if (json['StatusCode'] == null) {   // no error from CE
                        $('body').removeClass('waiting');
                        //------------------
                        var folderpath = encodeURIComponent(json["path"].substr(1));
                        folderpath = folderpath.split("%2F").join("/");
                        if (folderpath.lastIndexOf(".") != -1)
                            folderpath = folderpath.substr(0, folderpath.lastIndexOf("/"));
                        url = url.replace("[folderPath]", folderpath);
                        //------------------
                        //url = url + json["path"].substring(0, json["path"].lastIndexOf('/'));
                        window.open(url, '_blank');
                    } else { // CE return error
                        jqAlert("Error", json['message']);
                        $('body').removeClass('waiting');
                    }
                })
        } else if (currentCloudStorage == CloudStorageName.Amazon) {
            //request to fetch parameter of Element of CloudElement
            var request = new XMLHttpRequest();
            request.onreadystatechange = function (request) {
                request = request.target;
                if (request.readyState === 4) {
                    var json = JSON.parse(request.response);
                    var url = "";
                    //bucket name is used in  folder path uri
                    if (json["filemanagement.provider.bucket_name"] != undefined && json["filemanagement.provider.region_name"] != undefined)
                        url = "https://s3.console.aws.amazon.com/s3/buckets/" + json["filemanagement.provider.bucket_name"] + "/[folderPath]/?region=" + json["filemanagement.provider.region_name"] + "&tab=overview";
                    var parRefId = refId;
                    parRefId = parRefId.substr(5);
                    parRefId = encodeURIComponent(parRefId);
                    MyApp.GetFolderPathByRefId("", parRefId,
                        function (json) {
                            if (json['StatusCode'] == null) {   // no error from CE
                                $('body').removeClass('waiting');
                                var folderpath = encodeURIComponent(json["path"].substr(1));
                                folderpath = encodeURIComponent(folderpath);
                                folderpath = folderpath.split("%252F").join("/");
                                if (folderpath.lastIndexOf(".")!=-1)
                                    folderpath = folderpath.substr(0, folderpath.lastIndexOf("/"));
                                url = url.replace("[folderPath]", folderpath);
                                window.open(url, '_blank');
                            } else { // CE return error
                                jqAlert("Error", json['message']);
                                $('body').removeClass('waiting');
                            }
                        })
                }
            }
            getInstanceID().then(function (json) {
                if (json['Code'] == 0) {
                    var instanceID = json.ConfigurationSetting.Data[0].TextValue;
                    getMaxToken(
                        function (token) {
                            MaxToken = token;
                            //get default path
                            var requestUrl = CloudStorageData.GetControllerUrl(true) + "/api/getInstanceProperty?InstanceID=" + instanceID + "&MxToken=" + token + "&MxAPIURL=" + urlVars["WebAPIURL"];
                            request.open("GET", requestUrl);
                            var response = request.send();

                        }, function (ex) { callback('', true, ex); }
                    );
                }
            });
        } else if (currentCloudStorage == CloudStorageName.DropboxBusiness) {
            var url = "https://www.dropbox.com/work/[refID]"

            var jUrl = CloudStorageData.GetControllerUrl(true) + "/SharePoint/getMember";
            var mxObj = { MxObj: JSON.stringify(MyApp.backgroundObj) }
            MyApp.GetFolderPathByRefId("", encodeURI(refId),
                function (json) {
                    if (json['StatusCode'] == null) {   // no error from CE
                        $('body').removeClass('waiting');
                        var urlRefId = json["path"].replace(/%252F/g, "/");
                        url = url.replace("[refID]", urlRefId);

                        $.ajax({
                            url: jUrl,
                            type: "POST",
                            dataType: "json",
                            async: true,
                            data: mxObj,
                            success: function (Result) {
                                if (Result["StatusCode"] != undefined || Result["StatusCode"] != "Exception") {
                                    var username = Result["profile"]["name"]["given_name"] + " " + Result["profile"]["name"]["surname"];
                                    username = encodeURIComponent(username);
                                    //url = url.replace("[userName]", username);
                                    if (url.lastIndexOf(".") != -1) {
                                        url = url.substr(0, url.lastIndexOf("/"));
                                    }
                                    window.open(url, '_blank');
                                }
                            },
                            error: function (resultJson) {
                                console.log(resultJson);
                            }
                        });
                        //url = url + decodeURI(decodeURI(json["parentFolderId"])).replace(/%2F/g, '/');
                    } else { // CE return error
                        jqAlert("Error", json['message']);
                        $('body').removeClass('waiting');
                    }
                })

        }

        /*else if (currentCloudStorage == CloudStorageName.DropboxBusiness) {
            var url = "https://www.dropbox.com/work/[userName]/[refID]"

            var jUrl = CloudStorageData.GetControllerUrl(true) + "/SharePoint/getMember";
            var mxObj = { MxObj: JSON.stringify(MyApp.backgroundObj) }
            MyApp.GetFolderPathByRefId("", encodeURI(refId),
                function (json) {
                    if (json['StatusCode'] == null) {   // no error from CE
                        $('body').removeClass('waiting');
                        var urlRefId = json["path"].replace(/%252F/g, "/");
                        url = url.replace("[refID]", urlRefId);

                        $.ajax({
                            url: jUrl,
                            type: "POST",
                            dataType: "json",
                            async: true,
                            data: mxObj,
                            success: function (Result) {
                                if (Result["StatusCode"] != undefined || Result["StatusCode"] !="Exception") {
                                    var username = Result["profile"]["name"]["given_name"] + " " + Result["profile"]["name"]["surname"];
                                    username = encodeURIComponent(username);
                                    url = url.replace("[userName]", username);
                                    if (url.lastIndexOf(".") != -1) {
                                        url = url.substr(0,url.lastIndexOf("/"));
                                    }
                                    window.open(url, '_blank');
                                }
                            },
                            error: function (resultJson) {
                                console.log(resultJson);
                            }
                        });
                        //url = url + decodeURI(decodeURI(json["parentFolderId"])).replace(/%2F/g, '/');
                    } else { // CE return error
                        jqAlert("Error", json['message']);
                        $('body').removeClass('waiting');
                    }
                })

        }*/ else {
            jqAlert("Error", "OpenFolder: invalid currentCloudStorage: " + currentCloudStorage);
            $('body').removeClass('waiting');
            return;
        }
    },
    /*OpenFolder: function (refId) {
        var obj = new CloudStorageData.RequestDataObj();
        obj.MXAPIURL = CloudStorageData.WebAPIURL;
        obj.CurrentParentKey = MyApp.CurrentParentKey;
        CloudStorageData.GetConfiguration1(obj, function (configRes) {
            if (configRes['StatusCode'] == null) {  // work normally
                var site = configRes['configuration']['sharepoint.site.address'];
                var subsite = "";
                var library = configRes['configuration']['sharepoint.document.library'];
                getDefaultFolderPathAndSubsite()
                    .then(function (res) {
                        if (res['Code'] == 0) {
                            if (res['Custom']['Data'][0] != null) {
                                if (res['Custom']['Data'][0]['Text2'] != "")
                                    subsite = "/" + res['Custom']['Data'][0]['Text2'];
                            }
                        }

                        MyApp.GetFolderPathByRefId(res['Custom']['Data'][0]['Text2'], refId,
                            function (json) {
                                if (json['StatusCode'] == null) {   // no error from CE
                                    var selectedFolderPath = json['path'];
                                    selectedFolderPath = selectedFolderPath.substring(0, selectedFolderPath.lastIndexOf('/'));

                                    var siteAddress = configRes['configuration']['sharepoint.site.address'];
                                    if (siteAddress.indexOf('/sites') != -1) {  //non root site
                                        siteAddress = siteAddress.substring(siteAddress.indexOf('/sites'), siteAddress.length);
                                    } else {    //root site
                                        siteAddress = "";
                                    }

                                    var encodedPath = "";
                                    if (subsite != "") {
                                        encodedPath = encodeURIComponent(siteAddress + subsite + "/" + library + selectedFolderPath);
                                    } else {
                                        encodedPath = encodeURIComponent(siteAddress + "/" + library + selectedFolderPath);
                                    }
                                    $('body').removeClass('waiting');
                                    var url = "https://" + site + subsite + "/" + library + "/Forms/AllItems.aspx?id=" + encodedPath;
                                    window.open(url, '_blank');
                                } else { // CE return error
                                    jqAlert("Error", json['message']);
                                    $('body').removeClass('waiting');
                                }
                            })
                    }).catch(function (err) {
                        $('body').removeClass('waiting');
                        console.log("MyApp.OpenFolder : " + err);
                    });
            } else {
                jqAlert(configRes['StatusCode'], 'MyApp.OpenFolder: ' + configRes['message']);
                $('body').removeClass('waiting');
            }
        });
    },
    */
    GetFolderPathByRefId: function (subsite, fileid, callback) {
        var request = new XMLHttpRequest();
        request.onreadystatechange = function (request) {
            request = request.target;
            if (request.readyState === 4) {
                var json = JSON.parse(request.response);
                callback(json);
                /*
                if (json['Message'] != null && json['Message'].indexOf('error') !== -1) { //contains error
                    callback('', true, "Something went wrong.  Failed to read meta data for this file from SharePoint.");
                } else {
                    callback(json, false);
                }*/
            }
        }
        //get abentry
        getMaxToken(
            function (token) {
                MaxToken = token;
                var requestUrl = CloudStorageData.GetControllerUrl(true) + "/api/readRefIdById?id=" + fileid + "&subsite=" + subsite + "&MxToken=" + token + "&MxAPIURL=" + urlVars["WebAPIURL"];
                request.open("POST", requestUrl);
                request.send();
            }, function (ex) { callback(ex); }
        );
    },
    displayBrokenLinkDialog: function (key, refId, name, path) {
        var dialog = $('<p>It looks like the originally linked Document (referenced by internal ID) can no longer be found.  Would you like to retry loading it or correct the problem by providing a new location of the file?</p>').dialog({
            title: "Linked Document Not Found",
            width: 400,
            buttons: {
                "Retry": function () {
                    dialog.dialog('close');
                    reloadBrokenLink(key, refId);
                },
                "Re-Link": function () {
                    dialog.dialog('close');
                    var defaultDocUrl = path + "/" + name;
                    reLinkDocument(refId, defaultDocUrl);
                },
                "Cancel": function () {
                    dialog.dialog('close');
                }
            }
        });

        // Retry loading of Document by RefId
        function reloadBrokenLink(key, refId) {
            if (refId != "") {
                jQuery("body").showLoading();
                MyApp.getSpDocMetaData(refId, function callback(ceObj, error, msg) {
                    if (!error && ceObj['StatusCode'] == undefined) {
                        // update db record
                        updateSpDocument(key, ceObj['id'], ceObj['name'], ceObj['path'], ceObj["createdDate"], ceObj['modifiedDate'], false).then(function (response) {
                            if (response['Code'] != 0) {
                                jQuery("body").hideLoading();
                                jqAlert("Update Fail", "Update link record for document '" + ceObj['name'] + "' failed.");
                            } else {
                                //update table row
                                MyApp.UpdateTableRow(ceObj['id'], response['CustomChild']['Data']);

                                // go back to downloading the document
                                MyApp.DownLoadSpDoc(ceObj['name'], ceObj['id'], false, key, ceObj['path']);
                                jQuery("body").hideLoading();
                            }
                        }).catch(function (err) {
                            jQuery("body").hideLoading();
                            console.log("MyApp.displayBrokenLinkDialog.reloadBrokenLink: [updateSpDocument]: " + err);
                        });
                    }
                    else {
                        jQuery("body").hideLoading();
                        jqAlert("Retry Failed", "Unable to find the Document.");
                    }
                })
            } else {
                jqAlert("Error", "MyApp.displayBrokenLinkDialog.reloadBrokenLink: empty refId");
            }
        }

        function reLinkDocument(refId, spDocUrl) {  // open addexisting dialog
            var addExistingPageURL = rootPath + integrationsModule + "/Documents/AddExisting.html?entryKey=" + $("#app-grid").data("kendoGrid").select()[0].getAttribute("parent") + "&GetTokenURL=" + tokenUrl + "&IdentityToken=" + IdentityToken + "&WebAPIURL=" + CloudStorageData.WebAPIURL + "&CloudStorage=" + currentCloudStorage + "&flag=update" + "&Leader=" + username;
			tmpKey = $("#app-grid").data("kendoGrid").select()[0].getAttribute("parent");
            top.CustomDialog.Open('dialog-modal-setup', addExistingPageURL, csu_width, csu_height * 1.03, "Update Link", function (json) {
                if (json != "canceled" && json != "dialog-modal-setup" && typeof (json) != undefined) {
                    // update kendo ui datasource
                    MyApp.UpdateTableRow(refId, json['CustomChild']['Data']);

                    // delete existing link record in db
                    MyApp.RemoveSPLink(refId, false, true, tmpKey);
                }
            });
        }
    },
    // Response with callback(jsonResp, error, msg)
    getSpDocMetaData: function (refId, callback) {
        if(currentCloudStorage == CloudStorageName.Dropbox || currentCloudStorage == CloudStorageName.DropboxBusiness) {
            refId = encodeURIComponent(refId);
        }

        var request = new XMLHttpRequest();
        request.onreadystatechange = function (request) {
            request = request.target;
            if (request.readyState === 4) {
                var json = JSON.parse(request.response);
                if (json['message'] != null && json['requestId'] != null) { //contains error
                    callback('', true, json['message']);
                } else {
                    callback(json, false);
                }
            }
        }

        getDefaultFolderPathAndSubsite().then(function (res) {
            if (res['Code'] == 0) {
                var path;
                var subsite;
                if (res['Custom']['Data'][0]['Text2'] != null)
                    subsite = res['Custom']['Data'][0]['Text2'];
                if (res['Custom']['Data'][0]['Text1'] != undefined || res['Custom']['Data'][0]['Text1'] != null || res['Custom']['Data']['Text1'] != undefined || res['Custom']['Data']['Text1'] != null) {

                    CloudStorageData.WebApiToken(function (token) {
                        var requestUrl = CloudStorageData.GetControllerUrl(true) + "/api/readRefIdById?id=" + refId + "&subsite=" + subsite + "&MxToken=" + token + "&MxAPIURL=" + CloudStorageData.WebAPIURL;
                        request.open("POST", requestUrl);
                        request.send();
                    })
                }
            } else {
                callback('', true, res['Msg'][0]['Message']);
            }
        }).catch(function (err) {
            callback('', true, err.message);
        });
    },
    DownLoadDocument: function (docName, refId) {
        var obj = new CloudStorageData.GenerateDownloadMyAppRequestObj();
        obj.MXAPIURL = CloudStorageData.WebAPIURL;
        obj.SPDocIds = CloudStorageData.SPDocIds;
        obj.DocumentID = docName;
		obj.DocumentNumber = refId;
		obj.TypeNumber = refId;
        CloudStorageData.WebApiToken(function (token) {
            obj.MXAPIToken = token;
            var subsite = "";
            getDefaultFolderPathAndSubsite()
                .then(function (res) {
                    if (res['Code'] == 0) {
                        if (res['Custom']['Data'][0] != null) {
                            if (res['Custom']['Data'][0]['Text2'] != "")
                                obj.Subsite = res['Custom']['Data'][0]['Text2'];
                        }
                    }

                    var jsObj = JSON.stringify(obj)
                    CloudStorageData.DownLoadFile(jsObj);
                }).catch(function (err) {
                    console.log("MyApp.DownLoadSpDoc : " + err);
                })
        })
    },
    OpenOfficeFile: function (docName, refId) {
        var url = "";
        if (currentCloudStorage == CloudStorageName.OneDrive) {
            url = "https://onedrive.live.com/edit.aspx?resid=" + refId;
            window.open(url, '_blank');
        } else if (currentCloudStorage == CloudStorageName.OneDriveForBusiness) {
            MyApp.getSpDocMetaData(refId, function (data) {
                url = data.webUrl;
                window.open(url, '_blank');
            })
        } else if (currentCloudStorage == CloudStorageName.Dropbox) {
            MyApp.getSpDocMetaData(refId, function (data) {
                url = "https://www.dropbox.com/ow/msft/edit/personal" + data.path;
                window.open(url, '_blank');
            })
        } else if (currentCloudStorage == CloudStorageName.DropboxBusiness) {
            MyApp.getSpDocMetaData(refId, function (data) {
                url = "https://www.dropbox.com/ow/msft/edit/work" + data.path;
                window.open(url, '_blank');
            })
        } else {
            jqAlert("Error", "OpenOfficeFile: invalid currentCloudStorage: " + currentCloudStorage);
            //$('body').removeClass('waiting');
            return;
        }
        /*
        var obj = new CloudStorageData.RequestDataObj();
        obj.MXAPIURL = CloudStorageData.WebAPIURL;
        obj.SPDocIds = CloudStorageData.SPDocIds;
        obj.CurrentParentKey = MyApp.CurrentParentKey;

        CloudStorageData.GetConfiguration1(obj, function (configRes) {
            var subsite = "";
            getDefaultFolderPathAndSubsite()
                .then(function (res) {
                    if (res['Code'] == 0) {
                        if (res['Custom']['Data'][0] != null) {
                            if (res['Custom']['Data'][0]['Text2'] != "")
                                subsite = "/" + res['Custom']['Data'][0]['Text2'];
                        }
                    }

                    var fileType = "";
                    if (ext == "docx") {
                        fileType = "/:w:";
                    } else if (ext == "xlsx") {
                        fileType = "/:x:";
                    } else if (ext == "pptx") {
                        fileType = "/:p:";
                    }
                    var url = "";
                    var siteAddress = configRes['configuration']['sharepoint.site.address'];
                    if (siteAddress.indexOf('/sites') != -1) {  //non root site
                        var rootAddress = siteAddress.substring(0, siteAddress.indexOf('/sites'));
                        siteAddress = siteAddress.substring(siteAddress.indexOf('/sites'), siteAddress.length);
                        url = encodeURI("https://" + rootAddress + fileType + "/r" + siteAddress + subsite + "/_layouts/15/Doc.aspx?sourcedoc={" + refId + "}");
                    } else {    //root site
                        url = encodeURI("https://" + siteAddress + fileType + "/r" + subsite + "/_layouts/15/Doc.aspx?sourcedoc={" + refId + "}");
                    }

                    window.open(url, '_blank');
                }).catch(function (err) {
                    console.log("MyApp.DownLoadSpDoc : " + err);
                })
        })
        */
    },
    // n is document name
    // d is ref ID
    DownLoadSpDoc: function (n, d, brokenFlag, key, path) {
        // if the linked is broken, prompt user with dialog instead of downloading file
        if (brokenFlag)
        {
            MyApp.displayBrokenLinkDialog(key, d, n, path);
            return;
        }
        var ext = n.split('.').pop();

        if (currentCloudStorage != CloudStorageName.OneDrive && currentCloudStorage != CloudStorageName.OneDriveForBusiness && currentCloudStorage != CloudStorageName.Dropbox && currentCloudStorage != CloudStorageName.DropboxBusiness) {
            MyApp.DownLoadDocument(n, d);
        } else {
            if (ext != "docx" && ext != "xlsx" && ext != "pptx") {  // dowload the file
                MyApp.DownLoadDocument(n, d);
            } else {    // redirect to sp 365 online
                MyApp.OpenOfficeFile(n, d);
            }
        }
    },
    GetSPDocKey: function (refId, customChildRecords, callback) {
		var pos = -1;
		for (var i = 0; i < Object.keys(customChildRecords['CustomChild']['Data']).length; i++) {
			var text1 = customChildRecords['CustomChild']['Data'][i]['Description'] == "Document ID of abEntry" ? customChildRecords['CustomChild']['Data'][i]['Text1'] : customChildRecords['CustomChild']['Data'][i]['Text1'] + customChildRecords['CustomChild']['Data'][i]['Description'];
			if (text1 == refId) {
				pos = i;
				break;
			}
		}

        if (pos != -1) {
            if (customChildRecords['CustomChild']['Data'][pos]['Key'] != null || customChildRecords['CustomChild']['Data'][pos]['Key'] != undefined) {
                callback(customChildRecords['CustomChild']['Data'][pos]['Key'], customChildRecords['CustomChild']['Data'][pos]['Text2']);
            } else {
                callback('error');
            }
        } else {
            callback('error');
        }
    },
    RemoveSPLink: function (refId, isRemoveSPDoc, suppressAlert, parentKey) {
        if (parentKey == undefined || parentKey == '')
            parentKey = MyApp.CurrentParentKey;
        callSpDocument(parentKey)
            .then(function (res) {
                if (res['Code'] == 0) {
                    MyApp.GetSPDocKey(refId, res, function (key, filename) {
                        if (key.indexOf('error') == -1) {
                            delSpDocument(key)
                                .then(function (result) {
                                    if (result['Code'] == 0) {
                                        removeRecord();
                                        if (isRemoveSPDoc) {
                                            jQuery('body').showLoading();
                                            MyApp.RemoveSPDoc(refId, filename);
                                        } else {
                                            if (!suppressAlert) {
                                                //note logging
                                                var text = "Remove Link from " + cloudStorageDisplayStr + "<br>File name: " + filename + "<br>Leader: " + username + "<br>Action: Remove Link"
                                                createNote(MyApp.CurrentParentKey, text, '');
                                                jqAlert('Deletion Success', 'The link has been removed.');
                                            }
                                        }
                                    } else { jqAlert('Request Fails', 'RemoveSPLink: (delSpDocument)' + result['Msg'][0]['Message']); }
                                }).catch(function (err) {
                                    console.log("MyApp.RemoveSPLink: [delSpDocument]: " + err);
                                });
                        } else { //jqAlert('Error', 'Retriving Key Fails!');
                            jqAlert('Error','Please choose the correct Address Book Entry.')
                        }
                    })
                } else { jqAlert('Request Fails', 'RemoveSPLink: (callSpDocument)' + res['Msg'][0]['Message']); }
            }).catch(function (err) {
                console.log("MyApp.RemoveSPLink: [callSpDocument]: " + err);
            });
    },
    RemoveSPDoc: function (refId, filename) {
        var obj = new CloudStorageData.RequestDataObj();
        obj.MXAPIURL = CloudStorageData.WebAPIURL;
        obj.CurrentParentKey = MyApp.CurrentParentKey;

        var parRefId = refId;
        if (currentCloudStorage == "AMAZON" ) {
            parRefId = parRefId.substr(5);
            parRefId = encodeURIComponent(parRefId);
        }else if (currentCloudStorage == CloudStorageName.Dropbox || currentCloudStorage == CloudStorageName.DropboxBusiness) {
            parRefId = encodeURIComponent(parRefId);
        }
        var request = new XMLHttpRequest();
        request.onreadystatechange = function (request) {
            request = request.target;
            if (request.readyState === 4) {
                var jsonObj = JSON.parse(request.response);
                if (jsonObj['StatusCode'] != null) {
                    if (jsonObj['StatusCode'] == '200') {
                        //note logging
                        var text = "Remove Document from " + cloudStorageDisplayStr + "<br>File name: " + filename + "<br>Leader: " + username + "<br>Action: Remove Document";
                        createNote(MyApp.CurrentParentKey, text, '');
                        jQuery('body').hideLoading();
                        jqAlert('Document Removal Success', 'The document has been removed from ' + cloudStorageDisplayStr + '.');
                    } else {
                        jQuery('body').hideLoading();
                        jqAlert('Error', 'MyApp.RemoveSPDoc: [' + jsonObj['StatusCode'] + '] :' + jsonObj['message']);
                    }
                } else {
                    jQuery('body').hideLoading();
                    jqAlert('Error', 'MyApp.RemoveSPDoc: response doesn\'t contain StatusCode!');
                }
            }
        }
        CloudStorageData.GetConfiguration1(obj, function (configRes) {
            if (configRes['StatusCode'] == null) {  // work normally
                var site = configRes['configuration']['sharepoint.site.address'];
                var subsite = "";
                getDefaultFolderPathAndSubsite()
                    .then(function (res) {
                        if (res['Code'] == 0) {
                            if (res['Custom']['Data'][0] != null) {
                                if (res['Custom']['Data'][0]['Text2'] != "")
                                    subsite = res['Custom']['Data'][0]['Text2'];
                            }
                            //send to endpoint
                            var hostname = window.location.hostname;
                        
                            var applicationPath = window.location.pathname.substr(0, window.location.pathname.lastIndexOf('/'));
                            applicationPath = applicationPath.replace('//', '/');
                            while ((applicationPath.match(/[/]/g || []).length) > 1) {
                                applicationPath = applicationPath.substr(0, applicationPath.lastIndexOf('/'));
                            }

                            CloudStorageData.WebApiToken(function (token) {
                                var requestUrl = CloudStorageData.GetControllerUrl(true) + "/api/removeSPDoc?docId=" + parRefId + "&subsite=" + subsite + "&MxToken=" + token + "&MxAPIURL=" + obj.MXAPIURL;
                                request.open("POST", requestUrl);
                                request.send();
                            })
                        } else {
                            jqAlert('Request Fails', 'MyApp.RemoveSPDoc(getDefaultFolderPathAndSubsite): ' + res['Msg'][0]['Message']);
                        }
                    }).catch(function (err) {
                        console.log("MyApp.RemoveSPDoc: " + err);
                    });
            } else {
                jqAlert(configRes['StatusCode'], 'MyApp.RemoveSPDoc: ' + configRes['message']);
                jQuery('body').hideLoading();
            }
        });
    },
    GetFolderPath: function (folderPath) {
        //if (folderPath.length > 1) 
         //   folderPath = folderPath.substring(0, folderPath.lastIndexOf('/'));
        return folderPath;
    },
	isChrome: function (userAgent) {
		var isEdge = (/Edge[\/](\d+\.\d+)/.test(userAgent));
		var ischrome = userAgent.lastIndexOf('Chrome/') > 0 && !isEdge;

		if (ischrome || navigator.userAgent.match('CriOS'))
			return true;
		else
			return false;
    },
	PopulatingGrid: function (data) {
		var items = MyApp.RetriveMyAppsData(data);
		MyApp.BindGrid(items);
	},
	RetriveMyAppsData: function (data) {
		var items = [];

		if (data.InvoiceData.response != undefined && data.InvoiceData.response.length > 0) {
			items = items.concat(data.InvoiceData.response);
		}
		if (data.OrderData.response != undefined && data.OrderData.response.length > 0) {
			items = items.concat(data.OrderData.response);
		}

		if (data.CustomerPaymentData.response != undefined && data.CustomerPaymentData.response.length > 0) {
			items = items.concat(data.CustomerPaymentData.response);
		}
		if (data.CreditRefundData.response != undefined && data.CreditRefundData.response.length > 0) {
			items = items.concat(data.CreditRefundData.response);
		}
		if (data.CreditSettlementData.response != undefined && data.CreditSettlementData.response.length > 0) {
			items = items.concat(data.CreditSettlementData.response);
		}
		if (data.DocumentsData.response != undefined && data.DocumentsData.response.length > 0) {
			items = items.concat(data.DocumentsData.response);
		}
		if (data.DocumentsDataNew.response != undefined && data.DocumentsDataNew.response.length > 0) {
			items = items.concat(data.DocumentsDataNew.response);
		}
		return items;

	},
	DisplayName: function (entryname) {
		var name = $('.max-app-entry-name');
		name.text(entryname);
	},
	HideAllPage: function () {
		MyApp.HideErrorPage();
		MyApp.HideSyncPage();
		MyApp.HideGridPage();
		MyApp.HideSyncBtn();
		MyApp.HideSyncStatus();
	},
	ShowErrorPage: function (msg) {
        var page = $('#max-app-error-page');
		page.removeClass('hidden');
	},
	HideErrorPage: function (msg) {
		var page = $('#max-app-error-page');
		if (!page.hasClass('hidden')) {
			page.addClass('hidden');
		}
	},
	ShowErrorMsg: function (msg) {
		var msgrow = $('#max-sync-msg');
		var ermsg = $('#max-sync-error-msg-txt');
		var errormsg = (MyApp.JssErrorStrings[msg] === null || typeof (MyApp.JssErrorStrings[msg]) === 'undefined') ? '' : MyApp.JssErrorStrings[msg];
		msgrow.removeClass('hidden');
		ermsg.text(errormsg);
	},
	HideErrorMsg: function (msg) {
		var msgrow = $('#max-sync-msg');
		var ermsg = $('#max-sync-error-msg-txt');
		if (!msgrow.hasClass('hidden')) {
			msgrow.addClass('hidden');
		}
		ermsg.text('');
    },
	ShowSyncPage: function () {
        var syncpage = $("#max-app-sync-page");
		syncpage.removeClass("hidden");
	},
	HideSyncPage: function () {
        var syncpage = $("#max-app-sync-page");
		if (!syncpage.hasClass('hidden')) {
			syncpage.addClass('hidden');
		}
	},
	ShowGridPage: function () {
		var gridpage = $("#max-app-grid-page");
		gridpage.removeClass("hidden");
	},
	HideGridPage: function () {
		var gridpage = $("#max-app-grid-page");
		if (!gridpage.hasClass('hidden')) {
			gridpage.addClass('hidden');
		}
	},
	ShowSyncBtn: function () {
		var syncbtn = $("#max-sync-connect-button");
		syncbtn.removeClass("hidden");
	},
	HideSyncBtn: function () {
		var syncbtn = $("#max-sync-connect-button");
		if (!syncbtn.hasClass('hidden')) {
			syncbtn.addClass('hidden');
		}
	},
	ShowSyncStatus: function () {
		var syncstatus = $("#max-sync-loading-status");
		syncstatus.removeClass("hidden");
	},
	HideSyncStatus: function () {
		var syncstatus = $("#max-sync-loading-status");
		if (!syncstatus.hasClass('hidden')) {
			syncstatus.addClass('hidden');
		}
    },
	GetParameterByName: function (name) {
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
			results = regex.exec(location.search);
		return results === null ? null : decodeURIComponent(results[1].replace(/\+/g, " "));
	},
	GridEmpty: function () {
		var grid = $("#app-grid");
		if (grid.data("kendoGrid") != null) {
			grid.data("kendoGrid").destroy();
			grid.empty();
		}
	},
	ClearGrid: function () {

		var grid = $("#app-grid").data("kendoGrid");
		if (typeof (grid) !== 'undefined' && grid !== null) {
			var dataSource = new kendo.data.DataSource({
				data: []
			});
			grid.setDataSource(dataSource);
		}
	},
	BindGrid: function (data) {
		var grid = $("#app-grid").data("kendoGrid");
		if (typeof (grid) === 'undefined' || grid === null) {
            $("#app-grid").kendoGrid({
                height: 100,    //remember to set height to make vertical scroll work
                width: 100,
				dataBound: function (e) {
					e.sender.pager.element.hide();
				},
				dataSource: {
					data: data,
					sort: ({
						field: "creationDate", dir: "desc"
					})

				},
				sortable: {
					mode: "single",
					allowUnsort: false
                },
                resizable: false,
                scrollable: true,
                selectable: "single, row",
				rowTemplate: kendo.template($("#rowTemplate").html()),
                toolbar: kendo.template($("#template").html()),
                pageable: {
                    refresh: false,
                    previousNext: false,
                    numeric: false,
                    messages: {
                        display: MyApp.JssStrings.TotalCount + ": {2}"
                    }
                },
				columns: MyApp.BindColumns()
			});
			MyApp.BindDropdown();
		} else {
			var dataSource = new kendo.data.DataSource({
				data: data,
				sort: ({
					field: "creationDate", dir: "desc"
				})
			});
			grid.setDataSource(dataSource);
		}

		MyApp.ResizeGrid();
	},
	BindDropdown: function () {
		var grid = $("#app-grid")
		var data = new kendo.data.DataSource({
		    data: MyApp.DropListItems,
			sort: ({
				field: "DisplayValue", dir: "asc"
			})
		});
		grid.find("#quikcbookstype").kendoDropDownList({
			dataTextField: "DisplayValue",
			dataValueField: "Value",
			autoBind: false,
			dataSource: data,
			change: MyApp.DropDownChange
		});
		MyApp.SetDefualtValue();
	},
	DropDownChange: function (e) {
		var grid = $("#app-grid");
		var value = this.value();

		if (value) {
			grid.data("kendoGrid").dataSource.filter({ field: "typeNumber", operator: "eq", value: parseInt(value) });
		} else {
			grid.data("kendoGrid").dataSource.filter({});
		}
	},
	SetDefualtValue: function () {
		var dropdownlist = $("#quikcbookstype").data("kendoDropDownList");
		dropdownlist.value("");
		dropdownlist.trigger("change");
	},
	BindColumns: function () {
        var cols = [
            {
                field: "modifiedDate", width: "85px", title: MyApp.JssStrings.Col_Date
            },
            {
                field: "name", width: "150px", title: MyApp.JssStrings.Col_FileName
            },
            {
                field: "path", width: "305px", title: MyApp.JssStrings.Col_Path
            }
        ]
	    //no createdDate field in Amazon
        if (currentCloudStorage != CloudStorageName.Amazon && currentCloudStorage != CloudStorageName.Dropbox && currentCloudStorage != CloudStorageName.DropboxBusiness) {
            cols.push(
            {
                field: "createdDate", width: "85px", title: MyApp.JssStrings.Col_CreatedOn
            });
            }
		return cols;
    },
	GetMasterColumnsWidth: function (tbl) {
		var result = 0;
		tbl.children("colgroup").find("col").not(":last").each(function (idx, element) {
			result += parseInt($(element).outerWidth() || 0, 10);
		});

		return result;
	},
	GetDetailRowWidth: function (tbl) {
		var detailCell = tbl.closest("td");

		return tbl.width() + parseInt(detailCell.css("padding-left"), 10) + parseInt(detailCell.css("border-left-width"), 10) + parseInt(detailCell.css("border-right-width"), 10) + parseInt(detailCell.css("padding-right"), 10) + detailCell.prev().outerWidth();
	},
	AdjustLastColumn: function () {
		setTimeout(function () {
			var grid = $("#app-grid").data("kendoGrid"),
				masterHeaderTable = grid.thead.parent(),
				masterBodyTable = grid.wrapper.children(".k-grid-content").children("table");

			masterHeaderTable.width("");
			masterBodyTable.width("");

			var headerWidth = MyApp.GetMasterColumnsWidth(masterHeaderTable),
				detailTable = grid.element.find(".k-detail-row").find("table"),
				detailWidth = MyApp.GetDetailRowWidth(detailTable),
				lastHeaderColElement = grid.thead.parent().find("col").last(),
				lastDataColElement = grid.tbody.parent().children("colgroup").find("col").last(),
				delta = parseInt(headerWidth, 10) - parseInt(detailWidth, 10);

			if (delta < 0) {
				delta = Math.abs(delta);
				lastHeaderColElement.width(delta);
				lastDataColElement.width(delta);
			} else {
				lastHeaderColElement.width(0);
				lastDataColElement.width(0);
			}

			var scrollableDiv = grid.wrapper.find(".k-grid-content");
			scrollableDiv.scrollLeft(scrollableDiv.scrollLeft() - 1);
			scrollableDiv.scrollLeft(scrollableDiv.scrollLeft() + 1);
		}, 1);
	},
	ResizeGrid: function () {
		var grid = $("#app-grid").data("kendoGrid");
		if (grid !== null && typeof (grid) !== 'undefined') {
			grid.resize();
		}
	}
}