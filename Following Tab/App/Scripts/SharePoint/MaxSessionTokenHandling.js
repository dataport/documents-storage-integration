﻿/************************************************************************************************
	Maximizer Gmail Gadget
	Released Date: May 6, 2013
	Required Maximizer Version: Maximizer CRM 12 Summer 2012 CTP
	
	Copyright © 1988 - 2013 Maximizer Software Inc.  

	This source code is provided as an example on how to use Maximizer's APIs.
	Anyone is free to copy, modify, publish, use, compile, sell, or distribute
	this source code, either in source code form or as a compiled binary, for
	any purpose, commercial or non-commercial, and by any means.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.

************************************************************************************************/


// Token will timeout after how long?
var tokenTimeout = 1000 * 10;   //  timeout in 10 second

// Global Token variable used by functions getMaxToken() and getNewMaxToken()
var thisToken = {
    strToken: "",
    lastTokenTime: 0,
    setToken: function (token) { this.strToken = token, this.lastTokenTime = (new Date()).getTime() },
    getToken: function () { return this.strToken; },
    isTokenExpired: function () {
        var timeSinceLastToken = (new Date()).getTime() - this.lastTokenTime;
        return timeSinceLastToken > tokenTimeout;
    }
}



// This function is used to chain with the next request async http request.
// The function return a promise object for the next request.
// successGetTokenCallBack will be called back with the actual Token string.
//
// This function checks and see if existing token is still good.  If yes, it will
//	 keep on using existing "currMaxToken".  If not, it will call getNewMaxToken()
//   to get a new one.
function getMaxToken(successGetTokenCallBack) {
    /*if (!tempToken) {
        var apiUrl = window.location.protocol + '//' + window.location.hostname + '/MaximizerWebData';

        //Next, construct the JSON request object
        var jsonRequest = {};
        jsonRequest.Database = 'General';
        jsonRequest.UID = 'MASTER';
        jsonRequest.Password = 'control';
    }
    return;
    */
    
    if (thisToken.isTokenExpired()) {
        return getTokenFromFollowingWindow()
			.then(
				// success
				function (result) {
				    thisToken.setToken(JSON.parse(result).d);
				    return successGetTokenCallBack(thisToken.getToken());
				},

				// failure
				function (result) {
				    return $.Deferred().reject(result);
				}
			);
    } else {
        return successGetTokenCallBack(thisToken.getToken());
    }
}



// **************************************************************
// Get a current session Token for use with the Web Data API.
// This should called for every user action, as you can't rely
// on your token not expiring.
//
// return the response string from HTTP GET request asynchronously with local xmlHTTP object
// **************************************************************
function getTokenFromFollowingWindow() {
    var httpRequest;
    var returnValue;
    var deferred = $.Deferred();
    var urlVars = getUrlVars();


    var path = urlVars["GetTokenURL"];
    var identityToken = urlVars["IdentityToken"];


    //we assume that this file is stored in the Content/FollowingWindows/paramUrlTab folder
    //var path = "../../../Services/Authenticationservice.asmx/GetWebAPIToken";

    if (window.XMLHttpRequest) { // Mozilla, Safari, ...
        httpRequest = new XMLHttpRequest();
    } else if (window.ActiveXObject) { // IE
        try {
            httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e) { }
        }
    }

    if (!httpRequest) {
        alert('This browser does not support AJAX');
        return false;
    }

    httpRequest.open('POST', path, true);
    httpRequest.onreadystatechange = function () {
        if (httpRequest.readyState === 4) {
            if (httpRequest.status === 200) {
                return deferred.resolve(httpRequest.responseText);
            } else {
                var err = {};
                err["Msg"] = "HTTP Error: " + httpRequest.status + ".  There was a problem getting the Web API Token.  Your Maximizer Web Access session probably has expired.";

                return deferred.reject(err);
            }
        }
    }
    httpRequest.setRequestHeader('Content-Type', 'application/json');
    httpRequest.send(JSON.stringify({ 'IdentityToken': identityToken }));
    return deferred;
}


