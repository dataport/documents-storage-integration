﻿var CloudStorageName = {
    OneDrive: "ONEDRIVE",
    Box: "BOX",
    Amazon: "AMAZON",
    Dropbox: "DROPBOX",
    OneDriveForBusiness: "ONEDRIVEBIZ",
    GoogleDrive: "GOOGLEDRIVE",
    DropboxBusiness: "DROPBOXBIZ",
}

var currentCloudStorage = "";
var cloudStorageDisplayStr = "";
var applicationId = "";