﻿var CloudStorageData = {
    WebApiToken: function (callback) {
        getMaxToken(
            function (token) {
                callback(token)
            }
        )
    },
	WebAPIURL: "",
	GetTokenURL: "",
	IdentityToken: "",
    SPDocIds: function (pKey, callback) {
        var docIds = "";
        callSpDocument(pKey).then(function (res) {
            if (res['Code'] == 0) {
                if (Object.keys(res['CustomChild']['Data']).length > 0) {
                    for (var i = 0; i < Object.keys(res['CustomChild']['Data']).length; i++) {
                        if (i < Object.keys(res['CustomChild']['Data']).length - 1) {
                            docIds += '"' + res['CustomChild']['Data'][i]['Text1'] + '",';
                        } else {
                            docIds += '"' + res['CustomChild']['Data'][i]['Text1'] + '"';
                        }
                    }
                    callback(docIds = '[' + docIds + ']');
                } else {
                    callback("");
                }
            } else {
                jqAlert('Request Fails', "SPDocIds: " + res['Msg'][0]['Message']);
            }
        });
    },
    FailedGetTokenCount: 0,
	RequestDataObj: function () {
		var obj = {
			'MXAPIToken': "",
			'MXAPIURL': "",
			'SPDocIds': "",
            'CurrentParentKey': "",
            'Subsite': "",
		};
		return obj;
	},
	JasonObject: function () {

		var obj = {
			'Token': "",
			'AbEntry': {
				'Scope': {
				},
				'Criteria': {
				}
			}
		};
		return obj;
	},
	GetUserInfoObject: function () {
		var obj = {
			'Token': "",
			"User": {
				"Key": { "UID": 1 }
			}
		};
		return obj;
	},
	GetCultureLocalizationObject: function () {
		var obj = {
			'Token': "",
			'ConfigurationSetting': {
				"Scope": {
					"Fields": {
						"TextValue": 1
					}
				},
				"Criteria": {
					"SearchQuery": {

					}
				}

			}
		};
		return obj;
	},
	SearchQueryObject: function (UID) {
		var andOperation = [];
		var obj = {
			"$AND": []
		}
		andOperation.push({ "Code2": { "$EQ": UID } });
		andOperation.push({ "Code3": { "$EQ": "Preferences" } });
		andOperation.push({ "Code4": { "$EQ": "CulturePreference" } });
		obj["$AND"] = andOperation;

		return obj;
	},
	UpdateJasonObject: function () {

		var obj = {
			'Token': "",
			'AbEntry': {
				'Data': {
				}
			}
		};
		return obj;
	},
	GenerateCriteria: function () {
		var obj = {
			'SearchQuery':
			{}
		}

		return obj;
	},
	GenerateMyAppRequestObj: function () {
		var obj = {
			'MXAPIURL': '',
			'MXAPIToken': '',
			'SPDocIds': ''
		}

		return obj;
	},
	GenerateDownloadMyAppRequestObj: function () {
		var obj = {
			'TypeNumber': '',
			'MXAPIURL': '',
			'MXAPIToken': '',
			'SPDocIds': '',
			'DocumentID': ''
		}

		return obj;
	},
	UDFObject: function () {

	},
	GetAPIToken: function (url, IdentityToken, callback, jsonQueryObj, othercallback) {
		$.ajaxSetup({ cache: false });
		$.ajax({
			async: false,
			type: "POST",
			contentType: "application/json",
			dataType: "json",
			data: JSON.stringify({ 'IdentityToken': IdentityToken }),
			url: url,
			success: function (data) {
				//CloudStorageData.WebApiToken = data.d;
				if (typeof (callback) === 'function') {
					callback(jsonQueryObj, othercallback);
				}
			},
			error: function (xhr, msg) {
				var errorMessage = msg + '\n' + xhr.responseText;
			}
		});
	},
	RequestAPI: function (method, obj, callback) {
	    CloudStorageData.WebApiToken(function (token) {
            if (token != null && token != "") {
                obj.Token = token;
                var url = CloudStorageData.WebAPIURL + "/json/" + method;
                $.ajaxSetup({ cache: false });
                $.ajax({
                    async: false,
                    type: "POST",
                    dataType: "json",
                    data: JSON.stringify(obj),
                    url: url,
                    success: function (data) {
                        if (typeof (callback) === 'function') {
                            callback(data);
                        }
                    },
                    error: function (xhr, msg) {
                        var errorMessage = msg + '\n' + xhr.responseText;
                    }
                });
            }
        })
	},
	GetAndSetLocalization: function (culture) {
		var url = "KendoCulture/Index"
		$.ajaxSetup({ cache: false });
		$.ajax({
			async: false,
			type: "POST",
			dataType: "json",
			data: { 'Culture': culture },
			url: url,
			success: function (data) {
				console.log(data);
			},
			error: function (xhr, msg) {
				var errorMessage = msg + '\n' + xhr.responseText;
			}
		});
	},
	GetMaximizerWebDataApi: function (jsonQueryObj, callback) {
	    CloudStorageData.WebApiToken(function (token) {
            if (MyApp.CurrentParentKey !== '' && token != null && token != "") {
                jsonQueryObj.Token = token;
                var url = CloudStorageData.WebAPIURL + "/json/Update";
                $.ajax({
                    url: url,
                    type: "POST",
                    dataType: "json",
                    async: true,
                    data: JSON.stringify(jsonQueryObj),
                    success: function (data) {
                        if (CloudStorageData.FailedGetTokenCount < 3 && data.Code == -2) {
                            CloudStorageData.GetAPIToken(CloudStorageData.GetTokenURL, CloudStorageData.IdentityToken, CloudStorageData.GetMaximizerWebDataApi, jsonQueryObj, callback);
                            return false;
                        }

                        CloudStorageData.FailedGetTokenCount = 0;

                        callback("data: ", data);
                    },
                    error: function (resultJson) {
                        console.log("Server error");

                    }
                });
            }
        })
    },
    GetConfiguration1: function (JsonObj, callback) {
        CloudStorageData.WebApiToken(function (token) {
            if (token != null && token != "") {
                var url = CloudStorageData.GetControllerUrl(false) + "/GetConfigAddress";
                JsonObj.MXAPIToken = token;
                var jsObj = { MxObj: JSON.stringify(JsonObj) }
                $.ajax({
                    url: url,
                    type: "POST",
                    dataType: "json",
                    async: true,
                    data: jsObj,
                    success: function (Result) {
                        callback(Result);
                    },
                    error: function (resultJson) {
                        console.log("Server error");
                    }
                });
            }
        })
    },
    GetControllerUrl: function(isApi) {
        pathname = location.pathname.replace("//", "/");

        if (pathname[0] == "/")
            pathname = pathname.substring(1);  // remove the first "/"

        var firstFolder = pathname.substring(0, pathname.search("/"));

        if (!isApi) return "/" + firstFolder + "/SharePoint";
        else return "/" + firstFolder;
    },
    GetMyAppData: function (JsonObj, callback) {
        CloudStorageData.WebApiToken(function (token) {
            if (token != null && token != "") {
                var url = CloudStorageData.GetControllerUrl(false) + "/GetSPData";
                JsonObj.MXAPIToken = token;
                var jsObj = { MxObj: JSON.stringify(JsonObj) }
                $.ajax({
                    url: url,
                    type: "POST",
                    dataType: "json",
                    async: true,
                    data: jsObj,
                    success: function (Result) {
                        if (CloudStorageData.FailedGetTokenCount < 3 && Result.Code == -2) {
                            CloudStorageData.FailedGetTokenCount++;
                            CloudStorageData.GetAPIToken(CloudStorageData.GetTokenURL, CloudStorageData.IdentityToken, CloudStorageData.GetMyAppData, JsonObj, callback);
                            return false;
                        }
                        CloudStorageData.FailedGetTokenCount = 0;

                        callback(Result);
                    },
                    error: function (resultJson) {
                        console.log("Server error");
                    }
                });
            }
        })
    },
    //DownLoadFile: function (JsonObj) {
    //    CloudStorageData.WebApiToken(function (token) {
    //        if (token != null && token != "") {
    //            var url = CloudStorageData.GetControllerUrl(false) + "/DownloadFile";
    //            JsonObj.MXAPIToken = token;
    //            var jsObj = { MxObj: JsonObj }
    //            $.ajax({
    //                url: url,
    //                type: "POST",
    //                dataType: "text",
    //                async: true,
    //                data: jsObj,
    //                success: function (Result) {
    //                    document.body.appendChild(Result);
	   //                 document.body.removeChild(form);
    //                },
    //                error: function (resultJson) {
    //                    console.log("Server error");

    //                    document.body.appendChild(Result);
    //                    document.body.removeChild(form);
    //                }
    //            });
    //        }
    //    })
    //}
	DownLoadFile: function (jsonQueryObj) {

        var url = this.GetControllerUrl(false) + "/DownloadFile";
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("action", url);

		// Detect Browser
		// Internet Explorer 6-11
		var isIE = /*@cc_on!@*/false || !!document.documentMode;

		// Edge 20+
		var isEdge = !isIE && !!window.StyleMedia;

		if ((isIE != true) && (isEdge != true)) {
			form.setAttribute("target", '_blank');
		}

		var input = document.createElement('input');
		input.type = 'hidden';
		input.name = "MxObj";
		input.value = jsonQueryObj;
		form.appendChild(input);
		document.body.appendChild(form);

        form.submit();
       
		document.body.removeChild(form);
	}
}
var SearchObject = function () {

	this.SearchOR = function (searchString) {
		var ORObject = {
			"$OR": searchString
		}
		return ORObject;
	};
	this.SearchAnd = function (searchString) {
		var ANDObject = {
			"$AND": searchString
		}

		return ANDObject;
	};
	this.SearchEqual = function (field, value) {
		var fieldValue = "$PARAM";
		var fieldName = field;
		if (value !== '') {
			fieldValue = value;
		}
		var searchObject = {};
		searchObject[fieldName] = { '$EQ': fieldValue };
		return searchObject;
	};
	this.SearchNotEqual = function (field, value) {
		var fieldValue = value;
		var fieldName = field;
		if (typeof (value) === 'undefined') {
			fieldValue = "$PARAM";
		}
		var searchObject = {};
		searchObject[fieldName] = { '$NE': fieldValue };
		return searchObject;
	};
}
