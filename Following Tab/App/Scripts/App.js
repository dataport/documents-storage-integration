﻿/* Common app functionality */

var app = (function () {
    "use strict";

    var app = {};

    // Common initialization function (to be called from each page)
    app.initialize = function () {
        $('#app-grid').append(
            '<div id="notification-message">' +
                '<div class="padding">' +
                    '<div id="notification-message-close"></div>' +
                    '<div id="notification-message-header"></div>' +
                    '<div id="notification-message-body"></div>' +
                '</div>' +
            '</div>');

        // original color
        var bgColor = $('#notification-message').css('background-color');
        $('#notification-message').css('display', 'none');

        var disappear = function () {
            setTimeout(function () {
                $('#notification-message').fadeOut('slow')
                                          .css('background-color', bgColor);
            }, 6000);
        };
        $('#notification-message-close').click(function () {
            $('#notification-message').hide();
        });


        // After initialization, expose a common notification function
        app.showNotification = function (header, text, htmlBool) {
            $('#notification-message-header').text(header);
            $('#notification-message').css('background-color', '#ff9900');
            if (htmlBool == undefined || !htmlBool)
                $('#notification-message-body').text(text);
            else
                $('#notification-message-body').html(text);

            $('#notification-message').slideDown('fast');

        };
        app.showIconNotification = function (header, body) {
            $('#notification-message-header').html('<div id="notification-message-icon"/>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp' + header);
            $('#notification-message-body').html(body);
            $('#notification-message').slideDown('fast');

        };

        // this is only for the message to inform user to press "SAVE TO MAXIMIZER" button 
        app.showFadeOutNotification = function (body) {
            $('#notification-message-header').text("");
            $('#notification-message').css('background-color', '#7070ff');
            $('#notification-message-body').html(body);
            $('#notification-message').slideDown('fast');

            disappear();
        };

    };

    return app;
})();