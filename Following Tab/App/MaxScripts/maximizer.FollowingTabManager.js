﻿// Uses: maximizer.Events.js

var maximizer = (function (maximizer) {
    "use strict";

    maximizer.DEBUG = false;

    var FollowingTabManager = function () {
        this.initialize();
    };

    var p = FollowingTabManager.prototype = new maximizer.EventManager();

    p.moduleEventManager = {};

    p.registerTab = function (tab, tabId) {
        //console.log('register');
        var that = this;
        if (tab) {
            tab.tabId = tabId;
            tab.showIndiCator = this.getShowIndiCator(tabId);
            tab.showPersonalOrMultiABEntryMSG = this.getShowPersonalOrMultiABEntryMSG(tabId);

            for (var i = 0; i < this.events.length; i += 1) {
                var e = this.events[i];
                tab.createEvent(e);

                if (e.properties.direction === 'out') {
                    // subscribe to tab's "out" events
                    tab.addEventHandler(e, function (e) {
                        that.dispatchEventFromTab(e);
                    });
                }
            }
            this.tabs.push(tab);
            this.currentTab = tab;

            var savedEventKeys = maximizer.getObjectKeys(this.savedEvents);
            // Check for parent key change events
            var updateCurrentRecord = true;
            for (var i = 0; i < savedEventKeys.length; i += 1) {
                var eventName = savedEventKeys[i];
                if (eventName == 'parentrecordchange') {
                    // Skip 'parentrecordupdate' combined with this event as it will cause a second refresh
                    updateCurrentRecord = false;
                }
            }
            for (var i = 0; i < savedEventKeys.length; i += 1) {
                var eventName = savedEventKeys[i];
                if (eventName != 'parentrecordupdate' || updateCurrentRecord) {
                    this.callCurrentTab(this.savedEvents[eventName]);
                }
            }

        }
    };

    p.switchTab = function (tab) {
        //        console.log('switching to tab ' + tab);
        this.callCurrentTab("blur");


        if (typeof tab === "string") {
            var t = null;
            for (var i = 0; i < this.tabs.length; i += 1) {
                if (this.tabs[i].tabId === tab) {
                    t = this.tabs[i];
                    break;
                }
            }
            if (t === null) {
                this.currentTab = null;

                //throw 'switchTab: Tab "' + tab + '" not found.';
            }
            tab = t;
        }

        if (tab) {
            this.currentTab = tab;
            this.callCurrentTab("beforefocus"); // this will eventually send the "focus" event to the tab
        }
        else {
            this.currentTab = null;
            if (this.tabs.length == 0) {
                // Event called on module switch before any tab is registered
                var event = { eventName: "beforefocus", direction: 'in' };
                this.savedEvents[event.eventName] = event;
            }
        }
    }

    p.reset = function () {
        this.tabs = [];             // Clean tab collection on init and when switching modules (onunload on a per-tab basis can't do it)
        this.modules = [];
        this.currentTab = null;
        this.savedEvents = {};
        this.indicator = {};
        this.showmsg = {};
    };

    p.dispatchEventToModules = function (event) {
        if (event.properties.direction == 'in') {
            for (var i = 0; i < this.modules.length; i += 1) {
                if (this.modules[i].availableForEvents) {
                    this.modules[i].triggerEvent(event);
                }
            }
        }
    };

    p.dispatchEventToTabs = function (event) {
        if (event.properties.direction == 'in') {
            this.savedEvents[event.eventName] = event;
            for (var i = 0; i < this.tabs.length; i += 1) {
                if (this.currentTab === this.tabs[i]) {
                    if (maximizer.DEBUG) {
                        console.log('fire ' + event.eventName + ' for tab #' + i);
                    }
                    event.properties.immediateEvent = true;
                    this.callCurrentTab(event);
                }
                else {
                    if (maximizer.DEBUG) {
                        console.log('save ' + event.eventName + ' for tab #' + i);
                    }
                    if (this.tabs[i].availableForEvents) {
                        this.tabs[i].saveEvent(event);
                    }
                }
            }
        }
    };

    p.dispatchEventFromTab = function (event, eventData) {
        this.triggerEvent(event, eventData);
    };

    p.callCurrentTab = function (event, eventData) {
        if (this.currentTab && this.currentTab.availableForEvents) {
            this.currentTab.triggerEvent(event, eventData);
        }
    };
    p.getShowIndiCator = function (tabid) {

        return this.indicator[tabid];
    }
    p.getShowPersonalOrMultiABEntryMSG = function (tabid) {
        return this.showmsg[tabid];
    }
    p.initialize = function () {
        var that = this;

        this.reset();  // events for newly registered tabs

        var arrEvents = [
            { eventName: "beforeSplit", direction: 'in' },
            { eventName: "afterSplit", direction: 'in' },
            { eventName: "panelExpand", direction: 'in' },
            { eventName: "panelCollapse", direction: 'in' },
            { eventName: "resize", direction: 'in' },
            { eventName: "beforefocus", direction: 'in' },
            { eventName: "focus", direction: 'in' },
            { eventName: "blur", direction: 'in' },
            { eventName: "moduleSwitch", direction: 'in' },
            { eventName: "tabSwitch", direction: 'in' },
            //{ eventName: "parentClear", direction: 'in' },
            { eventName: "parentAbort", direction: 'in' },
            { eventName: "parentRecordUpdate", direction: 'in' },
            { eventName: "parentRecordChange", direction: 'in' },
            { eventName: "parentSelectionChange", direction: 'in' },
            { eventName: "ClearComplete", direction: 'in' },
        //{ eventName: "getPrevNextStatus", direction: 'in' },
        //    
        //{eventName: "setPrevNextStatus", direction: 'out' },
            {eventName: "updateParentCurrentRecord", direction: 'out' },
           // { eventName: "loadContent", direction: 'out' },
           // { eventName: "updateContent", direction: 'out' },
            { eventName: "addtorecententries", direction: 'out' }
           // { eventName: "addnewrecord", direction: 'out' }
        ];

        var e;

        for (var i = 0; i < arrEvents.length; i++) {
            e = arrEvents[i];
            this.addEventHandler(e, function (e) {
                that.dispatchEventToTabs(e);
            });
        }

        // Tab Modules
        this.moduleEventManager = new maximizer.EventManager();

        var arrModuleEvents = [
            { eventName: "moveUpOneRecord", direction: 'in' },
            { eventName: "moveDownOneRecord", direction: 'in' },
        ];

        for (var i = 0; i < arrModuleEvents.length; i++) {
            e = arrModuleEvents[i];
            this.moduleEventManager.addEventHandler(e, function (e) {
                that.dispatchEventToModules(e);
            });
        }

    };

    p.registerTabModule = function (tabModule) {
        var that = this;
        if (tabModule) {

            for (var i = 0; i < this.moduleEventManager.events.length; i += 1) {
                var e = this.moduleEventManager.events[i];
                tabModule.createEvent(e);
            }
            this.modules.push(tabModule);
        }
    };

    maximizer.FollowingTabManager = FollowingTabManager;

    return maximizer;
} (maximizer || {}));


// FollowingTab

var maximizer = (function (maximizer) {
    "use strict";
    var FollowingTab = function () {
        this.initialize();
        this.initializeTab();
    }

    var p = FollowingTab.prototype = new maximizer.EventManager();
    p.showIndiCator = '';
    p.showPersonalOrMultiABEntryMSG = '';
    p.savedEvents = {};
    p.initializeTab = function () {
        this.savedEvents = {};
        this.active = false;
        this.currentModuleId = '';
        this.parentKey = "";
        this.tabSize = { width: 0, height: 0 };
        this.availableForEvents = true;
        this.showIndiCator = '';
        this.showPersonalOrMultiABEntryMSG = '';
        var that = this;

        this.on("focus", function () {

            that.active = true;
        });

		this.on("beforefocus", function () {
	
            top.$page.removeContentContainerFocus(); // TODO: replace with "this.controllingWindow.trigger("blur")"

            // Whatever happened when the tab was inactive - process these events
            var savedEventKeys = maximizer.getObjectKeys(that.savedEvents);
            // Check for parent key change events
            var updateCurrentRecord = true;
            for (var i = 0; i < savedEventKeys.length; i += 1) {
                var eventName = savedEventKeys[i];
                if (eventName == 'parentrecordchange') {
                    // Skip 'parentrecordupdate' combined with this event as it will cause a second refresh
                    updateCurrentRecord = false;
                }
            }
            for (var i = 0; i < savedEventKeys.length; i += 1) {
                var eventName = savedEventKeys[i];
                if (eventName != 'parentrecordupdate' || updateCurrentRecord) {
                    that.triggerEvent(that.savedEvents[eventName]);
                }
                delete that.savedEvents[eventName];
            }

            that.triggerEvent({ eventName: "focus", data: {} });
        });

        this.on("blur", function () {
            that.active = false;
        });

        this.on("parentRecordChange", function (e) {
            var newKey = e.data.key;
            if (that.parentKey != newKey) {
                if (maximizer.DEBUG) {
                    console.log('Tab\'s manager sets parentkey to ' + newKey);
                }
                that.parentKey = newKey;
            }
            else {
                if (maximizer.DEBUG) {
                    console.log('Tab\'s manager cancels setting parentkey to ' + newKey + '. Already set.');
                }
                top.$page.FollowingWindow.ontabloadend();
                e.cancelPropagation = true;
            }
        });
        this.on("ClearComplete", function (e) {
            that.parentKey = "zzzzzzzzzzzzzz0928388837475"; // fake parentKey
        });
        this.on("resize", function (e) {
            if (e.data.width === that.tabSize.width && e.data.height === that.tabSize.height) {
                // filter out resize events that do not resize anything
                if (maximizer.DEBUG) {
                    console.log('Tab\'s manager cancels resize event. No size change.');
                }
                return false;
            }
            that.tabSize = { width: e.data.width, height: e.data.height };
            that.active = true;
        });
    }

    p.start = function () {
        // find the manager

        if (top.$page && top.$page.registerFollowingTab) {
            top.$page.registerFollowingTab(this, window);

            var that = this;
            if (top.$page.FollowingWindowManager.currentTab != null && that.showIndiCator == 'Yes') {
                top.$page.FollowingWindow.ontabloadbegin();
            }
            else if (top.$page.FollowingWindowManager.currentTab != null && that.showIndiCator == 'No') {
                top.$page.FollowingWindow.ontabloadend();
                top.$page.FollowingWindow.hideLoading();
            }

            if (top.$page.FollowingWindow.currentModule == 'task' && top.$page.FollowingWindowManager.currentTab != null) {
                if (top.$page.FollowingWindow.stash != null && top.$page.FollowingWindow.stash.reason === 'noparent') {
                    that.parentKey = '';
                    that.managePersonalTasks(that.parentKey);
                }
            }
            else if (top.$page.FollowingWindow.currentModule != 'task' && top.$page.FollowingWindowManager.currentTab != null) {
                that.managePersonalTasks(that.parentKey);
            }
            if (top.$page.FollowingWindowManager.currentTab != null && top.$page.FollowingWindowManager.currentTab.parentKey == "" && that.showIndiCator == 'Yes') {
                top.$page.FollowingWindow.ontabloadend();
                top.$page.FollowingWindow.hideLoading();
            }
            if (top.$page.FollowingWindowManager.currentTab != null) {
                if (top.$page.FollowingWindowManager.currentTab.parentKey == '') {
                    if (typeof (top.$page.FollowingWindowManager.currentTab.HideActionPanel) == 'function') {
                        top.$page.FollowingWindowManager.currentTab.HideActionPanel(top.$page.FollowingWindowManager.currentTab.tabId);
                    }
                } else {
                    if (typeof (top.$page.FollowingWindowManager.currentTab.ShowActionPanel) == 'function') {
                        top.$page.FollowingWindowManager.currentTab.ShowActionPanel(top.$page.FollowingWindowManager.currentTab.tabId);
                    }
                }
            }
        }
        else {
            throw "maximizer.FollowingTab unable to initialize: window.top.$page not found.";
        }
    }
    p.saveEvent = function (event) {
        event.properties.immediateEvent = false;
        this.savedEvents[event.eventName] = event;
        //console.log('Event saved (' + event.eventName + ')');
    };
    p.removeEvent = function (event, savedEvents) {
        for (var i in savedEvents) {
            if (i === event.toLowerCase()) {
                delete savedEvents[i];
            }
        }
    }

    // shortcut
    p.updateParentCurrentRecord = function (eventData) {
        this.triggerEvent({ eventName: "updateParentCurrentRecord", data: eventData });
    };

    // getter
    p.getParentKey = function () {
        return this.parentKey;
    };

    p.refresh = function () {
        this.triggerEvent("parentRecordUpdate", { key: this.getParentKey() });
    }
    p.parentReload = function () {
        top.MainGridUpdate.Refresh();
    }

    p.managePersonalTasks = function (parentKey) {
        var msgElem = $('#fw_message');
        var contextMenu = $('.contextMenu');
        var FollowingVisibleContents = $('.FollowingVisibleContents');
        if (msgElem.length == 0) {
            msgElem = $('<div id="fw_message" class="max-fw-message" />'); // fw_message CSS class is defined in ContentPage.css
            $('body').prepend(msgElem);
        }

        if (typeof (top.taskMaxGrid) != 'undefined' && top.taskMaxGrid.GridID == 'tasks' && parentKey == '' && typeof (top.taskMaxGrid.DataManager) != 'undefined' && typeof (top.taskMaxGrid.DataManager.TableData) != 'undefined' && '' != top.taskMaxGrid.DataManager.TableData.CurrentKey && this.showPersonalOrMultiABEntryMSG == "Yes") {

            var m = top.JSS_MsgFollowingWindowHotlistPersonalParent || // Generated by ContentPage.aspx.cs
                'The current task or appointment is either personal or has multiple Address Book entries assigned. ' +
                'This following window only shows entries for tasks or appointments that have a single associated Address Book entry.';

            // Hiding contents except message

            FollowingVisibleContents.hide();
            msgElem.text(m).show();
            contextMenu.css("display", "none");
            top.$page.FollowingWindow.hideLoading();
        }
        else if (typeof (top.taskMaxGrid) != 'undefined' && top.taskMaxGrid.GridID == 'tasks' && parentKey != '' && typeof (top.taskMaxGrid.DataManager) != 'undefined' && typeof (top.taskMaxGrid.DataManager.TableData) != 'undefined' && '' != top.taskMaxGrid.DataManager.TableData.CurrentKey && this.showPersonalOrMultiABEntryMSG == "Yes") {
            msgElem.hide();
            contextMenu.removeAttr("style");
            FollowingVisibleContents.show();
            //top.$page.FollowingWindow.hideLoading();
        } else if (msgElem.length > 0 && this.showPersonalOrMultiABEntryMSG == "No") {
            msgElem.remove();
            contextMenu.removeAttr("style");
            FollowingVisibleContents.show();
            // top.$page.FollowingWindow.hideLoading();
        }
        else if (typeof (top.taskMaxGrid) != 'undefined' && top.taskMaxGrid.GridID != 'tasks' && msgElem.length > 0) {
            msgElem.remove();
            contextMenu.removeAttr("style");
            FollowingVisibleContents.show();
        }
        else {

            msgElem.hide();
            contextMenu.removeAttr("style");
            FollowingVisibleContents.show();
            // top.$page.FollowingWindow.hideLoading();
        }
    };
    p.ExpandCollapseActionPanel = function (isActionPanelCollapsed, fwtabId, tabFrame) {
        var actionsColumn = $('#actions-column');
        var btnAction = $('#btnAction');
        if (fwtabId == 'details' && actionsColumn.length == 0 && btnAction.length == 0) {
            var detailsTab = document.getElementById("detailsFrame").contentWindow.document || document.getElementById("detailsFrame").contentDocument;
            actionsColumn = $('#actions-column', detailsTab);
            btnAction = $('#btnAction', detailsTab);
        }
        if (actionsColumn.length > 0 && btnAction.length > 0) {

            var hasValue = btnAction.data('isOpend');
            if (isActionPanelCollapsed != hasValue) {
                var frame = tabFrame[0].contentWindow || tabFrame[0].contentDocument;
                var ActionPanel = frame.ActionPanel;
                if (fwtabId != 'details' && typeof (ActionPanel) != "undefined" && typeof (ActionPanel) == "object") {
                    if (isActionPanelCollapsed) {
                        ActionPanel.ExpandActionMenu();
                    }
                    else {
                        ActionPanel.CallapseActionMenu();
                    }
                    btnAction.data('isOpend', isActionPanelCollapsed);
                }
                else if (fwtabId == 'details') {
                    var detailsTab = document.getElementById("detailsFrame").contentWindow || document.getElementById("detailsFrame").contentDocument;
                    ActionPanel = detailsTab.ActionPanel;
                    if (typeof (ActionPanel) == "object") {
                        if (isActionPanelCollapsed) {
                            ActionPanel.ExpandActionMenu();
                        }
                        else {
                            ActionPanel.CallapseActionMenu();
                        }
                        btnAction.data('isOpend', isActionPanelCollapsed);
                    }

                }
            }
        }

    };
    p.HideActionPanel = function (fwtabId) {
        var actionsColumn = $('#actions-column');

        if (fwtabId == 'details' && actionsColumn.length == 0) {
            var detailsTab = document.getElementById("detailsFrame").contentWindow.document || document.getElementById("detailsFrame").contentDocument;
            actionsColumn = $('#actions-column', detailsTab);

        }
        if (actionsColumn.length > 0) {
            actionsColumn.addClass('max-hidden');
        }
    }
    p.ShowActionPanel = function (fwtabId) {
        var actionsColumn = $('#actions-column');

        if (fwtabId == 'details' && actionsColumn.length == 0) {
            var detailsTab = document.getElementById("detailsFrame").contentWindow.document || document.getElementById("detailsFrame").contentDocument;
            actionsColumn = $('#actions-column', detailsTab);
        }
        if (actionsColumn.length > 0) {
            actionsColumn.removeClass('max-hidden');
        }
        if (typeof (ActionPanel) == 'object') {
            ActionPanel.ActionResize();
        }
    }
    maximizer.FollowingTab = FollowingTab;

    return maximizer;
} (maximizer || {}));

// usage:
/**
$(function () {
var fwTestTab = new maximizer.FollowingTab();

fwTestTab.on("parentRecordChange", function (e) { console.log('f1:' + e.data.key); });

var fwTestTab2 = new maximizer.FollowingTab();

fwTestTab2.on("parentRecordChange", function (e) { console.log('f2:' + e.data.key); });

});
/**/

// FollowingTabModule

var maximizer = (function (maximizer) {
    "use strict";
    var FollowingTabModule = function () {
        this.initialize();
        this.initializeTabModule();
    };

    var p = FollowingTabModule.prototype = new maximizer.EventManager();
    p.savedKey = "";
    p.initializeTabModule = function () {
        this.savedKey = null;
        this.availableForEvents = true;
        var that = this;
    };

    p.start = function () {
        // hook up events
        top.$page.FollowingWindowManager.registerTabModule(this);
        var hasUpDown = false;
        for (var i = 0, iLen = this.events.length; i < iLen; i++) {
            if (this.events[i].eventName == "moveuponerecord" || this.events[i].eventName == "movedownonerecord") hasUpDown = true;
        }
        if (hasUpDown) {
            top.$page.FollowingWindow.upDownButtonsSetup(null);
        }
    };

    p.changeParent = function (key) {
        if (this.savedKey == key) {
            top.$page.FollowingWindow.repeatLastParentChange('refresh');
        }
        else {
            this.savedKey = key;
            top.$page.FollowingWindow.onparentchange(null, key, "click");
        }
    };

    maximizer.FollowingTabModule = FollowingTabModule;

    return maximizer;
} (maximizer || {}));


// usage
/*

var fwTestModule;

$(function () {
  fwTestModule = new maximizer.FollowingTabModule();
     fwTestModule.on("moveUpOneRecord”, function (e) {
    alert('Move Up One Record'); 
  });
     fwTestModule.on("moveDownOneRecord”, function (e) {
    alert('Move Down One Record'); 
  });
  fwTestModule.start();
});

// Change the parent key - will trigger parentRecordChange (or parentRecordUpdate, if the key is the same as the previous call) 
fwTestModule.changeParent(key);

*/