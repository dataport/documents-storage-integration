var apiUrl1 = "http://localhost/MaximizerWebData";


function getCustomCRUDToken() {

	var returnValue;
	var urlVars = getUrlVars();
	var path = urlVars["GetTokenURL"];
	apiUrl1 = urlVars["WebAPIURL"];
	var identityToken = urlVars["IdentityToken"];

	var httpRequest = new XMLHttpRequest();
	httpRequest.open('POST', path, false);
	httpRequest.onreadystatechange = function () {
		if (httpRequest.readyState == 4 && httpRequest.status == 200) {
			returnValue = JSON.parse(httpRequest.responseText).d;
		}
	}
	httpRequest.setRequestHeader('Content-Type', 'application/json');
	httpRequest.send(JSON.stringify({ 'IdentityToken': identityToken }));
	return returnValue;

}

function callMaximizerApi1(baseUrl, methodName, parameters) {
	var returnValue = '';
	var httpRequest = new XMLHttpRequest();
	httpRequest.open('POST', baseUrl + '/json/' + methodName, false);

	httpRequest.onreadystatechange = function () {
		if (httpRequest.readyState == 4 && httpRequest.status == 200) {
			returnValue = JSON.parse(httpRequest.responseText);
		}
	};

	httpRequest.setRequestHeader('Content-Type', 'plain/text');

	httpRequest.send(JSON.stringify(parameters));

	return returnValue;
}

function abEntryFamilyRecord(key) {
    token = getCustomCRUDToken();
    var request = {
        Token: token,
        "AbEntry": {
            "Scope": {
                "Fields": {
                    "FullName": 1,
                    "Key": 1,
                }
            },
            "Criteria": {
                "SearchQuery": {
                    "Key": { "$EQ": { "Body": key, "OrRelated": true } }
                }
            }
        }
    };

    return callMaximizerApi1(apiUrl1, 'AbEntryRead', request);
}

function getEntryFamily(key) {
    return new Promise(function (resolve, reject) {
        resolve(abEntryFamilyRecord(key));
    });
}

function customChildFamilyRecord(keys) {
    token = getCustomCRUDToken();
    var request = {
        Token: token,
        "CustomChild": {
            "Criteria": {
                "SearchQuery": {
                    "$AND": [
					{
					    "$IN": {
					        "ParentKey": keys
					    }
					},
					{
					    "$EQ": {
					        "Name": currentCloudStorage + "DocLink"
					    }
					}
                    ]
                }
            },
            "Scope": {
                "Fields": {
                    "ApplicationId": 1,
                    "Name": 1,
                    "Description": 1,
                    "Text1": 1,
                    "Text2": 1,
                    "Text3": 1,
                    "Text4": 1,
                    "Key": 1,
                    "Parent": 1,
                    "DateTime1": 1,
                    "DateTime2": 1,
                    "LastModifyDate": 1,
                    "Number1": 1
                }
            }
        }
    };

    return callMaximizerApi1(apiUrl1, 'customChildRead', request);
}

function getCustomChildFamily(keys) {
    return new Promise(function (resolve, reject) {
        resolve(customChildFamilyRecord(keys));
    });
}

function createCustomRecord(folderPath, subsite) {
    token = getCustomCRUDToken();
    var request = {
        Token: token,
        "Custom": {
            "Data": {
                "Key": null,
                "ApplicationId": applicationId,
                "Name": "Setting",
                "Description": "Folder Location in " + currentCloudStorage,
                "Text1": folderPath,
                "Text2": subsite
            }
        }
    };

    return callMaximizerApi1(apiUrl1, 'customCreate', request);
}

function createCustomShowAll(access, showall) {
	token = getCustomCRUDToken();
	var request = {
		Token: token,
		"Custom": {
			"Data": {
				"Key": null,
				"ApplicationId": applicationId,
				"Name": "ShowAll",
				"Description": "Show All entry flag",
				"Text1": showall,
				"SecAccess": {
					"Read": [
						access
					],
					"Write": [
						access
					]
				}
			}
		}
	};

	return callMaximizerApi1(apiUrl1, 'customCreate', request);
}

function readCustomRecord() {
    token = getCustomCRUDToken();
	var request = {
		Token: token,
		"Custom": {
			"Criteria": {
				"SearchQuery": {
				    "$AND": [
					{
					    "$EQ": {
					        "Name": "Setting"
					    }
					},
					{
					    "$EQ": {
					        "ApplicationId": applicationId
					    }
					}
				    ]
				}
			},
			"Scope": {
				"Fields": {
					"ApplicationId": 1,
					"Name": 1,
					"Description": 1,
					"Text1": 1,
					"Text2": 1,
					"Text3": 1,
					"Text4": 1,
					"Key": 1
				}
			}
		}
	};

	return callMaximizerApi1(apiUrl1, 'CustomRead', request);
}

function readCustomShowAll(userKey) {
	token = getCustomCRUDToken();
	var request = {
		Token: token,
		"Custom": {
			"Criteria": {
				"SearchQuery": {
					"$AND": [
						{
							"$EQ": {
								"Name": "ShowAll"
							}
						},
						{
							"$EQ": {
								"ApplicationId": applicationId
							}
						},
                        {
							"$EQ": {
								"Creator": userKey
							}
						}
					]
				}
			},
			"Scope": {
				"Fields": {
					"ApplicationId": 1,
					"Name": 1,
					"Description": 1,
					"Text1": 1,
					"Text2": 1,
					"Text3": 1,
					"Text4": 1,
					"Key": 1
				}
			}
		}
	};

	return callMaximizerApi1(apiUrl1, 'CustomRead', request);
}

function updateCustomRecord(id, newPath, subsite) {
    token = getCustomCRUDToken();

    if (id != null || id != undefined) {
        var request = {
            Token: token,
            "Custom": {
                "Data": {
                    "Key": id,
                    "Text1": newPath,
                    "Text2": subsite
                }
            }
        };
    } else {
        // update failed!
    }

    return callMaximizerApi1(apiUrl1, 'customUpdate', request);
}

function updateCustomShowAll(id, showall) {
	token = getCustomCRUDToken();

	if (id != null || id != undefined) {
		var request = {
			Token: token,
			"Custom": {
				"Data": {
					"Key": id,
					"Text1": showall,
				}
			}
		};
	} else {
		// update failed!
	}

	return callMaximizerApi1(apiUrl1, 'customUpdate', request);
}

function updateShowAll(userKey, showall) {
	return new Promise(function (resolve, reject) {
		var customRecord = readCustomShowAll(userKey);
		if (Object.keys(customRecord['Custom']['Data']).length <= 0) {  // record just created
			resolve(createCustomShowAll(userKey, showall));
		} else {
			resolve(updateCustomShowAll(customRecord['Custom']['Data'][0]['Key'], showall));
		}	
	});
}

function updateDefaultFolderPath(newPath, subsite) {
    return new Promise(function (resolve, reject) {
        var customRecord = readCustomRecord();
        if (Object.keys(customRecord['Custom']['Data']).length <= 0) {  // record just created
            resolve(createCustomRecord(newPath, subsite));
        } else {
            resolve(updateCustomRecord(customRecord['Custom']['Data'][0]['Key'], newPath, subsite));
        }
    });
}

function getDefaultFolderPathAndSubsite() {
    return new Promise(function (resolve, reject) {
        resolve(readCustomRecord());
    });
}

function getShowAll(userKey) {
	return new Promise(function (resolve, reject) {
		resolve(readCustomShowAll(userKey));
	});
}

function getDocLinks(abEntryKey) {
    token = getCustomCRUDToken();
	var request = {
		Token: token,
		"CustomChild": {
			"Criteria": {
				"SearchQuery": {
					"$AND": [
						{
							"Name": {
							    "$EQ": currentCloudStorage + "DocLink"
							}
						},
						{
							"ParentKey": {
								"$EQ": abEntryKey
							}
						}
					]
				}
			},
			"Scope": {
				"Fields": {
					"ApplicationId": 1,
					"Name": 1,
					"Description": 1,
					"Text1": 1,
					"Text2": 1,
					"Text3": 1,
                    "Text4": 1,
                    "DateTime1": 1,
                    "DateTime2": 1,
                    "Key": 1,
                    "Number1": 1
				}
            }
        },
        "Globalization": {
            "Culture": "en-US",
            "TimeZone": "UTC",
            "TimeZoneDesignator": true
        }
	};

	return callMaximizerApi1(apiUrl1, 'customChildRead', request);
}

function getLinkingRecords(refId) {
    token = getCustomCRUDToken();
    var request = {
        Token: token,
        "Configuration": {
            "Drivers": {
                "ICustomChildSearcher": "Maximizer.Model.Access.Sql.CustomChildSearcher"
            }
        },
        "CustomChild": {
            "Criteria": {
                "SearchQuery": {
                    "$AND": [
                        {
                            "$EQ": {
                                "Name": currentCloudStorage + "DocLink"
                            }
                        },
                        {
                            "$EQ": {
                                "Text1": refId
                            }
                        }
                    ]
                }
            },
            "Scope": {
                "Fields": {
                    "ApplicationId": 1,
                    "Name": 1,
                    "Description": 1,
                    "Text1": 1,
                    "Text2": 1,
                    "Text3": 1,
                    "Text4": 1,
                    "DateTime1": 1,
                    "DateTime2": 1,
                    "Key": 1,
                    "Number1": 1
                }
            }
        },
        "Globalization": {
            "Culture": "en-US",
            "TimeZone": "UTC",
            "TimeZoneDesignator": true
        }
    };

    return callMaximizerApi1(apiUrl1, 'customChildRead', request);
}

function checkLinks(refId) {
    return new Promise(function (resolve, reject) { 
        resolve(getLinkingRecords(refId));
    })
}

function getSPDocByRefId(abEntryKey, refId) {
    token = getCustomCRUDToken();
    var request = {
        Token: token,
        "Configuration": {
            "Drivers": {
                "ICustomChildSearcher": "Maximizer.Model.Access.Sql.CustomChildSearcher"
            }
        },
        "CustomChild": {
            "Criteria": {
                "SearchQuery": {
                    "$AND": [
                        {
                            "$EQ": {
                                "Name": currentCloudStorage + "DocLink"
                            }
                        },
                        {
                            "$EQ": {
                                "ParentKey": abEntryKey
                            }
                        },
                        {
                            "$EQ": {
                                "Text1": refId
                            }
                        }
                    ]
                }
            },
            "Scope": {
                "Fields": {
                    "ApplicationId": 1,
                    "Name": 1,
                    "Description": 1,
                    "Text1": 1,
                    "Text2": 1,
                    "Text3": 1,
                    "Text4": 1,
                    "DateTime1": 1,
                    "DateTime2": 1,
                    "Key": 1,
                    "Number1": 1
                }
            }
        },
        "Globalization": {
            "Culture": "en-US",
            "TimeZone": "UTC",
            "TimeZoneDesignator": true
        }
    };

    return callMaximizerApi1(apiUrl1, 'customChildRead', request);
}

function checkExistenceOfRecord(abEntry, refId) {
    return new Promise(function (resolve, reject) {
        resolve(getSPDocByRefId(abEntry, refId));
    });
}

function insertDocLink(abEntryKey, spDocId, filename, folderPath, date, modifiedDate, brokenFlag) {
    if (currentCloudStorage == CloudStorageName.OneDrive || currentCloudStorage == CloudStorageName.OneDriveForBusiness) {
        if (typeof (date) == "number" || typeof (date) == "string") date = new Date(parseInt(date)).toISOString();
        if (typeof (modifiedDate) == "number" || typeof (date) == "string") modifiedDate = new Date(parseInt(modifiedDate)).toISOString();
	}
	var description = "Document ID of abEntry";
	var folderPath2 = "";
	if (spDocId.length > 253) {
		description = spDocId.substring(253, spDocId.length);
		spDocId = spDocId.substr(0, 253);
	}
	if (folderPath.length > 253) {
		folderPath2 = folderPath.substring(253, folderPath.length);
		folderPath = folderPath.substr(0, 253);
	}
    token = getCustomCRUDToken();
    var request = {
        Token: token,
        "CustomChild": {
            "Data": {
                "Key": null,
                "ParentKey": abEntryKey,
                "ApplicationId": currentCloudStorage + "Integration",
                "Name": currentCloudStorage + "DocLink",
				"Description": description,
                "Text1": spDocId,
                "Text2": filename,
				"Text3": folderPath,
				"Text4": folderPath2,
                "DateTime1": date,
                "DateTime2": modifiedDate,
                "Number1": brokenFlag ? 1 : 0
            }
        },
        "Globalization": {
            "Culture": "en-US",
            "TimeZone": "UTC",
            "TimeZoneDesignator": true
        }
    };

    return callMaximizerApi1(apiUrl1, 'customChildCreate', request);
}


function updateDocLink(key,  spDocId, filename, folderPath, date, modifiedDate, brokenFlag) {
    if (currentCloudStorage == CloudStorageName.OneDrive || currentCloudStorage == CloudStorageName.OneDriveForBusiness) {
        if (typeof (date) == "number" || typeof (date) == "string") date = new Date(parseInt(date)).toISOString();
        if (typeof (modifiedDate) == "number" || typeof (date) == "string") modifiedDate = new Date(parseInt(modifiedDate)).toISOString();
	}
	var folderPath2 = "";
	var description = "Document ID of abEntry";
	if (spDocId.length > 253) {
		description = spDocId.substring(253, spDocId.length);
		spDocId = spDocId.substr(0, 253);
	}
	if (folderPath.length > 253) {
		folderPath2 = folderPath.substring(253, folderPath.length);
		folderPath = folderPath.substr(0, 253);
	}
    token = getCustomCRUDToken();
    var request = {
        Token: token,
        "CustomChild": {
            "Data": {
                "Key": key,
                "ApplicationId": currentCloudStorage + "Integration",
                "Name": currentCloudStorage + "DocLink",
				"Description": description,
                "Text1": spDocId,
                "Text2": filename,
				"Text3": folderPath,
				"Text4": folderPath2,
                "DateTime1": date,
                "DateTime2": modifiedDate,
                "Number1": brokenFlag ? 1 : 0
            }
        },
        "Globalization": {
            "Culture": "en-US",
            "TimeZone": "UTC",
            "TimeZoneDesignator": true
        }
    };

    return callMaximizerApi1(apiUrl1, 'customChildUpdate', request);
}

function updateSpDocument(key, spDocId, filename, folderPath, date, modifiedDate, brokenFlag) {
    return new Promise(function (resolve, reject) {
        resolve(updateDocLink(key, spDocId, filename, folderPath, date, modifiedDate, brokenFlag));
    });
}

function deleteDocLink(key) {
    token = getCustomCRUDToken();
    var request = {
        Token: token,
        "CustomChild": {
            "Data": {
                "Key": key
            }
        }
    };	
    return callMaximizerApi1(apiUrl1, 'customChildDelete', request);
}

function callSpDocument(abEntryKey) {
	return new Promise(function (resolve, reject) {
        resolve(getDocLinks(abEntryKey));
	});
}
function createSpDocument(abEntryKey, spDocId, filename, folderPath, serializedDate, serializedModifiedDate, brokenFlag) {
    return new Promise(function (resolve, reject) {
        resolve(insertDocLink(abEntryKey, spDocId, filename, folderPath, serializedDate, serializedModifiedDate, brokenFlag));
    });
}

function delSpDocument(key) {
    return new Promise(function (resolve, reject) {
        resolve(deleteDocLink(key));
    });
}

function createNote(parent, text, filenames) {
	// create not function
	var dateTime = new Date().toISOString();
	token = getCustomCRUDToken();
	var request = {
		Token: token,
		"Note": {
			"Data": {
				"Parent": parent,
				"Category": currentCloudStorage + " Document",
				"Text": text,
				"RichText": text,
				"DateTime": dateTime
			}
		}
	};

	return callMaximizerApi1(apiUrl1, 'noteCreate', request);
}


/*
 * userRead for current User
 */
function GetCurrentUserInfo() {
	var userReadRequest = {
		Token: getCustomCRUDToken(),
		"User": {
			"Scope": {
				"Fields": {
					"Key": {
						"Value": 1,
						"UID": 1
					},
					"DisplayName": 1,
					"Role": {
						"Administrator": 1,
						"SalesManager": 1,
						"SalesRepresentative": 1,
						"CustomerServiceManager": 1,
						"CustomerServiceRepresentative": 1,
						"KnowledgeBaseApprover": 1
					}
				}
			},
			"Criteria": {
				"SearchQuery": {
					"$EQ": {
						"Key": {
							"UID": "$CURRENTUSER()"
						}
					}
				}
			}
		}
	};

	var response = callMaximizerApi1(apiUrl1, 'UserRead', userReadRequest);

	return response;
}

function getAbEntry() {
    return new Promise(function (resolve, reject) {
        resolve(abEntryRead());
    });
}

function abEntryRead() {
    var request = {
        Token: getCustomCRUDToken(),
        "AbEntry": {
            "Scope": {
                "Fields": {
                    "FullName": 1
                }
            },
            "Criteria": {
                "SearchQuery": {
                    "Key": {}
                }
            }
        }
    }
    var response = callMaximizerApi1(apiUrl1, 'AbEntryRead', request);

    return response;
}

function instanceIdRead() {
    token = getCustomCRUDToken();
    var request = {
        Token: token,
        "ConfigurationSetting": {
            "Criteria": {
                "SearchQuery": {
                    "Code4": 
					{
					    "$EQ": "MX_AMAZONS3_ID"
					}
                }
            },
            "Scope": {
                "Fields": {
                    "Key": 1,
                    "Code1": 1,
                    "Code2": 1,
                    "Code3": 1,
                    "Code4": 1,
                    "TextValue": 1,
                }
            }
        }
    };

    return callMaximizerApi1(apiUrl1, 'Read', request);
}
function getInstanceID(key) {
    return new Promise(function (resolve, reject) {
        resolve(instanceIdRead(key));
    });
}
function instanceIdRead() {
    token = getCustomCRUDToken();
    var request = {
        Token: token,
        "ConfigurationSetting": {
            "Criteria": {
                "SearchQuery": {
                    "Code4":
					{
					    "$EQ": "MX_AMAZONS3_ID"
					}
                }
            },
            "Scope": {
                "Fields": {
                    "Key": 1,
                    "Code1": 1,
                    "Code2": 1,
                    "Code3": 1,
                    "Code4": 1,
                    "TextValue": 1,
                }
            }
        }
    };

    return callMaximizerApi1(apiUrl1, 'Read', request);
}
function getEm(key) {
    return new Promise(function (resolve, reject) {
        resolve(instanceIdRead(key));
    });
}