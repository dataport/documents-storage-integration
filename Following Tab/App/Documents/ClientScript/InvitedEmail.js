this._emailuname;
function iec_create(tablename, inputFrom, inputSubject, inputTemplate, doneCallBack, failCallBack, alwaysCallBack) {
    if (document.getElementById("iec_name").value == "") {
        document.getElementById("iec_result").style.color = "white";
        document.getElementById("iec_result").style.backgroundColor = "red";
        document.getElementById("iec_result").innerHTML = "Name cannnot be empty";
        return;
    }
    var iec = {};
    iec.Key = document.getElementById("iec_key").value;
    if (tablename != undefined) {
        iec.Name = this._emailuname = tablename;
    } else {
        iec.Name = this._emailuname;
    }
    iec.ApplicationId = "MaxExternalPortals";
    iec.Description = document.getElementById("iec_description").value;
    iec.User = _user;

    if (inputFrom) {
        iec.Info = { From: inputFrom, Subject: inputSubject, Content: inputTemplate };
    } else {
        try {
            var info = {};
            info.From = document.getElementById("iec_from").value;
            info.Subject = document.getElementById("iec_subject").value;
            info.Content = document.getElementById("iec_content").value;
            iec.Info = info;
        } catch (Exception) {
            iec.Info = { From: "", Subject: "", Content: "" };
        }
    }

    var doneCallBack = function (response) {
        document.getElementById("iec_result").style.color = "white";
        document.getElementById("iec_result").style.backgroundColor = "green";
        document.getElementById("iec_result").innerHTML = "record created";
        iec_read(tablename);
    }
    var failCallBack = function (error) {
        document.getElementById("iec_result").style.color = "white";
        document.getElementById("iec_result").style.backgroundColor = "red";
        try {
            document.getElementById("iec_result").innerHTML = "error: " + error.responseJSON.ExceptionMessage;
            jqAlert("Email Create Error", "Error Creating Email Template record: " + error.responseJSON.ExceptionMessage);

        } catch (Exception) {
            document.getElementById("iec_result").innerHTML = "error: " + error.toString();
            jqAlert("Email Create Error", "Error Creating Email Template record: " + error.toString());
        }
    }

    getMaxToken(function (token) {
        meta.Token = token;
		meta.AddressBook = database;
		//Encode the invitation email template
		iec.Info.Content = encodeURIComponent(iec.Info.Content);
        postInvitationEmailApi(meta, iec, doneCallBack, failCallBack, null);
    });
}

function iec_read(tablename, onSuccess, onFailure) {
    if (tablename == "" || tablename == undefined) {
        document.getElementById("iec_result").style.color = "white";
        document.getElementById("iec_result").style.backgroundColor = "red";
        document.getElementById("iec_result").innerHTML = "Name cannnot be empty";
        return;
    }
    this._emailuname = tablename;
    var doneCallback = function (response) {

        if (response.Key == '' || response.Key == null) {
            if (onFailure)
                return onFailure();
            else
                return jqAlert("IEC Read Error", "Response.key== '' or null");
        }

        document.getElementById("iec_result").innerHTML = "record loaded";
        document.getElementById("iec_key").value = response.Key;
        document.getElementById("iec_ID").value = response.ApplicationId;
        document.getElementById("iec_description").value = response.Description;
		var info = response.Info;
		// Decode the encoded invitation email template
		try { info.Content = decodeURIComponent(info.Content);}
		catch (err) { info.Content = (info.Content); }

        document.getElementById("iec_from").value = info.From;
		document.getElementById("iec_subject").value = info.Subject;
        document.getElementById("iec_content").value = info.Content;

        if (onSuccess)
            return onSuccess();
    };

    var failCallBack = function (error) {
        if (onFailure)
            return onFailure();

        document.getElementById("iec_result").style.color = "white";
        document.getElementById("iec_result").style.backgroundColor = "red";
        try {
            document.getElementById("iec_result").innerHTML = "error: " + error.responseJSON.ExceptionMessage;
            jqAlert("Email Read Error", "Error Reading Email Template record: " + error.responseJSON.ExceptionMessage);
        } catch (Exception) {
            document.getElementById("iec_result").innerHTML = "error: " + error.toString();
            jqAlert("Email Read Error", "Error Reading Email Template record: " + error.toString());
        }
    };

    var alwaysCallBack = function () { };

    getMaxToken(function (token) {
        meta.Token = token;
        meta.AddressBook = database;
        getInvitationEmailApi(JSON.stringify(meta), tablename, this._user, doneCallback, failCallBack, alwaysCallBack);
    });
}

function iec_update(tablename) {
    if (document.getElementById("iec_key").value == "") {
        document.getElementById("iec_result").style.color = "white";
        document.getElementById("iec_result").style.backgroundColor = "red";
        document.getElementById("iec_result").innerHTML = "Key cannnot be empty";
        return;
    }
    var iec = {};
    iec.Key = document.getElementById("iec_key").value;
    if (tablename != undefined) {
        iec.Name = this._emailuname = tableName;
    } else {
        iec.Name = this._emailuname;
    }
    iec.ApplicationId = document.getElementById("iec_ID").value;
    iec.Description = document.getElementById("iec_description").value;
    iec.User = this._user;
    var temp;
    try {
        var info = {};
        info.From = document.getElementById("iec_from").value;
        info.Subject = document.getElementById("iec_subject").value;
		info.Content = document.getElementById("iec_content").value;
        iec.Info = info;
    } catch (Exception) {
        iec.Info = "";
    }

    var doneCallBack = function (response) {
        document.getElementById("iec_result").style.color = "white";
        document.getElementById("iec_result").style.backgroundColor = "green";
        if (response == true) {
            document.getElementById("iec_result").innerHTML = "result updated";
            jqAlert("Update Successful", "Invitation Email Template has been saved successfully!");
        }
        else {
            document.getElementById("iec_result").innerHTML = "result is not updated";
            jqAlert("Email Update Error", "Error updating Email Template:" + JSON.stringify(response));
        }
    }
    var failCallBack = function (error) {
        document.getElementById("iec_result").style.color = "white";
        document.getElementById("iec_result").style.backgroundColor = "red";

        try {
            document.getElementById("iec_result").innerHTML = "error: " + error.responseJSON.ExceptionMessage;
            jqAlert("Email Update Error", "Error updating Email Template: " + error.responseJSON.ExceptionMessage);
        } catch (Exception) {
            document.getElementById("iec_result").innerHTML = "error: " + error.toString();
            jqAlert("Email Update Error", "Error updating Email Template: " + error.toString());
        }
    }
    var alwaysCallBack = function () { };

    getMaxToken(function (token) {
        meta.Token = token;
		meta.AddressBook = database;
		//Encode the invitation email template
		iec.Info.Content = encodeURIComponent(iec.Info.Content);
        putInvitationEmailApi(meta, iec, doneCallBack, failCallBack, alwaysCallBack);
    });
}


function emailTemplate_read(tablename, callback) {
	if (tablename == "" || tablename == undefined) {
		alert("emailTemplate_read() : Name cannnot be empty");
		return;
	}
	this._emailuname = tablename;
	var doneCallback = function (response) {
		if (response.Key == '' || response.Key == null) {
			alert("Email Template is not configured.");
			return;
		}
		var info = response.Info;

		// Decode the encoded invitation email template
		try {
			response.Info.Content = decodeURIComponent(response.Info.Content);
			callback(response.Info);
		} catch(err){
			response.Info.Content = (response.Info.Content);
			callback(response.Info);
		}

	}
	var failCallBack = function (error) {
		alert("error: " + error.responseJSON.ExceptionMessage);
	}
	var alwaysCallBack = function () { };
	getInvitationEmailApi(JSON.stringify(meta), tablename, this._user, doneCallback, failCallBack, alwaysCallBack);
}