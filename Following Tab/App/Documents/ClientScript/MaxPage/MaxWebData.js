/************************************************************************************************
	
	Copyright © 1988 - 2015 Maximizer Software Inc.  

	This source code is provided as an example on how to use Maximizer's APIs.
	Anyone is free to copy, modify, publish, use, compile, sell, or distribute
	this source code, either in source code form or as a compiled binary, for
	any purpose, commercial or non-commercial, and by any means.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.

************************************************************************************************/


function getUrlParameter(sParam)
{
	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++) 
	{
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam) 
		{
			return sParameterName[1];
		}
	}
}

var MaxWebData = function() {
	var identityToken = '';
	var getTokenUrl = decodeURIComponent(getUrlParameter('GetTokenURL'));
	var maxWebDataUrl = decodeURIComponent(getUrlParameter('WebAPIURL')) + "/json";
	var maxWebDataToken = '';

	var that = this;

	this.getMaxWebDataToken = function() {
		var deferred = $.Deferred();

		that.checkMaxWebDataToken(maxWebDataToken).then(
			function(code) {
				if(code == 0) {
					return deferred.resolve(maxWebDataToken);
				} else {
					$.ajaxSetup({
						cache: false
					});

					$.ajax({
						async: true,
						cache: false,
						type: "POST",
						contentType: "application/json; charset=utf-8",
						dataType: "json",
						data: JSON.stringify({ 'IdentityToken': identityToken }),
						url: getTokenUrl,
						success: function (data) {
							maxWebDataToken = data.d;
							return deferred.resolve(maxWebDataToken);
						},
							error: function (xhr, msg) {
							return deferred.reject(xhr.responseText);
						}
					});

				}
			},
			function(msg) {
				return deferred.reject(msg);
			}
		);
		return deferred;
	}


	this.checkMaxWebDataToken = function(token) {
		var deferred = $.Deferred();

		//If no token is passed, we won't bother checking it
		//and will just return -1 to indicate it is not valid
		if(!token) {
			return deferred.resolve(-1);
		}

		var parameters = { "Token" : token };
		$.ajax({
			async: true,
			cache: false,
			type: "POST",
			contentType: "text/plain",
			dataType: "json",
			url: maxWebDataUrl + "/TokenValid",
			data: JSON.stringify(parameters),
			success: function (data) {
				return deferred.resolve(data["Code"]);
			},
			error: function (xhr, msg) {
				return deferred.reject(xhr.responseText);
			}
		});		
		
		return deferred;
	}

	this.AbEntryRead = function(search, fields) {
		var deferred = $.Deferred();

		that.getMaxWebDataToken().then(
			function(token) {

				var parameters = { 
					"Token" : token,
					"AbEntry" : {
						"Scope" : {
							"Fields" : fields
						},
						"Criteria" : {
							"SearchString" : search
						}
					}
				};

				$.ajaxSetup({
					cache: false
				});

				$.ajax({
					async: true,
					type: "POST",
					contentType: "text/plain",
					dataType: "json",
					url: maxWebDataUrl + "/AbEntryRead",
					data: JSON.stringify(parameters),
					success: function (data) {
						if(data["Code"] == 0) {
							return deferred.resolve(data["AbEntry"]["Data"]);
						} else {
							return deferred.reject(data["Msg"]);
						}
					},
					error: function (xhr, msg) {
						return deferred.reject(xhr.responseText);
					}
				});

			},
			function(msg) {
				return deferred.reject(msg);
			}
		);		
		
		return deferred;		

	}

}