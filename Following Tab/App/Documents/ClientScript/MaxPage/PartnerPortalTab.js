


var apiUrl = "You must set the apiUrl variable";


function jsonValidate(jsonString) {
    try {
        var a = JSON.parse(jsonString);
		return null;
    } catch(e) {
		var positionStrIndexOf = e.message.indexOf("position");
		var positionStr= (positionStrIndexOf!=-1 ? e.message.substr(positionStrIndexOf) : "");
		
        if (positionStr!="") {
			var position = parseInt(positionStr.replace("position", ""));
			if (position > 6) 	position -= 5;
			return ( "Invalid JSON syntax error at near:<br/><br/><pre>..." + jsonString.substr(position, 100)  + "...</pre>");
		}
		else
			return e.message;
    }
}

function jqAlert(titleMsg, outputMsg, onCloseCallback) {
    if (!titleMsg)
        titleMsg = 'Alert';

    if (!outputMsg)
        outputMsg = 'No Message to Display.';

    $("<div></div>").html(outputMsg).dialog({
        title: titleMsg,
        resizable: false,
        modal: true,
        open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },
        buttons: {
            "OK": function () {
                $(this).dialog("close");
            }
        },
        close: function() { if (onCloseCallback) onCloseCallback(); }
    });
}

function jqConfirm(titleMsg, outputMsg, onOKCallback,OnCancelCallback,onCloseCallback) {
    if (!titleMsg)
        titleMsg = 'Alert';

    if (!outputMsg)
        outputMsg = 'No Message to Display.';

    $("<div></div>").html(outputMsg).dialog({
        title: titleMsg,
        resizable: false,
        modal: true,
        open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },
        buttons: [
        {
            text: 'OK',
            click: function () {
                if (onOKCallback) onOKCallback();
                $(this).dialog("close");
            }
        },
        {
            text: 'Cancel',
            click: function () {
                if (OnCancelCallback) OnCancelCallback();
                $(this).dialog("close");
            }
        }
        ],
        close: function () { if (onCloseCallback) onCloseCallback(); }
    });
}
function callMaximizerApi(baseUrl, methodName, parameters) {
    var returnValue = '';
    var httpRequest = new XMLHttpRequest();
    httpRequest.open('POST', baseUrl + '/Data.svc/json/' + methodName, false);

    httpRequest.onreadystatechange = function () {
        if (httpRequest.readyState == 4 && httpRequest.status == 200) {
            returnValue = JSON.parse(httpRequest.responseText);
        }
    };

    httpRequest.setRequestHeader('Content-Type', 'plain/text');

    httpRequest.send(JSON.stringify(parameters));

    return returnValue;
}


function callMaximizerApiAsync(baseUrl, methodName, parameters, callback) {
    var returnValue = '';
    var httpRequest = new XMLHttpRequest();
    httpRequest.open('POST', baseUrl + '/Data.svc/json/' + methodName, true);

    httpRequest.onreadystatechange = function () {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                callback(JSON.parse(httpRequest.responseText));
            }
            else {
            }
        }
    };

    httpRequest.setRequestHeader('Content-Type', 'plain/text');

    httpRequest.send(JSON.stringify(parameters));
}

function SchemaRead(token) {

    var gfiRequest = {
        Token: token,
        "Schema": {
            "Scope": {
                "Fields": {
                    "Key": 1,
                    "Alias": 1,
                    "AppliesTo": 1,
                    "Type": 1,
                    "Name": 1,
                    "Folder": {
                        "Key": {
                            "ID": 1,
                            "Value": 1
                        },
                        "Path": 1
                    },
                    "SecAccess": {
                        "Write": 0,
                        "Read": 0
                    },
                    "Creator": {
                        "Key": {
                            "Value": 0,
                            "UID": 0
                        },
                        "DisplayName": 1
                    },
                    "CreationDate": 1,
                    "RequestedBy": 1,
                    "Items": [
                      {
                          "Key": 1,
                          "Value": 1
                      }
                    ],
                    "Inactive": 1,
                    "Mandatory": 1,
                    "Formula": {
                        "Rule": 0
                    },
                    "Attributes": 1
                }
            },
            "Criteria": {
                "SearchQuery": {
                    "$EQ": {
                        "AppliesTo": "AbEntry"
                    }
                }
            }
        },

        "Globalization": {
            "Culture": "en-US",
            "TimeZone": "UTC",
            "TimeZoneDesignator": true
        }
    };

    return callMaximizerApi(apiUrl, 'schemaRead', gfiRequest);
}

function schemaReadForNameField(token) {

    var gfiRequest = {
        Token: token,
        "Schema": {
            "Scope": {
                "Fields": {
                    "Key": 1,
                    "Alias": 1,
                    "AppliesTo": 1,
                    "Type": 1,
                    "Name": 1,
                    "Folder": {
                        "Key": {
                            "ID": 1,
                            "Value": 1
                        },
                        "Path": 1
                    },
                    "SecAccess": {
                        "Write": 0,
                        "Read": 0
                    },
                    "Creator": {
                        "Key": {
                            "Value": 0,
                            "UID": 0
                        },
                        "DisplayName": 1
                    },
                    "CreationDate": 1,
                    "RequestedBy": 1,
                    "Items": [
                      {
                          "Key": 1,
                          "Value": 1
                      }
                    ],
                    "Inactive": 1,
                    "Mandatory": 1,
                    "Formula": {
                        "Rule": 0
                    },
                    "Attributes": 1
                }
            },
            "Criteria": {
                "SearchQuery": {
                    "$AND": [
                        { "$EQ": { "Name": "Partner Password" } },
                        { "$EQ": { "AppliesTo": "AbEntry" } }
                    ]
                }
            }
        },

        "Globalization": {
            "Culture": "en-US",
            "TimeZone": "UTC",
            "TimeZoneDesignator": true
        }
    };

    return callMaximizerApi(apiUrl, 'schemaRead', gfiRequest);
}

function schemaFolderRead(token, folder_name) {

    var request = {
        "Token": token,
        "SchemaFolder": {
            "Scope": {
                "Fields": {
                    "Key": {
                        "Value": 1,
                        "ID": 1
                    },
                    "Name": 1,
                    "AppliesTo": 1,
                    "Path": 1,
                    "ParentFolderKey": 1,
                    "Creator": 1,
                    "CreationDate": 1,
                    "Inactive": 1,
                    "ReadOnly": 1
                }
            },
            "Criteria": {
                "SearchQuery": {
                    "Name": { "$EQ": folder_name }
                }
            }
        },
        "Globalization": {
            "Culture": "en-US",
            "TimeZone": "UTC",
            "TimeZoneDesignator": true
        }
    };
    return callMaximizerApi(apiUrl, 'schemaFolderRead', request);
}

function schemaFolderCreate(token, name, parentFolderKey){
    var request = {};
    if(parentFolderKey){
         request = {
            Token: token,
            SchemaFolder: {
                "Data":{
                    "Key":{"Value":null, "ID":null},
                    "ParentFolderKey":parentFolderKey,
                    "AppliesTo":["AbEntry"],
                    "Name":name
                }
            },
            "Globalization": {
                "Culture": "en-US",
                "TimeZone": "UTC",
                "TimeZoneDesignator": true
            }
        };
    }
    else{
         request= {
            Token: token,
            SchemaFolder: {
                "Data":{
                    "Key":{"Value":null, "ID":null},
                    "AppliesTo":["AbEntry"],
                    "Name":name
                }
            },
            "Globalization": {
                "Culture": "en-US",
                "TimeZone": "UTC",
                "TimeZoneDesignator": true
            }
        };   
    }
    
    return callMaximizerApi(apiUrl, 'schemaFolderCreate', request);
}

function getFolder_Portals(token) {
    var response = schemaReadForNameField(token);

    if (response.Code == 0) {
        var folderKey = findFolderID.Schema.Data[0].Folder.Key.ID;
        return folderID;
    }
    return null;
}
function getFolderID(token) {
    var findFolderID = schemaReadForNameField(token);

    if (findFolderID.Code == 0) {
        var folderID = findFolderID.Schema.Data[0].Folder.Key.ID;
        return folderID;
    }
    return null;
}

function findUdfKey(inputUDFKey, schemas) {
    for (var i = 0; i < schemas.length; i++) {

        if (schemas[i].Key === inputUDFKey) {

            return true;
        }
    }

    return false;
}

function udfDefinitionsCheck(token) {
    var passwordFound = false;
    var regTokenFound = false;
    var regStatusFound = false;

    var schemas = SchemaRead(token).Schema.Data;

    for (var i = 0; i < schemas.length; i++) {
        if (schemas[i].Alias != null) {
            for (var j = 0; j < schemas[i].Alias.length; j++) {
                if (schemas[i].Alias[j] === "Udf/$TAG(PORTALS_PARTNER_PASSWORD)")
                    passwordFound = true;
                else if (schemas[i].Alias[j] === "Udf/$TAG(PORTALS_PARTNER_REGTOKEN)")
                    regTokenFound = true;
                else if (schemas[i].Alias[j] === "Udf/$TAG(PORTALS_PARTNER_REGSTATUS)")
                    regStatusFound = true;
            }
        }
    }
    
    return passwordFound && regTokenFound && regStatusFound;
}

function udfPasswordExists(token) {
    var passwordFound = false;
    var schemas = SchemaRead(token).Schema.Data;
    for (var i = 0; i < schemas.length; i++) {
        if (schemas[i].Alias != null) {
            for (var j = 0; j < schemas[i].Alias.length; j++) {
                if (schemas[i].Alias[j] === "Udf/$TAG(PORTALS_PARTNER_PASSWORD)")
                    passwordFound = true;
            }
        }
    }
    return passwordFound;
}

function udfRegTokenExists(token) {
    var regTokenFound = false;
    var schemas = SchemaRead(token).Schema.Data;
    for (var i = 0; i < schemas.length; i++) {
        if (schemas[i].Alias != null) {
            for (var j = 0; j < schemas[i].Alias.length; j++) {
                if (schemas[i].Alias[j] === "Udf/$TAG(PORTALS_PARTNER_REGTOKEN)")
                    regTokenFound = true;
            }
        }
    }
    return regTokenFound;
}

function udfRegStatusExists(token) {
    var regStatusFound = false;
    var schemas = SchemaRead(token).Schema.Data;
    for (var i = 0; i < schemas.length; i++) {
        if (schemas[i].Alias != null) {
            for (var j = 0; j < schemas[i].Alias.length; j++) {
                if (schemas[i].Alias[j] === "Udf/$TAG(PORTALS_PARTNER_REGSTATUS)")
                    regStatusFound = true;
            }
        }
    }
    return regStatusFound;
}

function udfDefinitionsCreate(token, name, tag, schematype, folder_ID) {

    if (schematype === "EnumField<StringItem>") {
        var scRequest = {
            Token: token,
            "Schema": {
                "Data": {
                    "Key": null,
                    "AppliesTo": "AbEntry",
                    "Name": name,
                    "TAG": tag,
                    "Type": schematype,
                    "Items": [{"Key": null, "Value": "Inivited"}, {"Key": null, "Value": "Registered"}, {"Key": null, "Value": "Rejected"}],
                    "Folder": {
                        "Key": {
                            "ID": folder_ID
                        }
                    },
                    "Creator": {
                        "Key": {
                            "UID": null
                        }
                    },
                    "CreationDate": null,
                    "Inactive": false,
                    "Attributes": {
                        "MultiSelect": false,
                        "YesNo": false
                    }
                }
            }
        };
        return callMaximizerApi(apiUrl, 'schemaCreate', scRequest);

    } else {

        var scRequest = {
            Token: token,
            "Schema": {
                "Data": {
                    "Key": null,
                    "AppliesTo": "AbEntry",
                    "Name": name,
                    "TAG": tag,
                    "Type": schematype,
                    "Folder": {
                        "Key": {
                            "ID": folder_ID
                        }
                    },
                    "Creator": {
                        "Key": {
                            "UID": null
                        }
                    },
                    "CreationDate": null,
                    "Inactive": false,
                    "Attributes": {
                        "MaxLength": 255,
                        "Encrypted": false
                    }
                }
            }
        };
        return callMaximizerApi(apiUrl, 'schemaCreate', scRequest);
    }
}


function abEntryReadRegistrationInfo(token, abEntryKey, callback) {
    {
        var gfiRequest = {
            Token: token,
            "AbEntry": {
                "Criteria": { "SearchQuery": { "$EQ": { "Key": abEntryKey } } },
                "Scope": {
                    "Fields": {
                        Key: 1,
                        Type: 1,
                        Phone: 1,
                        Email: 1,
                        FullName: 1,
                        CompanyName: 1,
                        FirstName: 1,
                        MiddleName: 1,
                        LastName: 1,
                        ParentKey: 1,
                        Partner: 1,
                        ReadOnly: 1,
                        Category: 1
                        
                    }
                }
            },

            "Globalization": {
                "Culture": "en-US",
                "TimeZone": "UTC",
                "TimeZoneDesignator": true
            }
        };

        if (callback)
            callMaximizerApiAsync(apiUrl, 'abEntryRead', gfiRequest, callback);
        else
            return callMaximizerApi(apiUrl, 'abEntryRead', gfiRequest);
    }
}

function getOpportunityInfo(token, entryKey, callback) {
    {
        var request = {
            "Opportunity": {
                "Criteria": {
                    "SearchQuery": {
                        "$EQ": { "Key": { "Value": entryKey } }
                    }
                },
                "Scope": {
                    "Fields": {
                        "Key": {
                            "Value": 1
                        },
                        "CreationDate": 1,
                        "CloseDate": 1,
                        "LastModifyDate": 1,
                        "Objective": 1,
                        "ActualRevenue": 1,
                        "Cost": 1,
                        "AbEntry": {
                            "Key": {
                                "EntityType": 1,
                                "Value": 1,
                                "ID": 1,
                                "Number": 1
                            },
                            "CompanyName": 1,
                            "FullName": 1
                        }
                    }
                }
            },
            "Token": token
        };

        if (callback)
            callMaximizerApiAsync(apiUrl, 'OpportunityRead', request, callback);
        else
            return callMaximizerApi(apiUrl, 'OpportunityRead', request);
    }
}

function revokePartnerMembership(token, abEntryKey) {

    var gfiRequest = {
        Token: token,
        "AbEntry": {
            "Data": {
                "Key": abEntryKey,
                "Udf/$TAG(PORTALS_PARTNER_REGSTATUS)": null
            }
        },
        "Globalization": {
            "Culture": "en-US",
            "TimeZone": "UTC",
            "TimeZoneDesignator": true
        }
    };
    return callMaximizerApi(apiUrl, 'abEntryUpdate', gfiRequest);
}

function resetPartnerPassword(token, abEntryKey, encryptedPassword) {
    var gfiRequest = {
        Token: token,
        "AbEntry": {
            "Data": {
                "Key": abEntryKey,
                "Udf/$TAG(PORTALS_PARTNER_PASSWORD)": encryptedPassword
            }
        },
        "Globalization": {
            "Culture": "en-US",
            "TimeZone": "UTC",
            "TimeZoneDesignator": true
        }
    };

    return callMaximizerApi(apiUrl, 'abEntryUpdate', gfiRequest);
}

function getSessionInfo(token) {
    var gfiRequest = {
        Token: token,
        User: {
            Key: 1,
            DisplayName: 1
        },
        AddressBook: {
            UDBID: 1,
            Name: 1,
            DisplayValue: 1,
            Database: 1,
            Alias: 1
        },
        "Globalization": {
            "Culture": "en-US",
            "TimeZone": "$CURRENTUSER()",
            "TimeZoneDesignator": true
        }
    };
    return callMaximizerApi(apiUrl, 'GetSessionInfo', gfiRequest);
}

function getCategoryLabelOf11(token) {
    var gfiRequest = {
        "Token": token,
        "AbEntry": {
            "Options": {
                "Category": {
                    "Key": 1,
                    "DisplayValue": 1
                }
            }
        },
        "Globalization": {
            "Culture": "en-US",
            "TimeZone": "$CURRENTUSER()",
            "TimeZoneDesignator": true
        }
    };

    var response = callMaximizerApi(apiUrl, 'AbEntryGetFieldOptions', gfiRequest);

    var fieldList = JSON.stringify(response, null, 4);

    var result = (response.AbEntry.Data.Category.Items);
    var categoryLabel = '';
    for (var i = 0; i < result.length; i++) {
        if (result[i].Key === "11") {
            categoryLabel = result[i].DisplayValue;
            return categoryLabel;
        }
    }
}

function updateRegToken(token, abEntryKey, regToken) {

    var gfiRequest = {
        Token: token,
        "AbEntry": {
            "Data": {
                "Key": abEntryKey,
                "Udf/$TAG(PORTALS_PARTNER_REGTOKEN)": regToken
            }
        },
        "Globalization": {
            "Culture": "en-US",
            "TimeZone": "UTC",
            "TimeZoneDesignator": true
        }
    };
    return callMaximizerApi(apiUrl, 'abEntryUpdate', gfiRequest);
}

function sendInvitationEmail(token, subject, emailMsgBody, from, to) {
    var request = {
        Token: token,
        "EmailMessage": {
            "Data": {
                "Key": null,
                "from": from,
                "to": to,
                "cc": null,
                "bcc": null,
                "Subject": subject,
                "Type": 1,
                "Body": "<!DOCTYPE html><html><body>" + emailMsgBody + "</body></html>"
            }
        }
    };
    console.log(request);
    return callMaximizerApi(apiUrl, 'Create', request);
}
