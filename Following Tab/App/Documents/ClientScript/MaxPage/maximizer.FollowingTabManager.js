﻿// Uses: maximizer.Events.js

var maximizer = (function (maximizer) {
    "use strict";

    maximizer.DEBUG = false;

    var FollowingTabManager = function () {
        this.initialize();
    };

    var p = FollowingTabManager.prototype = new maximizer.EventManager();

    p.registerTab = function (tab, tabId) {
        //console.log('register');
        var that = this;
        if (tab) {
            tab.tabId = tabId;
            tab.showIndiCator = this.getShowIndiCator(tabId);
            tab.showPersonalOrMultiABEntryMSG = this.getShowPersonalOrMultiABEntryMSG(tabId);

            for (var i = 0; i < this.events.length; i += 1) {
                var e = this.events[i];
                tab.createEvent(e);

                if (e.properties.direction === 'out') {
                    // subscribe to tab's "out" events
                    tab.addEventHandler(e, function (e) {
                        that.dispatchEventFromTab(e);
                    });
                }
            }
            this.tabs.push(tab);
            this.currentTab = tab;

            var savedEventKeys = maximizer.getObjectKeys(this.savedEvents);
            // Check for parent key change events
            var updateCurrentRecord = true;
            for (var i = 0; i < savedEventKeys.length; i += 1) {
                var eventName = savedEventKeys[i];
                if (eventName == 'parentrecordchange') {
                    // Skip 'parentrecordupdate' combined with this event as it will cause a second refresh
                    updateCurrentRecord = false;
                }
            }
            for (var i = 0; i < savedEventKeys.length; i += 1) {
                var eventName = savedEventKeys[i];
                if (eventName != 'parentrecordupdate' || updateCurrentRecord) {
                    this.callCurrentTab(this.savedEvents[eventName]);
                }
            }

        }
    };

    p.switchTab = function (tab) {
        //        console.log('switching to tab ' + tab);
        this.callCurrentTab("blur");


        if (typeof tab === "string") {
            var t = null;
            for (var i = 0; i < this.tabs.length; i += 1) {
                if (this.tabs[i].tabId === tab) {
                    t = this.tabs[i];
                    break;
                }
            }
            if (t === null) {
                this.currentTab = null;

                //throw 'switchTab: Tab "' + tab + '" not found.';
            }
            tab = t;
        }

        if (tab) {
            this.currentTab = tab;
            this.callCurrentTab("beforefocus"); // this will eventually send the "focus" event to the tab
        }
        else {
            this.currentTab = null;
            if (this.tabs.length == 0) {
                // Event called on module switch before any tab is registered
                var event = { eventName: "beforefocus", direction: 'in' };
                this.savedEvents[event.eventName] = event;
            }
        }
    }

    p.reset = function () {
        this.tabs = [];             // Clean tab collection on init and when switching modules (onunload on a per-tab basis can't do it)
        this.currentTab = null;
        this.savedEvents = {};
        this.indicator = {};
        this.showmsg = {};
    };

    p.dispatchEventToTabs = function (event) {
        if (event.properties.direction == 'in') {
            this.savedEvents[event.eventName] = event;
            for (var i = 0; i < this.tabs.length; i += 1) {
                if (this.currentTab === this.tabs[i]) {
                    if (maximizer.DEBUG) {
                        console.log('fire ' + event.eventName + ' for tab #' + i);
                    }
                    event.properties.immediateEvent = true;
                    this.callCurrentTab(event);
                }
                else {
                    if (maximizer.DEBUG) {
                        console.log('save ' + event.eventName + ' for tab #' + i);
                    }
                    if (this.tabs[i].availableForEvents) {
                        this.tabs[i].saveEvent(event);
                    }
                }
            }
        }
    };

    p.dispatchEventFromTab = function (event, eventData) {
        this.clear(event);
        this.triggerEvent(event, eventData);
    };

    p.callCurrentTab = function (event, eventData) {
        if (this.currentTab && this.currentTab.availableForEvents) {
            this.currentTab.triggerEvent(event, eventData);
        }
    };
    p.getShowIndiCator = function (tabid) {

        return this.indicator[tabid];
    }
    p.getShowPersonalOrMultiABEntryMSG = function (tabid) {
        return this.showmsg[tabid];
    }
    p.initialize = function () {
        var that = this;

        this.reset();  // events for newly registered tabs

        var arrEvents = [
            { eventName: "beforeSplit", direction: 'in' },
            { eventName: "afterSplit", direction: 'in' },
            { eventName: "panelExpand", direction: 'in' },
            { eventName: "panelCollapse", direction: 'in' },
            { eventName: "resize", direction: 'in' },
            { eventName: "beforefocus", direction: 'in' },
            { eventName: "focus", direction: 'in' },
            { eventName: "blur", direction: 'in' },
            { eventName: "moduleSwitch", direction: 'in' },
            { eventName: "tabSwitch", direction: 'in' },
            { eventName: "parentClear", direction: 'in' },
            { eventName: "parentAbort", direction: 'in' },
            { eventName: "parentRecordUpdate", direction: 'in' },
            { eventName: "parentRecordChange", direction: 'in' },
            { eventName: "parentSelectionChange", direction: 'in' },
        //{ eventName: "getPrevNextStatus", direction: 'in' },
        //    
        //{eventName: "setPrevNextStatus", direction: 'out' },
            {eventName: "updateParentCurrentRecord", direction: 'out' },
            { eventName: "loadContent", direction: 'out' },
            { eventName: "updateContent", direction: 'out' },
            { eventName: "addtorecententries", direction: 'out' },
            { eventName: "addnewrecord", direction: 'out' }
        ];

        var e;

        for (var i = 0; i < arrEvents.length; i++) {
            e = arrEvents[i];
            this.addEventHandler(e, function (e) {
                that.dispatchEventToTabs(e);
            });
        }

    };

    maximizer.FollowingTabManager = FollowingTabManager;

    return maximizer;
} (maximizer || {}));


// FollowingTab

var maximizer = (function (maximizer) {
    "use strict";
    var FollowingTab = function () {
        this.initialize();
        this.initializeTab();
    }

    var p = FollowingTab.prototype = new maximizer.EventManager();
    p.showIndiCator = '';
    p.showPersonalOrMultiABEntryMSG = '';
    p.savedEvents = {};
    p.initializeTab = function () {
        this.savedEvents = {};
        this.active = false;
        this.currentModuleId = '';
        this.parentKey = "";
        this.tabSize = { width: 0, height: 0 };
        this.availableForEvents = true;
        this.showIndiCator = '';
        this.showPersonalOrMultiABEntryMSG = '';
        var that = this;

        this.on("focus", function () {

            that.active = true;
        });

        this.on("beforefocus", function () {
            top.$page.removeContentContainerFocus(); // TODO: replace with "this.controllingWindow.trigger("blur")"

            // Whatever happened when the tab was inactive - process these events
            var savedEventKeys = maximizer.getObjectKeys(that.savedEvents);
            // Check for parent key change events
            var updateCurrentRecord = true;
            for (var i = 0; i < savedEventKeys.length; i += 1) {
                var eventName = savedEventKeys[i];
                if (eventName == 'parentrecordchange') {
                    // Skip 'parentrecordupdate' combined with this event as it will cause a second refresh
                    updateCurrentRecord = false;
                }
            }
            for (var i = 0; i < savedEventKeys.length; i += 1) {
                var eventName = savedEventKeys[i];
                if (eventName != 'parentrecordupdate' || updateCurrentRecord) {
                    that.triggerEvent(that.savedEvents[eventName]);
                }
                delete that.savedEvents[eventName];
            }

            that.triggerEvent({ eventName: "focus", data: {} });
        });

        this.on("blur", function () {
            that.active = false;
        });

        this.on("parentRecordChange", function (e) {
            var newKey = e.data.key;

            if (that.parentKey != newKey) {
                if (maximizer.DEBUG) {
                    console.log('Tab\'s manager sets parentkey to ' + newKey);
                }
                that.parentKey = newKey;
            }
            else {
                if (maximizer.DEBUG) {
                    console.log('Tab\'s manager cancels setting parentkey to ' + newKey + '. Already set.');
                }
                top.$page.FollowingWindow.ontabloadend();
                e.cancelPropagation = true;
            }
        });

        this.on("resize", function (e) {
            if (e.data.width === that.tabSize.width && e.data.height === that.tabSize.height) {
                // filter out resize events that do not resize anything
                if (maximizer.DEBUG) {
                    console.log('Tab\'s manager cancels resize event. No size change.');
                }
                return false;
            }
            that.tabSize = { width: e.data.width, height: e.data.height };
        });
        /*
        this.on("panelExpand", function (e) {
        var newKey = e.data.key;

        if (that.parentKey != newKey) {
        if (maximizer.DEBUG) {
        console.log('Tab\'s manager sets parentkey to ' + newKey);
        }
        e.data.key = that.parentKey;
        } else {
        if (maximizer.DEBUG) {
        console.log('Tab\'s manager cancels setting parentkey to ' + newKey + '. Already set.');
        }
        top.$page.FollowingWindow.ontabloadend();
        }
        });
        */
    }

    p.start = function () {
        // find the manager

        if (top.$page && top.$page.registerFollowingTab) {
            top.$page.registerFollowingTab(this, window);

            var that = this;
            if (top.$page.FollowingWindowManager.currentTab != null && that.showIndiCator == 'Yes') {
                top.$page.FollowingWindow.ontabloadbegin();
            }
            else if (top.$page.FollowingWindowManager.currentTab != null && that.showIndiCator == 'No') {
                top.$page.FollowingWindow.ontabloadend();
                top.$page.FollowingWindow.hideLoading();
            }

            if (top.$page.FollowingWindow.currentModule == 'task' && top.$page.FollowingWindowManager.currentTab != null) {
                if (top.$page.FollowingWindow.stash != null && top.$page.FollowingWindow.stash.reason === 'noparent') {
                    that.parentKey = '';
                    that.managePersonalTasks(that.parentKey);
                }
            }
            else if (top.$page.FollowingWindow.currentModule != 'task' && top.$page.FollowingWindowManager.currentTab != null) {
                that.managePersonalTasks(that.parentKey);
            }
            if (top.$page.FollowingWindowManager.currentTab != null && top.$page.FollowingWindowManager.currentTab.parentKey == "" && that.showIndiCator == 'Yes') {
                top.$page.FollowingWindow.ontabloadend();
                top.$page.FollowingWindow.hideLoading();
            }

        }
        else {
            throw "maximizer.FollowingTab unable to initialize: window.top.$page not found.";
        }
    }
    p.saveEvent = function (event) {
        event.properties.immediateEvent = false;
        this.savedEvents[event.eventName] = event;
        //console.log('Event saved (' + event.eventName + ')');
    };
    p.removeEvent = function (event, savedEvents) {
        for (var i = 0; i < savedEvents.length; i++)
            if (this.savedEvents[i] = event) {
                this.savedEvents.splice(i, 1);
            }
    }

    // shortcut
    p.updateParentCurrentRecord = function (eventData) {
        this.triggerEvent({ eventName: "updateParentCurrentRecord", data: eventData });
    };

    // getter
    p.getParentKey = function () {
        return this.parentKey;
    };

    p.refresh = function () {
        this.triggerEvent("parentRecordUpdate", { key: this.getParentKey() });
    }
    p.parentReload = function () {
        top.$page.FollowingWindow.savedParams.grid.Reload();
    }

    p.managePersonalTasks = function (parentKey) {
        var msgElem = $('#fw_message');

        if (msgElem.length == 0) {
            msgElem = $('<div id="fw_message" class="fw_message" />'); // fw_message CSS class is defined in ContentPage.css
            $('body').prepend(msgElem);
        }

        if (typeof (top.taskMaxGrid) != 'undefined' && top.taskMaxGrid.GridID == 'tasks' && parentKey == '' && typeof (top.taskMaxGrid.DataManager) != 'undefined' && typeof (top.taskMaxGrid.DataManager.TableData) != 'undefined' && '' != top.taskMaxGrid.DataManager.TableData.CurrentKey && this.showPersonalOrMultiABEntryMSG == "Yes") {

            var m = top.JSS_MsgFollowingWindowHotlistPersonalParent || // Generated by ContentPage.aspx.cs
                'The current task or appointment is either personal or has multiple Address Book entries assigned. ' +
                'This following window only shows entries for tasks or appointments that have a single associated Address Book entry.';

            // Hiding contents except message


            msgElem.text(m).show();
            top.$page.FollowingWindow.hideLoading();
        }
        else if (typeof (top.taskMaxGrid) != 'undefined' && top.taskMaxGrid.GridID == 'tasks' && parentKey != '' && typeof (top.taskMaxGrid.DataManager) != 'undefined' && typeof (top.taskMaxGrid.DataManager.TableData) != 'undefined' && '' != top.taskMaxGrid.DataManager.TableData.CurrentKey && this.showPersonalOrMultiABEntryMSG == "Yes") {
            msgElem.hide();
            //top.$page.FollowingWindow.hideLoading();
        } else if (msgElem.length > 0 && this.showPersonalOrMultiABEntryMSG == "No") {
            msgElem.remove();
            // top.$page.FollowingWindow.hideLoading();
        }
        else if (typeof (top.taskMaxGrid) != 'undefined' && top.taskMaxGrid.GridID != 'tasks' && msgElem.length > 0) {
            msgElem.remove();
        }
        else {
            msgElem.hide();
            // top.$page.FollowingWindow.hideLoading();
        }
    };
    maximizer.FollowingTab = FollowingTab;

    return maximizer;
} (maximizer || {}));

// usage:
/**
$(function () {
var fwTestTab = new maximizer.FollowingTab();

fwTestTab.addEventHandler("parentRecordChange", function (e) { console.log('f1:' + e.data.key); });

var fwTestTab2 = new maximizer.FollowingTab();

fwTestTab2.addEventHandler("parentRecordChange", function (e) { console.log('f2:' + e.data.key); });

});
/**/
