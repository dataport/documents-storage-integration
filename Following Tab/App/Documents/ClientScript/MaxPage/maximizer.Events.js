﻿// using namespace maximizer

// maximizer.Event

var maximizer = (function (maximizer) {
    "use strict";

    maximizer.getObjectKeys = function (obj) {
        var keys = [];
        for (var prop in obj) {
            if (obj.hasOwnProperty(prop)) {
                keys.push(prop);
            }
        }
        return keys;
    };

    var Event = function (e) {
        this.initialize(e);
    };

    var p = Event.prototype;

    p.eventName = null;
    p.handlers = null;
    p.data = null;
    p.properties = null;

    p.initialize = function (e) {
        this.properties = {};
        this.data = {};
        this.handlers = [];

        if (e && typeof e === 'string') {
            e = { eventName: e };
        }
        if (e.eventName && typeof e.eventName === 'string') {
            this.eventName = e.eventName.toLowerCase();
            var propKeys = maximizer.getObjectKeys(e);

            for (var i = 0; i < propKeys.length; i += 1) {
                var prop = propKeys[i];
                if (prop !== 'aeventName') {
                    this.properties[prop] = e[prop];
                }
            }
        }
        else {
            throw "maximizer.Event.initialize: Invalid event object; expected eventName property.";
        }
    };


    p.addHandler = function (handler) {
        if (typeof handler === 'function') {
            this.handlers.push(handler);
        }
        else {
            throw "maximizer.Event.addHandler: (event '" + this.eventName + "'): invalid parameter (handler is not a function))";
        }
    };

    p.removeAllHandlers = function () {
        while (this.handlers.length > 0) {
            this.handlers.pop();
        }
    };

    p.trigger = function (eventData) {
        var i, h;
        this.cancelPropagation = false;
        for (i = 0; i < this.handlers.length; i += 1) {
            h = this.handlers[i];

            if (typeof h === 'function') {
                try {
                    this.data = eventData;
                    var result = h.call(null, this);
                    if (eventData && eventData.onsuccess) {
                        eventData.onsuccess();
                    }
                    if (result === false || this.cancelPropagation === true) {
                        break;
                    }
                } catch (e) {
                    if (eventData && eventData.onerror) {
                        eventData.onerror(e);
                    }
                    else {
                        throw "maximizer.Event.trigger: (event '" + this.eventName + "'): " + e;
                    }
                }
            }
        }
    };

    maximizer.Event = Event;

    return maximizer;
} (maximizer || {}));



// maximizer.EventManager

var maximizer = (function (maximizer) {
    "use strict";

    var EventManager = function () {
        this.initialize();
    };

    var p = EventManager.prototype;

    // Constructor
    p.initialize = function () {
        this.events = [];
    };


    // Instantiates an Event object and adds it to the collection of events
    p.createEvent = function (e) {

        var event = this._getEventByName(e);    // already exists?

        if (event === null) {
            // instantiate Event object here
            event = new maximizer.Event(e);
            if (event) {
                // add to the collection
                this.events.push(event);
            }
        }
        return event;
    };


    // Attaches a handler to an event
    p.addEventHandler = function (e, handler) {

        var 
            event = this.createEvent(e);

        if (event) {
            event.addHandler(handler);
        }
        else {
            throw "maximizer.EventManager.addEventHandler: unable to add event '" + e.eventName + "'.";
        }
    };

    // Clears all event handlers (as in notes dialog previous and next)
    p.clearEventHandlers = function (e) {

        var 
            event = this.createEvent(e);

        if (event) {
            event.removeAllHandlers();
        }
        else {
            throw "maximizer.EventManager.clearEventHandlers: unable to clear event '" + e.eventName + "'.";
        }
    };

    // Calls all event's handlers with eventData, if present
    p.triggerEvent = function (e, eventData) {

        var 
            event = this._getEventByName(e);

        if (event) {

            event
                .trigger(eventData || ((typeof e === 'object') ? e.data || {} : {}));
        }
        else {
            throw "maximizer.EventManager.triggerEvent: unable to trigger event '" + ((typeof e === 'object') ? e.eventName || e : e) + "'";
        }
    };

    // helper, not to be used publicly. Finds and returns an event by its name (case-insensitive)
    p._getEventByName = function (e) {

        var eventName = (typeof e === 'string') ? e : e.eventName || "";

        if (eventName && typeof eventName === 'string') {

            var i;

            for (i = 0; i < this.events.length; i += 1) {
                var 
                    event = this.events[i];

                if (event && event.eventName && event.eventName.toLowerCase() === eventName.toLowerCase()) {
                    return event;
                }
            }
        }
        else {
            throw eventName;
        }
        return null;
    };

    // shortcut to addEventHandler
    p.on = function (e, handler) {
        return this.addEventHandler(e, handler);
    }

    p.clear = function (e) {
        this.clearEventHandlers(e);
    }

    // shortcut to triggerEvent
    p.trigger = function (e, eventData) {
        this.triggerEvent(e, eventData);
    };


    maximizer.EventManager = EventManager;

    return maximizer;
} (maximizer || {}));
