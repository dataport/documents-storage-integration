this._cssname;



function css_create(tablename, inputCss) {
    if (document.getElementById("css_name").value == "") {
        document.getElementById("css_result").style.color = "white";
        document.getElementById("css_result").style.backgroundColor = "red";
        document.getElementById("css_result").innerHTML = "Name cannnot be empty";
        return;
    }
    var css = {};
    css.Key = document.getElementById("css_key").value;
    if (tablename != undefined) {
        css.Name = this._cssname = tablename;
    } else {
        css.Name = this._cssname;
    }
    css.ApplicationId = "MaxExternalPortals";
    css.Description = document.getElementById("css_description").value;
    css.User = _user;
		
	if (inputCss) {
		css.Info = inputCss;
	} else {
		try {
		    css.Info = document.getElementById('css_custom').value;
		} catch (Exception) {
			css.Info = "";
		}
	}

    var doneCallBack = function (response) {
        document.getElementById("css_result").style.color = "white";
        document.getElementById("css_result").style.backgroundColor = "green";
        document.getElementById("css_result").innerHTML = "record created";
        css_read(tablename);
    }
    var failCallBack = function (error) {
        document.getElementById("css_result").style.color = "white";
        document.getElementById("css_result").style.backgroundColor = "red";
        try {
            document.getElementById("css_result").innerHTML = "error: " + error.responseJSON.ExceptionMessage;
			jqAlert("CSS Create Error", "Error Creating CSS Customization record: " + error.responseJSON.ExceptionMessage);
			
        } catch (Exception) {
            document.getElementById("css_result").innerHTML = "error: " + error.toString();
			jqAlert("CSS Create Error", "Error Creating CSS Customization record: " + error.toString());
        }
    }
	
    getMaxToken(function (token) {
        meta.Token = token;
		postCSSCustomizationApi(meta, css, doneCallBack, failCallBack, null);
	});
}
function css_read(tablename, onSuccess, onFailure) {
    if (tablename == "" || tablename == undefined) {
        document.getElementById("css_result").style.color = "white";
        document.getElementById("css_result").style.backgroundColor = "red";
        document.getElementById("css_result").innerHTML = "Name cannnot be empty";
        return;
    }
    this._cssname = tablename;
    var doneCallback = function (response) {
        if (response != null) {
            if (response.Key == '' || response.Key == null) {
                if (onFailure)
                    return onFailure();
                else
                    return jqAlert("CSS Read Error", "Response.key== '' or null");
            }
				
            document.getElementById("css_result").innerHTML = "record loaded";
            document.getElementById("css_key").value = response.Key;
            document.getElementById("css_ID").value = response.ApplicationId;
            document.getElementById("css_description").value = response.Description;
            var cols = response.Info;
            document.getElementById("css_custom").value = cols;
        
            if (onSuccess)
                return onSuccess();
        }else
            return onFailure();
    }
    var failCallBack = function (error) {
		if (onFailure)
			return onFailure();
		 
		// This is not reliable. The response does NOT seem to be the same on every machine.
		
        document.getElementById("css_result").style.color = "white";
        document.getElementById("css_result").style.backgroundColor = "red";
        try {
            document.getElementById("css_result").innerHTML = "error: " + error.responseJSON.ExceptionMessage;
			jqAlert("CSS Read Error", "Error Reading CSS Customization record: " + error.responseJSON.ExceptionMessage);
        } catch (Exception) {
            document.getElementById("css_result").innerHTML = "error: " + error.toString();
			jqAlert("CSS Read Error", "Error Reading CSS Customization record: " + error.toString());
        }
    }
    var alwaysCallBack = function () { };
	
	getMaxToken(function (token) {
        meta.Token = token;
		getCSSCustomizationApi(JSON.stringify(meta), tablename, this._user, doneCallback, failCallBack, alwaysCallBack);
	});
}

function css_update(tablename) {
    if (document.getElementById("css_key").value == "") {
        document.getElementById("css_result").style.color = "white";
        document.getElementById("css_result").style.backgroundColor = "red";
        document.getElementById("css_result").innerHTML = "Key cannnot be empty";
        return;
    }
    var css = {};
    css.Key = document.getElementById("css_key").value;
    if (tablename != undefined) {
        css.Name = this._cssname = tablename;
    } else {
        css.Name = this._cssname;
    }
    css.ApplicationId = document.getElementById("css_ID").value;
    css.Description = document.getElementById("css_description").value;
    css.User = this._user;
    var temp;
    try {
        var info = document.getElementById('css_custom').value;
        css.Info = info;
    } catch (Exception) {
        css.Info = "";
    }

    var doneCallBack = function (response) {
        document.getElementById("css_result").style.color = "white";
        document.getElementById("css_result").style.backgroundColor = "green";
        if (response == true) {
            document.getElementById("css_result").innerHTML = "result updated";
			jqAlert("Update Successful", "CSS has been saved successfully!");
		}
        else {
            document.getElementById("css_result").innerHTML = "result is not updated";
			jqAlert("CSS Update Error", "Error updating CSS:" + JSON.stringify(response));
		}
    }
    var failCallBack = function (error) {
        document.getElementById("css_result").style.color = "white";
        document.getElementById("css_result").style.backgroundColor = "red";
        try {
            document.getElementById("css_result").innerHTML = "error: " + error.responseJSON.ExceptionMessage;
			jqAlert("CSS Update Error", "Error updating CSS: " + error.responseJSON.ExceptionMessage);
        } catch (Exception) {
            document.getElementById("css_result").innerHTML = "error: " + error.toString();
			jqAlert("CSS Update Error", "Error updating CSS: " + error.toString());
        }
    }
    var alwaysCallBack = function () { };
	
	getMaxToken(function (token) {
        meta.Token = token;
		putCSSCustomizationApi(meta, css, doneCallBack, failCallBack, alwaysCallBack);
	});
}