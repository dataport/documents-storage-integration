this._labelname = "Partner.App.Labels";
this._lblname;
	
function label_create(inputLabel, onSuccess) {
	if (document.getElementById("label_name").value == "") {
		document.getElementById("label_result").style.color = "white";
		document.getElementById("label_result").style.backgroundColor = "red";
		document.getElementById("label_result").innerHTML = "Name cannnot be empty";
		return;
	}
	var labels = {};
	labels.Key = document.getElementById("label_key").value;
	labels.Name = this._labelname;
	labels.ApplicationId = "MaxExternalPortals";
	labels.Description = document.getElementById("label_description").value;
	labels.User = _user;

	if (inputLabel)
	    labels.Fields = inputLabel;
	else {
		// Initial DB condition.
		if (!document.getElementById('label_json') || !document.getElementById('label_json').value || document.getElementById('label_json').value=="")
			document.getElementById('label_json').value = "[]";
		
		try{
			var jsonobject = JSON.parse(document.getElementById('label_json').value);
			labels.Fields = jsonobject;
		} catch (Exception) {
			labels.Fields = [];
		}
	}

	var doneCallBack = function (response) {
		document.getElementById("label_result").style.color = "white";
		document.getElementById("label_result").style.backgroundColor = "green";
		document.getElementById("label_result").innerHTML = "record created";
		label_read(onSuccess);
	}
	var failCallBack = function (error) {
		document.getElementById("label_result").style.color = "white";
		document.getElementById("label_result").style.backgroundColor = "red";
        try {
            document.getElementById("label_result").innerHTML = "error: " + error.responseJSON.ExceptionMessage;
			jqAlert("Custom Label Error", "Error Creating Custom Label record: " + error.responseJSON.ExceptionMessage);
			
        } catch (Exception) {
            document.getElementById("label_result").innerHTML = "error: " + error.toString();
			jqAlert("Custom Label Error", "Error Creating Custom Label record: " + error.toString());
        }
	}
	getMaxToken(function (token) {
	    meta.Token = token;
	    meta.AddressBook = database;
		postLabelCustomizationApi(meta, labels, doneCallBack, failCallBack, null);
	});
}
function label_read(onSuccess, onFailure) {
	if (this._labelname == "") {
		document.getElementById("label_result").style.color = "white";
		document.getElementById("label_result").style.backgroundColor = "red";
		document.getElementById("label_result").innerHTML = "Name cannnot be empty";
		return;
	}
	var doneCallback = function (response) {
        if (response.Key == '' || response.Key == null) {
			if (onFailure)
				return onFailure();
			else
				return jqAlert("Label Custom Read Error", "Response.key== '' or null");
		}
		
		document.getElementById("label_result").style.color = "white";
		document.getElementById("label_result").style.backgroundColor = "green";
		if (response == null) {
			document.getElementById("label_result").innerHTML = 'result: no record found';
			return;
		}
		document.getElementById("label_result").innerHTML = "record loaded";
		document.getElementById("label_key").value = response.Key;
		document.getElementById("label_ID").value = response.ApplicationId;
		document.getElementById("label_description").value = response.Description;
		var fields = response.Info;
		document.getElementById("label_json").value = JSON.stringify(fields);
		
		if (onSuccess) onSuccess();
	}
	var failCallBack = function (error) {
		if (onFailure)
			return onFailure();
		 
		// This is not reliable. The response does NOT seem to be the same on every machine.
		if (error.responseJSON.ExceptionMessage == 'No record found') {   
			return label_create();
		}
		
		document.getElementById("label_result").style.color = "white";
		document.getElementById("label_result").style.backgroundColor = "red";
		
		try {
            document.getElementById("label_result").innerHTML = "error: " + error.responseJSON.ExceptionMessage;
			jqAlert("Custom L:abel Error", "Error Reading Custom Label record: " + error.responseJSON.ExceptionMessage);
        } catch (Exception) {
            document.getElementById("label_result").innerHTML = "error: " + error.toString();
			jqAlert("Custom Label Error", "Error Reading Custom Label record: " + error.toString());
        }
	}
	var alwaysCallBack = function () { };
	getMaxToken(function (token) {
	    meta.Token = token;
	    meta.AddressBook = database;
		getLabelCustomizationApi(JSON.stringify(meta), this._labelname, this._user, doneCallback, failCallBack, alwaysCallBack);
	});
}
function label_update() {
	if (document.getElementById("label_key").value == "") {
		document.getElementById("label_result").style.color = "white";
		document.getElementById("label_result").style.backgroundColor = "red";
		document.getElementById("label_result").innerHTML = "Key cannnot be empty";
		return;
	}
	var labels = {};
	labels.Key = document.getElementById("label_key").value;
	labels.Name = this._labelname;
	labels.ApplicationId = document.getElementById("label_ID").value;
	labels.Description = document.getElementById("label_description").value;
	labels.User = this._user;

	if (!document.getElementById("label_json") || !document.getElementById("label_json").value)
		return;
	
	var inputJsonStr = document.getElementById("label_json").value;
	var errStr = jsonValidate(inputJsonStr);
	if (errStr) {
		jqAlert("Save Error", errStr);
		return;
	}
	else {
		try {
			labels.Fields = JSON.parse(document.getElementById("label_json").value);
		} catch (Exception) {
			jqAlert("Save Error", "The JSON document seems to be invalid.");
			return;
		}
	}

	var doneCallBack = function (response) {
		document.getElementById("label_result").style.color = "white";
		document.getElementById("label_result").style.backgroundColor = "green";
        if (response == true) {
            document.getElementById("label_result").innerHTML = "result updated";
			jqAlert("Update Successful", "Custom Label has been saved successfully!");
		}
        else {
            document.getElementById("label_result").innerHTML = "result is not updated";
			jqAlert("Custom Label Error", "Error updating Custom Label:" + JSON.stringify(response));
		}
	}
	var failCallBack = function (error) {
		document.getElementById("label_result").style.color = "white";
		document.getElementById("label_result").style.backgroundColor = "red";
        try {
            document.getElementById("label_result").innerHTML = "error: " + error.responseJSON.ExceptionMessage;
			jqAlert("Custom Label Error", "Error updating Custom Label: " + error.responseJSON.ExceptionMessage);
        } catch (Exception) {
            document.getElementById("label_result").innerHTML = "error: " + error.toString();
			jqAlert("Custom Label Error", "Error updating Custom Label: " + error.toString());
        }
	}
	var alwaysCallBack = function () { };
	getMaxToken(function (token) {
	    meta.Token = token;
	    meta.AddressBook = database;
		putLabelCustomizationApi(meta, labels, doneCallBack, failCallBack, alwaysCallBack);
	});
}


// Next Version

function lbl_create(tablename, input_lbl, onSuccess) {
    if (document.getElementById("label_name").value == "") {
        document.getElementById("label_result").style.color = "white";
        document.getElementById("label_result").style.backgroundColor = "red";
        document.getElementById("label_result").innerHTML = "Name cannnot be empty";
        return;
    }
    var lbl = {};
    lbl.Key = document.getElementById("label_key").value;
    if (tablename != undefined) {
        lbl.Name = this._lblname = tablename;
    } else {
        lbl.Name = this._lblname;
    }
    lbl.ApplicationId = "MaxExternalPortals";
    lbl.Description = document.getElementById("label_description").value;
    lbl.User = _user;

    if (input_lbl) {
        lbl.Info = input_lbl;
    } else {
        try {
            lbl.Info = document.getElementById('label_json').value;
        } catch (Exception) {
            lbl.Info = "";
        }
    }

    var doneCallBack = function (response) {
        document.getElementById("label_result").style.color = "white";
        document.getElementById("label_result").style.backgroundColor = "green";
        document.getElementById("label_result").innerHTML = "record created";
        lbl_read(tablename,onSuccess);
    }
    var failCallBack = function (error) {
        document.getElementById("label_result").style.color = "white";
        document.getElementById("label_result").style.backgroundColor = "red";
        try {
            document.getElementById("label_result").innerHTML = "error: " + error.responseJSON.ExceptionMessage;
            jqAlert("Label Create Error", "Error Creating Label Customization record: " + error.responseJSON.ExceptionMessage);

        } catch (Exception) {
            document.getElementById("label_result").innerHTML = "error: " + error.toString();
            jqAlert("Label Create Error", "Error Creating Label Customization record: " + error.toString());
        }
    }

    getMaxToken(function (token) {
        meta.Token = token;
        meta.AddressBook = database;
        postLabelCustomizationApi(meta, lbl, doneCallBack, failCallBack, null);
    });
}
function lbl_read(tablename, onSuccess, onFailure) {
    if (tablename == "" || tablename == undefined) {
        document.getElementById("label_result").style.color = "white";
        document.getElementById("label_result").style.backgroundColor = "red";
        document.getElementById("label_result").innerHTML = "Name cannnot be empty";
        return;
    }
    this._lblname = tablename;
    var doneCallback = function (response) {
        if (response != null) {
            if (response.Key == '' || response.Key == null) {
                if (onFailure)
                    return onFailure();
                else
                    return jqAlert("Label Read Error", "Response.key== '' or null");
            }

            document.getElementById("label_result").innerHTML = "record loaded";
            document.getElementById("label_key").value = response.Key;
            document.getElementById("label_ID").value = response.ApplicationId;
            document.getElementById("label_description").value = response.Description;
            var cols = response.Info;
            document.getElementById("label_json").value = cols;

            if (onSuccess)
                return onSuccess();
        } else
            return onFailure();
    }
    var failCallBack = function (error) {
        if (onFailure)
            return onFailure();

        // This is not reliable. The response does NOT seem to be the same on every machine.

        document.getElementById("label_result").style.color = "white";
        document.getElementById("label_result").style.backgroundColor = "red";
        try {
            document.getElementById("label_result").innerHTML = "error: " + error.responseJSON.ExceptionMessage;
            jqAlert("Label Read Error", "Error Reading Label Customization record: " + error.responseJSON.ExceptionMessage);
        } catch (Exception) {
            document.getElementById("label_result").innerHTML = "error: " + error.toString();
            jqAlert("Label Read Error", "Error Reading Label Customization record: " + error.toString());
        }
    }
    var alwaysCallBack = function () { };

    getMaxToken(function (token) {
        meta.Token = token;
        meta.AddressBook = database;
        getLabelCustomizationApi(JSON.stringify(meta), tablename, this._user, doneCallback, failCallBack, alwaysCallBack);
    });
}
function lbl_update(tablename) {
    if (document.getElementById("label_key").value == "") {
        document.getElementById("label_result").style.color = "white";
        document.getElementById("label_result").style.backgroundColor = "red";
        document.getElementById("label_result").innerHTML = "Key cannnot be empty";
        return;
    }
    var lbl = {};
    lbl.Key = document.getElementById("label_key").value;
    if (tablename != undefined) {
        lbl.Name = this._lblname = tablename;
    } else {
        lbl.Name = this._lblname;
    }
    lbl.ApplicationId = document.getElementById("label_ID").value;
    lbl.Description = document.getElementById("label_description").value;
    lbl.User = this._user;
    var temp;
    try {
        var info = document.getElementById('label_json').value;
        lbl.Info = info;
    } catch (Exception) {
        lbl.Info = "";
    }

    var doneCallBack = function (response) {
        document.getElementById("label_result").style.color = "white";
        document.getElementById("label_result").style.backgroundColor = "green";
        if (response == true) {
            document.getElementById("label_result").innerHTML = "result updated";
            jqAlert("Update Successful", "Label has been saved successfully!");
        }
        else {
            document.getElementById("label_result").innerHTML = "result is not updated";
            jqAlert("Label Update Error", "Error updating Label:" + JSON.stringify(response));
        }
    }
    var failCallBack = function (error) {
        document.getElementById("label_result").style.color = "white";
        document.getElementById("label_result").style.backgroundColor = "red";
        try {
            document.getElementById("label_result").innerHTML = "error: " + error.responseJSON.ExceptionMessage;
            jqAlert("Label Update Error", "Error updating Label: " + error.responseJSON.ExceptionMessage);
        } catch (Exception) {
            document.getElementById("label_result").innerHTML = "error: " + error.toString();
            jqAlert("Label Update Error", "Error updating Label: " + error.toString());
        }
    }
    var alwaysCallBack = function () { };

    getMaxToken(function (token) {
        meta.Token = token;
        meta.AddressBook = database;
        putLabelCustomizationApi(meta, lbl, doneCallBack, failCallBack, alwaysCallBack);
    });
}