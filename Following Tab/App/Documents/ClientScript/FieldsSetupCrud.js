this._fsuname = "Partner.Ab.FieldsSetUp";

function fsu_create(name, inputFsu, onSuccess) {
    if (document.getElementById("fsu_name").value == "") {
        document.getElementById("fsu_result").style.color = "white";
        document.getElementById("fsu_result").style.backgroundColor = "red";
        document.getElementById("fsu_result").innerHTML = "Name cannnot be empty";
        return;
    }
    var fieldssetup = {};
    fieldssetup.Key = document.getElementById("fsu_key").value;
    fieldssetup.Name = name;
    fieldssetup.ApplicationId = "MaxExternalPortals";
    fieldssetup.Description = document.getElementById("fsu_description").value;
    fieldssetup.User = _user;

    if (inputFsu)
        fieldssetup.Fields = inputFsu;
    else {
        // Initial DB condition.
        if (!document.getElementById('fsu_json') || !document.getElementById('fsu_json').value || document.getElementById('fsu_json').value == "")
            document.getElementById('fsu_json').value = "[]";

        try {
            var jsonobject = JSON.parse(document.getElementById('fsu_json').value);
            fieldssetup.Fields = jsonobject;
        } catch (Exception) {
            fieldssetup.Fields = [];
        }
    }

    var failCallBack = function (error) {
        document.getElementById("fsu_result").style.color = "white";
        document.getElementById("fsu_result").style.backgroundColor = "red";
        try {
            document.getElementById("fsu_result").innerHTML = "error: " + error.responseJSON.ExceptionMessage;
            jqAlert("Fields Setup Error", "Error Creating Fields Setup record: " + error.responseJSON.ExceptionMessage);

        } catch (Exception) {
            document.getElementById("fsu_result").innerHTML = "error: " + error.toString();
            jqAlert("Fields Setup Error", "Error Creating Fields Setup record: " + error.toString());
        }
    };

    var doneCallBack = function (response) {
        document.getElementById("fsu_result").style.color = "white";
        document.getElementById("fsu_result").style.backgroundColor = "green";
        document.getElementById("fsu_result").innerHTML = "record created";
        fsu_read(name, onSuccess, failCallBack);
    };

    getMaxToken(function (token) {
        meta.Token = token;
        meta.AddressBook = database;
        postFieldsSetUpApi(meta, fieldssetup, doneCallBack, failCallBack, null);
    });
}
function fsu_read(name, onSuccess, onFailure) {
    if (name == "") {
        document.getElementById("fsu_result").style.color = "white";
        document.getElementById("fsu_result").style.backgroundColor = "red";
        document.getElementById("fsu_result").innerHTML = "Name cannnot be empty";
        return;
    }
    var doneCallback = function (response) {
        if (response.Key == '' || response.Key == null) {
            if (onFailure)
                return onFailure();
            else
                return jqAlert("FSU Read Error", "Response.key== '' or null");
        }


        document.getElementById("fsu_result").style.color = "white";
        document.getElementById("fsu_result").style.backgroundColor = "green";
        if (response == null) {
            document.getElementById("fsu_result").innerHTML = 'result: no record found';
            return;
        }
        document.getElementById("fsu_result").innerHTML = "record loaded";
        document.getElementById("fsu_key").value = response.Key;
        document.getElementById("fsu_ID").value = response.ApplicationId;
        document.getElementById("fsu_description").value = response.Description;
        var fields = response.Fields;
        document.getElementById("fsu_json").value = JSON.stringify(fields);
        if (response == null) {
            document.getElementById("fsu_result").innerHTML = 'result: no record found';
            return;
        }

        var cols = response.Fields;
        if (cols != null) {
            drawFsuTable(fullModuleAry[name.split('.')[1]], cols.length);
            document.getElementById("fsu_json").value = JSON.stringify(cols);
            for (var i = 0; (i < cols.length && document.getElementById('fsu_colName' + cols.length) != null) ; i++) {
                document.getElementById("fsu_colName" + (i + 1)).value = cols[i].Label;
                document.getElementById("fsu_colContent" + (i + 1)).value = cols[i].Content;
                document.getElementById("fsu_mandatory" + (i + 1)).checked = cols[i].Mandatory;
                document.getElementById("fsu_read" + (i + 1)).checked = cols[i].ReadOnly;
                document.getElementById("fsu_modify" + (i + 1)).checked = cols[i].ShowInEditForm;
                document.getElementById("fsu_create" + (i + 1)).checked = cols[i].ShowInCreateForm;
                getDefaultControl(document.getElementById("fsu_colContent" + (i + 1)), fullModuleAry[name.split('.')[1]]);
                var tempDefault;
                tempDefault = $("#fsu_default" + (i + 1) + " select");
                if (tempDefault.length == 0)
                    tempDefault = $("#fsu_default" + (i + 1) + " input");
                

                // Code to detect multivaluetable Default values in Opportunity FieldSetUp
                if ($("#fsu_default" + (i + 1) + " select").hasClass("multiselect") == true && cols[i].DefaultValue != null) {
                    $("#fsu_default" + (i + 1) + " select").multiselect('select', cols[i].DefaultValue.split(','));
                }
                else {
                    tempDefault.val(cols[i].DefaultValue);
                }

            }
        }


        if (onSuccess) onSuccess();
    }
    var failCallBack = function (error) {
        if (onFailure)
            return onFailure();

        // This is not reliable. The response does NOT seem to be the same on every machine.
        if (error.responseJSON.ExceptionMessage == 'No record found') {
            return fsu_create();
        }

        document.getElementById("fsu_result").style.color = "white";
        document.getElementById("fsu_result").style.backgroundColor = "red";

        try {
            document.getElementById("fsu_result").innerHTML = "error: " + error.responseJSON.ExceptionMessage;
            jqAlert("Fields Setup Error", "Error Reading Fields Setup record: " + error.responseJSON.ExceptionMessage);
        } catch (Exception) {
            document.getElementById("fsu_result").innerHTML = "error: " + error.toString();
            jqAlert("Fields Setup Error", "Error Reading Fields Setup record: " + error.toString());
        }
    }
    var alwaysCallBack = function () { };
    getMaxToken(function (token) {
        meta.Token = token;
        meta.AddressBook = database;
        getFieldsSetUpApi(JSON.stringify(meta), name, this._user, doneCallback, failCallBack, alwaysCallBack);
    });
}
function fsu_update(name) {
    if (document.getElementById("fsu_key").value == "") {
        document.getElementById("fsu_result").style.color = "white";
        document.getElementById("fsu_result").style.backgroundColor = "red";
        document.getElementById("fsu_result").innerHTML = "Key cannnot be empty";
        return;
    }
    if (name == undefined) {
        name = this._portalCategory + '.' + document.getElementById('sel_FieldsSetUp').value + '.FieldsSetUp'
    }
    var fieldssetup = {};
    fieldssetup.Key = document.getElementById("fsu_key").value;
    fieldssetup.Name = name;
    fieldssetup.ApplicationId = document.getElementById("fsu_ID").value;
    fieldssetup.Description = document.getElementById("fsu_description").value;
    fieldssetup.User = this._user;
    var columns = [];
    var temp;
    if (_jsontoggle) {
        var jsonobject = JSON.parse(document.getElementById("fsu_json").value);
        fieldssetup.Fields = jsonobject;
    } else {
        for (var i = 1; i <= $('#fsu_tab_content tbody tr').length; i++) {

            temp = $('#fsu_tab_content tbody tr:nth-child(' + i + ') td:nth-child(2) input').attr('id');
            index = temp.replace('fsu_colName', '');
            tempName = document.getElementById("fsu_colName" + index).value;
            tempContent = document.getElementById("fsu_colContent" + index).value;
            tempDefault = $("#fsu_default" + index + " select");
            if (tempDefault.length == 0) {
                tempDefault = $("#fsu_default" + index + " input");
            }
            tempDefault = tempDefault.val();
            if (typeof tempDefault == 'object' && tempDefault != null) {
                tempDefault = tempDefault.join(',');
            }
            tempReadOnly = document.getElementById("fsu_read" + index).checked;
            tempMandatory = document.getElementById("fsu_mandatory" + index).checked;
            tempCreate = document.getElementById("fsu_create" + index).checked;
            tempModify = document.getElementById("fsu_modify" + index).checked;
            if ((tempName != "" && tempName != null && tempName != undefined) &&
				(tempContent != "")) {
                var column = {};
                column.Label = tempName;
                column.Content = tempContent;
                column.Mandatory = tempMandatory;
                column.ReadOnly = tempReadOnly;
                column.ShowInCreateForm = tempCreate;
                column.ShowInEditForm = tempModify;
                column.DefaultValue = tempDefault;
                columns[columns.length] = column;
            }
        }
        fieldssetup.Fields = columns;
    }

    var doneCallBack = function (response) {
        document.getElementById("fsu_result").style.color = "white";
        document.getElementById("fsu_result").style.backgroundColor = "green";
        if (response == true) {
            document.getElementById("fsu_result").innerHTML = "result updated";
            jqAlert("Update Successful", "Fields Setup has been saved successfully!");
        }
        else {
            document.getElementById("fsu_result").innerHTML = "result is not updated";
            jqAlert("Fields Setup Error", "Error updating Fields Setup:" + JSON.stringify(response));
        }
    }
    var failCallBack = function (error) {
        document.getElementById("fsu_result").style.color = "white";
        document.getElementById("fsu_result").style.backgroundColor = "red";
        try {
            document.getElementById("fsu_result").innerHTML = "error: " + error.responseJSON.ExceptionMessage;
            jqAlert("Fields Setup Error", "Error updating Fields Setup: " + error.responseJSON.ExceptionMessage);
        } catch (Exception) {
            document.getElementById("fsu_result").innerHTML = "error: " + error.toString();
            jqAlert("Fields Setup Error", "Error updating Fields Setup: " + error.toString());
        }
    }
    var alwaysCallBack = function () { };
    getMaxToken(function (token) {
        meta.Token = token;
        meta.AddressBook = database;
        putFieldsSetUpApi(meta, fieldssetup, doneCallBack, failCallBack, alwaysCallBack);
    });
}
function fsu_update_backup(name) {
    if (document.getElementById("fsu_key").value == "") {
        document.getElementById("fsu_result").style.color = "white";
        document.getElementById("fsu_result").style.backgroundColor = "red";
        document.getElementById("fsu_result").innerHTML = "Key cannnot be empty";
        return;
    }
    if (name == undefined) {
        name = this._portalCategory + '.' + document.getElementById('sel_FieldsSetUp').value + '.FieldsSetUp'
    }
    var fieldssetup = {};
    fieldssetup.Key = document.getElementById("fsu_key").value;
    fieldssetup.Name = name;
    fieldssetup.ApplicationId = document.getElementById("fsu_ID").value;
    fieldssetup.Description = document.getElementById("fsu_description").value;
    fieldssetup.User = this._user;

    if (!document.getElementById("fsu_json") || !document.getElementById("fsu_json").value)
        return;

    var inputJsonStr = document.getElementById("fsu_json").value;
    var errStr = jsonValidate(inputJsonStr);
    if (errStr) {
        jqAlert("Save Error", errStr);
        return;
    }
    else {
        try {
            fieldssetup.Fields = JSON.parse(document.getElementById("fsu_json").value);
        } catch (Exception) {
            jqAlert("Save Error", "The JSON document seems to be invalid.");
            return;
        }
    }

    var doneCallBack = function (response) {
        document.getElementById("fsu_result").style.color = "white";
        document.getElementById("fsu_result").style.backgroundColor = "green";
        if (response == true) {
            document.getElementById("fsu_result").innerHTML = "result updated";
            jqAlert("Update Successful", "Fields Setup has been saved successfully!");
        }
        else {
            document.getElementById("fsu_result").innerHTML = "result is not updated";
            jqAlert("Fiels Setup Error", "Error updating Fields Setup:" + JSON.stringify(response));
        }
    }
    var failCallBack = function (error) {
        document.getElementById("fsu_result").style.color = "white";
        document.getElementById("fsu_result").style.backgroundColor = "red";
        try {
            document.getElementById("fsu_result").innerHTML = "error: " + error.responseJSON.ExceptionMessage;
            jqAlert("Fields Setup Error", "Error updating Fields Setup: " + error.responseJSON.ExceptionMessage);
        } catch (Exception) {
            document.getElementById("fsu_result").innerHTML = "error: " + error.toString();
            jqAlert("Fields Setup Error", "Error updating Fields Setup: " + error.toString());
        }
    }
    var alwaysCallBack = function () { };
    getMaxToken(function (token) {
        meta.Token = token;
        meta.AddressBook = database;
        putFieldsSetUpApi(meta, fieldssetup, doneCallBack, failCallBack, alwaysCallBack);
    });
}

