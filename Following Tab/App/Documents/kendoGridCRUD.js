﻿
// add table row with given json data
function addRecord(json) {
    var grid = $("#app-grid").data("kendoGrid");
    var dataItem = grid.dataSource;
    dataItem.add({
        createdDate: json["DateTime1"],
        modifiedDate: json['DateTime2'],
        name: json['Text2'],
        path: json['Text3'],
        refId: json['Text1'],
        brokenFlag: json['Number1'],
        key: json['Key'],
        parent_key: json['ParentKey']
    });
}

// delete table row with selected row refId
function deleteDocument() {
    if (!$('#delete')[0].disabled) {
        if ($("#app-grid").data("kendoGrid").select()[0] != undefined) { // if kendo grid contain a selected row
            var refId = $("#app-grid").data("kendoGrid").select()[0].id;
            if ($("#app-grid").data("kendoGrid").select()[0].className.indexOf('brokenRow') != -1) { // it is broken link
                $("#dialog-confirm").show();
                $("#dialog-confirm").dialog("open");
            } else {    // not broken link
                checkLinks(refId).then(function (res) {
                    if (Object.keys(res['CustomChild']['Data']).length > 1) {
                        $("#dialog-confirm").show();
                        $("#dialog-confirm").dialog("open");
                    } else {
                        $("#delete-dialog-confirm").show();
                        $("#delete-dialog-confirm").dialog("open");
                    }
                })
            }
        } else {
            jqAlert('No document is selected', 'Please select a document first.');
        }
    }
}

function removeRecord() {
    var grid = $("#app-grid").data("kendoGrid");
    var selectedItem = grid.dataItem(grid.select());
    if (selectedItem != undefined) {
        var rowNum = grid.select().index() + 1;
        grid.removeRow("tr:eq(" + rowNum + ")");
    }
}
