﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Employee.mvc.AngularCulture
{
    public class AngularCultureAction : ActionResult
    {
        private readonly string _content;

        public AngularCultureAction()
        {
            _content = GetAngularScript();
        }
        public override void ExecuteResult(ControllerContext context)
        {
            context.HttpContext.Response.ContentType = "application/javascript";
            context.HttpContext.Response.Write(_content);
        }

        private string EN(string text)
        {
            return System.Web.HttpUtility.JavaScriptStringEncode(text);
        }
        private string UppercaseFirst(string s)
        {
	        if (string.IsNullOrEmpty(s))
	        {
	            return string.Empty;
	        }

	        return char.ToUpper(s[0]) + s.Substring(1);
        }
        private string numpattern(int pattern)
        {
            string strpattern = "(n)";
            switch (pattern)
            {
                case 1:
                    strpattern = "-n";
                    break;
                case 2:
                    strpattern = "- n";
                    break;
                case 3:
                    strpattern = "n-";
                    break;
                case 4:
                    strpattern = "n -";
                    break;
            }
            return strpattern;
        }

        private string percentPospattern(int pattern)
        {
            string strpattern = "n %";
            switch (pattern)
            {
                case 1:
                    strpattern = "n%";
                    break;
                case 2:
                    strpattern = "%n";
                    break;
                case 3:
                    strpattern = "% n";
                    break;
            }
            return strpattern;
        }

        private string percentNegpattern(int pattern)
        {
            string strpattern = "-n %";
            switch (pattern)
            {
                case 1:
                    strpattern = "-n%";
                    break;
                case 2:
                    strpattern = "-%n";
                    break;
                case 3:
                    strpattern = "%-n";
                    break;
                case 4:
                    strpattern = "%n-";
                    break;
                case 5:
                    strpattern = "n-%";
                    break;
                case 6:
                    strpattern = "n%-";
                    break;
                case 7:
                    strpattern = "-% n";
                    break;
                case 8:
                    strpattern = "n %-";
                    break;
                case 9:
                    strpattern = "% n-";
                    break;
                case 10:
                    strpattern = "% -n";
                    break;
                case 11:
                    strpattern = "n- %";
                    break;
                
            }
            return strpattern;
        }

        private string currencyPospattern(int pattern)
        {
            string strpattern = "$n";
            switch (pattern)
            {
                case 1:
                    strpattern = "n$";
                    break;
                case 2:
                    strpattern = "$ n";
                    break;
                case 3:
                    strpattern = "n $";
                    break;
            }
            return strpattern;
        }

        private string currencyNegpattern(int pattern)
        {
            string strpattern = "($n)";
            switch (pattern)
            {
                case 1:
                    strpattern = "-$n";
                    break;
                case 2:
                    strpattern = "$-n";
                    break;
                case 3:
                    strpattern = "$n-";
                    break;
                case 4:
                    strpattern = "(n$)";
                    break;
                case 5:
                    strpattern = "-n$";
                    break;
                case 6:
                    strpattern = "n-$";
                    break;
                case 7:
                    strpattern = "n$-";
                    break;
                case 8:
                    strpattern = "-n $";
                    break;
                case 9:
                    strpattern = "-$ n";
                    break;
                case 10:
                    strpattern = "n $-";
                    break;
                case 11:
                    strpattern = "$ n-";
                    break;
                case 12:
                    strpattern = "$ -n";
                    break;
                case 13:
                    strpattern = "n- $";
                    break;
                case 14:
                    strpattern = "($ n)";
                    break;
                case 15:
                    strpattern = "(n $)";
                    break;

            }
            return strpattern;
        }

        private string GetAngularScript()
        {
            System.Globalization.CultureInfo c = System.Globalization.CultureInfo.CurrentCulture;
            System.Globalization.DateTimeFormatInfo info = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat;

            string am = info.AMDesignator;
            string pm = info.PMDesignator;


            List<String> dn = new List<string>();
            List<String> dna = new List<string>();
            List<String> dns = new List<string>();

            for (int i = 0; i < c.DateTimeFormat.DayNames.Length; i++)
            {
                dn.Add(EN(c.DateTimeFormat.DayNames[i]));
                dna.Add(EN(c.DateTimeFormat.AbbreviatedDayNames[i]));
                dns.Add(EN(c.DateTimeFormat.ShortestDayNames[i]));
            }

            List<String> mn = new List<string>();
            List<String> mna = new List<string>();
            List<String> mns = new List<string>();

            for (int i = 0; i < c.DateTimeFormat.MonthNames.Length; i++)
            {
                mn.Add(EN(c.DateTimeFormat.MonthNames[i]));
                mna.Add(EN(c.DateTimeFormat.AbbreviatedMonthNames[i]));
            }

            return @"angular.module('ngLocale', [], ['$provide', function($provide) {
                    var PLURAL_CATEGORY = {ZERO: 'zero', ONE: 'one', TWO: 'two', FEW: 'few', MANY: 'many', OTHER: 'other'};
                    function getDecimals(n) {
                      n = n + '';
                      var i = n.indexOf('.');
                      return (i == -1) ? 0 : n.length - i - 1;
                    }

                    function getVF(n, opt_precision) {
                      var v = opt_precision;

                      if (undefined === v) {
                        v = Math.min(getDecimals(n), 3);
                      }

                      var base = Math.pow(10, v);
                      var f = ((n * base) | 0) % base;
                      return {v: v, f: f};
                    }

                    $provide.value('$locale', {
                      'DATETIME_FORMATS': {
                        'AMPMS': [
                          '"+ EN(am.Trim()) +"',"+
                          "'" + EN(pm.Trim()) + "'" +
                         "]," +
                        "'DAY': [" +
                          "'"+ dn[0] +"',"+
                          "'" +dn[1] +"',"+
                          "'" +dn[2] +"',"+
                          "'" +dn[3] +"',"+
                          "'" +dn[4] +"',"+
                          "'" +dn[5] +"',"+
                          "'" + dn[6] + "'" +
                        "],"+
                        "'ERANAMES': ["+
                          "'Before Christ',"+
                          "'Anno Domini'" +
                          "],"+
                        "'ERAS': ["+
                          "'BC'," +
                          "'AD'" +
                        "]," +
                        "'FIRSTDAYOFWEEK':" + ((int)c.DateTimeFormat.FirstDayOfWeek).ToString() + "," +
                        "'MONTH': [" +
                          "'" + mn[0] +"'," +
                          "'" + mn[1] +"'," +
                          "'" + mn[2] +"'," +
                          "'" + mn[3] +"'," +
                          "'" + mn[4] +"'," +
                          "'" + mn[5] +"'," +
                          "'" + mn[6] +"'," +
                          "'" + mn[7] +"'," +
                          "'" + mn[8] +"'," +
                          "'" + mn[9] +"'," +
                          "'" + mn[10] +"'," +
                          "'" + mn[11] +"'" +
                        "]," +
                        "'SHORTDAY': ["+
                          "'" + dna[0] + "'," +
                          "'" + dna[1] + "'," +
                          "'" + dna[2] + "'," +
                          "'" + dna[3] + "'," +
                          "'" + dna[4] + "'," +
                          "'" + dna[5] + "'," +
                          "'" + dna[6] + "'" +
                        "]," +
                        "'SHORTMONTH': [" +
                          "'" + mna[0]+"'," +
                          "'" + mna[1]+"'," +
                          "'" + mna[2]+"'," +
                          "'" + mna[3]+"'," +
                          "'" + mna[4]+"'," +
                          "'" + mna[5]+"'," +
                          "'" + mna[6]+"'," +
                          "'" + mna[7]+"'," +
                          "'" + mna[8]+"'," +
                          "'" + mna[9]+"'," +
                          "'" + mna[10]+"'," +
                          "'" + mna[11]+"'" +
                        "],"+
                        "'STANDALONEMONTH': [" +
                          "'" + UppercaseFirst(mn[0]) +"'," +
                          "'" + UppercaseFirst(mn[1]) +"'," +
                          "'" + UppercaseFirst(mn[2]) +"'," +
                          "'" + UppercaseFirst(mn[3]) +"'," +
                          "'" + UppercaseFirst(mn[4]) +"'," +
                          "'" + UppercaseFirst(mn[5]) +"'," +
                          "'" + UppercaseFirst(mn[6]) +"'," +
                          "'" + UppercaseFirst(mn[7]) +"'," +
                          "'" + UppercaseFirst(mn[8]) +"'," +
                          "'" + UppercaseFirst(mn[9]) +"'," +
                          "'" + UppercaseFirst(mn[10]) +"'," +
                          "'" + UppercaseFirst(mn[11]) +"'" +
                        "]," +
                        "'WEEKENDRANGE': [" +
                          "5," +
                          "6" +
                        "]," +
                        "'fullDate': '" + EN(c.DateTimeFormat.LongDatePattern.Replace("dddd", "EEE")) + " " + EN(c.DateTimeFormat.LongTimePattern.Replace("tt", "a")) + "'," +
                        "'longDate': '" + EN(c.DateTimeFormat.LongDatePattern.Replace("dddd", "EEE")) + "'," +
                        "'medium': '" + EN(c.DateTimeFormat.LongDatePattern.Replace("dddd", "EEE")) + " " + EN(c.DateTimeFormat.ShortTimePattern.Replace("tt", "a")) + "'," +
                        "'mediumDate': '" + EN(c.DateTimeFormat.LongDatePattern.Replace("dddd", "EEE")) + "'," +
                        "'mediumAbbMonthDate': '" + EN(c.DateTimeFormat.LongDatePattern.Replace("MMMM", "MMM").Replace("dddd", "EEE")) + "'," +
                        "'mediumTime': '" + EN(c.DateTimeFormat.LongTimePattern) + "'," +
                        "'short': '" + EN(c.DateTimeFormat.ShortDatePattern) + " " + EN(c.DateTimeFormat.ShortTimePattern.Replace("tt", "a")) + "'," +
                        "'shortDate': '" +  EN(c.DateTimeFormat.ShortDatePattern) + "'," +
                        "'shortTime': '" + EN(c.DateTimeFormat.ShortTimePattern.Replace("tt", "a")) + "'," +
                        "'timeless': '" + EN(c.DateTimeFormat.LongDatePattern.Replace("dddd", "EEE")) + "'," +
                        "'monthday': '" + EN(c.DateTimeFormat.ShortDatePattern.Replace("yyyy", "").TrimStart(c.DateTimeFormat.DateSeparator.ToCharArray()).TrimEnd(c.DateTimeFormat.DateSeparator.ToCharArray())) + "'," +
                      "}," +
                      "'NUMBER_FORMATS': { " +
                        "'CURRENCY_SYM': '" + EN(c.NumberFormat.CurrencySymbol)  + "',"+
                        "'DECIMAL_SEP': '"+ EN(c.NumberFormat.NumberDecimalSeparator)  + "'," +
                        "'GROUP_SEP': '"+ EN(c.NumberFormat.NumberGroupSeparator)  + "'," +
                        "'PATTERNS': [" +  
                              "{" +
                                "'gSize': 3," +
                                "'lgSize': 3," +
                                "'maxFrac': 3," +
                                "'minFrac': 0," +
                                "'minInt': 1," +
                                "'negPre': '-'," +
                                "'negSuf': ''," +
                                "'posPre': ''," +
                                "'posSuf': ''" +
                              "}," +                           
                              "{" +
                                "'gSize': 3," +
                                "'lgSize': 3," +
                                "'maxFrac': 2," +
                                "'minFrac': 2," +
                                "'minInt': 1," +
                                "'negPre': '-\u00a4'," +
                                "'negSuf': ''," +
                                "'posPre': '\u00a4'," +
                                "'posSuf': ''" +
                            "}" +
                        "]"+
                      "}," +
                      "'id':'" + EN(c.Name)  + "'," +
                      "'pluralCat': function(n, opt_precision) {  var i = n | 0;  var vf = getVF(n, opt_precision);  if (i == 1 && vf.v == 0) {    return PLURAL_CATEGORY.ONE;  }  return PLURAL_CATEGORY.OTHER;}" +
                      "});" + 
                    "}]);";
        }
    }
}
