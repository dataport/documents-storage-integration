﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace SharePoint.Class
{
    public class WebClient 
    {
        public static bool CreateDownLoadPDF(string url, string cotentType, Dictionary<string, string> headers, string ContentDisposition)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            bool IsSuccessed = false;
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

            request.Method = "GET";
            if (cotentType != null) { request.ContentType = cotentType; request.Accept = cotentType; }
            if (headers != null && headers.Count > 0) { foreach (var i in headers) { request.Headers.Add(i.Key, i.Value); } }
            try
            {
                HttpWebResponse httpWebResponse = (HttpWebResponse)request.GetResponse();
                HttpResponse Response = HttpContext.Current.Response;

                using (var webStream = httpWebResponse.GetResponseStream())
                {
                    if (webStream != null)
                    {
                        Response.Clear();
                        Response.Buffer = true;
                        Response.ContentType = cotentType;
                        ContentDisposition = GetContentDisposition(httpWebResponse.Headers["Content-Disposition"].ToString(), ContentDisposition);
                        Response.AddHeader("Content-Disposition", ContentDisposition);
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        webStream.CopyTo(Response.OutputStream);
                        Response.Flush();
                        Response.End();
                    }
                }
                httpWebResponse.Close();
                IsSuccessed = true;
            }
            catch(Exception e)
            {
				var err = e.StackTrace;
                IsSuccessed = false;
            }
            return IsSuccessed;
        }
		public static bool OpenFile(string url)
		{
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
			bool IsSuccessed = false;
			HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

			request.Method = "GET";

			try
			{
				HttpWebResponse httpWebResponse = (HttpWebResponse)request.GetResponse();
				HttpResponse Response = HttpContext.Current.Response;

				using (var webStream = httpWebResponse.GetResponseStream())
				{
					if (webStream != null)
					{
						Response.Clear();
						Response.Buffer = true;
						Response.Cache.SetCacheability(HttpCacheability.NoCache);
						webStream.CopyTo(Response.OutputStream);
						Response.Flush();
						Response.End();
					}
				}
				httpWebResponse.Close();
				IsSuccessed = true;
			}
			catch (Exception e)
			{
				var err = e.StackTrace;
				IsSuccessed = false;
			}
			return IsSuccessed;
		}
		public static string DownloadFile(string url, string cotentType, Dictionary<string, string> headers, string s)
		{
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
			string IsSuccessed = "error";
			HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

			request.Method = "GET";

			if (cotentType != null) { request.ContentType = cotentType; request.Accept = cotentType; }
			if (headers != null && headers.Count > 0) { foreach (var i in headers) { request.Headers.Add(i.Key, i.Value); } }
			try
			{
				HttpWebResponse httpWebResponse = (HttpWebResponse)request.GetResponse();
				HttpResponse Response = HttpContext.Current.Response;

				using (var webStream = httpWebResponse.GetResponseStream())
				{
					if (webStream != null)
					{
						Response.Clear();
						Response.Buffer = true;
						Response.ContentType = cotentType;
						Response.AddHeader("Content-Disposition", "attachment; filename=" + s);
						Response.Cache.SetCacheability(HttpCacheability.NoCache);
						webStream.CopyTo(Response.OutputStream);
						Response.Flush();
						Response.End();
					}
				}
				httpWebResponse.Close();
				IsSuccessed = "success";
			}
            catch (WebException ex)
            {
                JObject errorJobj = new JObject();
                using (var stream = ex.Response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    string errorMsg = reader.ReadToEnd();
                    errorJobj = JObject.Parse(errorMsg);
                }

                if (ex.Response is HttpWebResponse)
                {
                    HttpWebResponse response = (HttpWebResponse)ex.Response;
                    errorJobj.Add(new JProperty("StatusCode", response.StatusCode.ToString()));
                    //StatusCode : e.g. NotFound...
                }
                IsSuccessed = errorJobj.ToString();
            }
			catch (Exception e)
			{
                var err = e.StackTrace;
				IsSuccessed = e.Message;
			}
			return IsSuccessed;
		}

		public static string CreateGetHttpResponse(string url, string cotentType, Dictionary<string, string> headers)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            string sResponse = "";
            request.Method = "GET";
            if (cotentType != null)
            {
                request.ContentType = cotentType;
            }

            if (headers != null && headers.Count > 0)
            {
                foreach (var i in headers)
                {
                    request.Headers.Add(i.Key, i.Value);
                }
            }
            try
            {
                WebResponse response = request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                sResponse = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
                response.Close();
            }
            catch (WebException ex)    // e.g. 400, 404, 500
            {
                JObject errorJobj = new JObject();
                using (var stream = ex.Response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    string errorMsg = reader.ReadToEnd();
                    errorJobj = JObject.Parse(errorMsg);
                }

                if (ex.Response is HttpWebResponse)
                {
                    HttpWebResponse response = (HttpWebResponse)ex.Response;
                    errorJobj.Add(new JProperty("StatusCode", response.StatusCode.ToString()));
                    //StatusCode : e.g. NotFound...
                }
                sResponse = errorJobj.ToString();
            }
            catch (Exception ex)
            {
                sResponse = "Error: " + ex.Message;
            }
            return sResponse;
        }

        public static string CreatePostHttpResponse(string url, string cotentType, Dictionary<string, string> headers, string data)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            request.Method = "POST";
            if (cotentType != null)
            {
                request.ContentType = cotentType;
            }

            if (headers != null && headers.Count > 0)
            {
                foreach (var i in headers)
                {
                    request.Headers.Add(i.Key, i.Value);
                }
            }

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(data);
            }

            var response = (HttpWebResponse)request.GetResponse();
            using (var streamReader = new StreamReader(response.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                return result;
            }
        }

        public static JObject CreateDeleteHttpResponse(string url, string cotentType, Dictionary<string, string> headers)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            request.Method = "DELETE";
            if (cotentType != null)
            {
                request.ContentType = cotentType;
            }

            if (headers != null && headers.Count > 0)
            {
                foreach (var i in headers)
                {
                    request.Headers.Add(i.Key, i.Value);
                }
            }

            JObject responseJobj = new JObject();
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                // This will have statii from 200 to 30x
                responseJobj = new JObject(new JProperty("StatusCode", response.StatusCode));   // successful delete will only return "200" Status code
            }
            catch (WebException ex)    // e.g. 400, 404, 500
            {
                using (var stream = ex.Response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    string errorMsg = reader.ReadToEnd();
                    responseJobj = JObject.Parse(errorMsg);
                }

                if (ex.Response is HttpWebResponse)
                {
                    HttpWebResponse response = (HttpWebResponse)ex.Response;
                    responseJobj.Add(new JProperty("StatusCode", response.StatusCode.ToString()));
                    //StatusCode : e.g. NotFound...
                }
            }

            return responseJobj;
        }
        private static string GetContentDisposition(string HttpContentDisposition, string ContentDisposition)
        {
            string contentDisposition = ContentDisposition;
            string[] array = HttpContentDisposition.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (String.IsNullOrEmpty(ContentDisposition)){

                if (array.Length > 0)
                {
                    if (array.Length > 1)
                    {
                        string fileName = array[1].Trim();
                        fileName = fileName.Replace(" ", "_");

                        contentDisposition = array[0] + "; " + fileName;
                    }
                }
            }
            
            return contentDisposition;
        }

    }
}