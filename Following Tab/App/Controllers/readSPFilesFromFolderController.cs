﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SharePoint.Class;
using CloudElement.Models.DataObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using System.Web.Configuration;
namespace SharePoint.Controllers
{
    public class readSPFilesFromFolderController : ApiController
    {
        public Boolean readSPFilesFromFolder(string fileName, string folderPath, string subsite, string MxToken, string MxAPIURL)
        {
            var mapper = new SharePointMapper();
            Boolean isExist = false;
            string url = mapper.CloudElementURL + "/elements/api-v2/hubs/documents/folders/contents?path=" + folderPath;
            
            string user = mapper.GetUser();
            string uorg = mapper.GetOrg();

            MaxApiModel obj = new MaxApiModel();
            obj.MXAPIToken = MxToken;
            obj.MXAPIURL = MxAPIURL;
            ResponsDataWithOnRequest maxResponse = mapper.GetConfigureAndInstance(obj);
            JObject response = new JObject();
            if (maxResponse.Code != -2)
            {
                var configurationSetting = maxResponse.ConfigurationSetting.Data[0];
                var InstanceToken = configurationSetting.TextValue;
                
                JObject requestJObj = new JObject();
                try
                {
                    try
                    {
                        Dictionary<string, string> header = new Dictionary<string, string>();
                        header.Add("Authorization", "User " + user + ", " + "Organization " + uorg + ", " + "Element " + InstanceToken);
                        if (subsite != null && subsite != "")
                        {
                            header.Add("Subsite", subsite);
                        }
                        requestJObj.Add(new JProperty("url", url));
                        requestJObj.Add(new JProperty("header", JsonConvert.DeserializeObject<JObject>(JsonConvert.SerializeObject(header))));

                        var Team_Member_Id = "";
                        var cloudDocumentName = WebConfigurationManager.AppSettings["CloudDocumentName"];
                        if (cloudDocumentName.Equals("DROPBOXBIZ"))
                        {
                            Team_Member_Id = configurationSetting.Description;
                            header.Add("Elements-As-Team-Member", Team_Member_Id);
                        }

                        //For Dropbox Business, request should provide namespaceId and includeTeadSpace=true
                        if (WebConfigurationManager.AppSettings["CloudDocumentName"] == "DROPBOXBIZ")
                        {

                            string namsspaceUrl = mapper.CloudElementURL + "elements/api-v2/namespaces";
                            string nameSpaceResponse = WebClient.CreateGetHttpResponse(namsspaceUrl, "Content-Type: application/json", header);
                            JArray namespaceArray = JArray.Parse(nameSpaceResponse);
                            string teamSpaceId = "";
                            for (var i = 0; i < namespaceArray.Count; i++)
                            {
                                if (((string)namespaceArray[i]["namespace_type"][".tag"]) == "team_folder")
                                {
                                    teamSpaceId = (string)namespaceArray[i]["namespace_id"];
                                    break;
                                }
                            }
                            url += "&includeTeamSpaces=true&teamSpaceId=" + teamSpaceId;
                        }

                        string httpResponse = WebClient.CreateGetHttpResponse(url, "Content-Type: application/json", header);

                        try
                        {   // get error from CE
                            JObject jobj = JObject.Parse(httpResponse);
                            if (jobj["StatusCode"].ToString() == "NotFound")
                            {
                                // if folder is not found, it will be created in file upload 
                                isExist = false;
                            }
                            else
                            {
                                throw new System.Net.WebException(httpResponse);
                            }
                        }
                        catch (System.Net.WebException we) {
                            throw new Exception(httpResponse);
                        }
                        catch (Exception e) // get files from CE
                        {
                            JArray jary = JArray.Parse(httpResponse);
                            foreach (JObject file in jary)
                            {
                                if ((string)file["name"] == fileName) isExist = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            return isExist;
        }
    }
}