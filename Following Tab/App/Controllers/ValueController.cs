﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections;

namespace App.Controllers
{
    public class ValuesController : Controller
    {
        private ILoggerFactory _Factory;
        private ILogger _Logger;

        //set by dependency injection
        public ValuesController(ILoggerFactory factory, ILogger logger)
        {
            _Factory = factory;
            _Logger = logger;
        }
    }
}
