﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SharePoint.Class;
using CloudElement.Models.DataObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Configuration;

namespace SharePoint.Controllers
{
    public class removeSPDocController : ApiController
    {
        [HttpPost]
       public JObject removeSPDoc(string subsite, string docId, string MxToken, string MxAPIURL)
        {
            var mapper = new SharePointMapper();
            string url = mapper.CloudElementURL + "/elements/api-v2/hubs/documents/files/" + docId;
            string user = mapper.GetUser();
            string uorg = mapper.GetOrg();
            MaxApiModel obj = new MaxApiModel();
            obj.MXAPIToken = MxToken;
            obj.MXAPIURL = MxAPIURL;
            ResponsDataWithOnRequest maxResponse = mapper.GetConfigureAndInstance(obj);
            JObject response = new JObject();
            if (maxResponse.Code != -2)
            {
                var configurationSetting = maxResponse.ConfigurationSetting.Data[0];
                var InstanceToken = configurationSetting.TextValue;

                try
                {
                    Dictionary<string, string> header = new Dictionary<string, string>();
                    header.Add("Authorization", "User " + user + ", " + "Organization " + uorg + ", " + "Element " + InstanceToken);
                    if (subsite != null && subsite != "")
                    {
                        header.Add("Subsite", subsite);
                    }

                    var Team_Member_Id = "";
                    var cloudDocumentName = WebConfigurationManager.AppSettings["CloudDocumentName"];
                    if (cloudDocumentName.Equals("DROPBOXBIZ"))
                    {
                        Team_Member_Id = configurationSetting.Description;
                        header.Add("Elements-As-Team-Member", Team_Member_Id);
                    }

                    //For Dropbox Business, request should provide namespaceId and includeTeadSpace=true
                    if (WebConfigurationManager.AppSettings["CloudDocumentName"] == "DROPBOXBIZ")
                    {

                        string namsspaceUrl = mapper.CloudElementURL + "elements/api-v2/namespaces";
                        string nameSpaceResponse = WebClient.CreateGetHttpResponse(namsspaceUrl, "Content-Type: application/json", header);
                        JArray namespaceArray = JArray.Parse(nameSpaceResponse);
                        string teamSpaceId = "";
                        for (var i = 0; i < namespaceArray.Count; i++)
                        {
                            if (((string)namespaceArray[i]["namespace_type"][".tag"]) == "team_folder")
                            {
                                teamSpaceId = (string)namespaceArray[i]["namespace_id"];
                                break;
                            }
                        }
                        url += "?includeTeamSpaces=true&teamSpaceId=" + teamSpaceId;
                    }

                    response = WebClient.CreateDeleteHttpResponse(url, "Content-Type: application/json", header);
                }
                catch (Exception ex)
                {
                    response.Add(new JProperty("StatusCode", "Exception"));
                    response.Add(new JProperty("message", "Error: Exception: " + ex.Message));
                }
            }
            return response;
        }
    }
}