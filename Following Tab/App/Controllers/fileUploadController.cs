﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CloudElement.Models.DataObject;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Configuration;
namespace SharePoint.Controllers
{
    public class fileUploadController : ApiController
    {
        [HttpPost]
        public string fileUpload(string fileName, string path, string subsite, string MxToken, string MxAPIURL)
        {
            string result = "";
            //string fileName = "";
            long fileLength = -1;
            string fileContentType = "";

            // Create a Stream object.
            var requestData = HttpContext.Current.Request;
			
            ///fileName = "AAA.txt";

            System.IO.Stream str = requestData.InputStream;

            fileLength = str.Length;
            fileContentType = requestData.ContentType;

            byte[] resultByte = readByteFromFile(str);
            result = HttpPostRequest(null, resultByte, path, fileName, fileLength, fileContentType, subsite, MxToken, MxAPIURL);
            return result;
        }
        private byte[] readByteFromFile(System.IO.Stream str)
        {

            Int32 strLen, strRead;
            // Find number of bytes in stream.
            strLen = Convert.ToInt32(str.Length);
            // Create a byte array.
            byte[] byteArr = new byte[strLen];

            strRead = str.Read(byteArr, 0, strLen);
            return byteArr;
        }
        private string HttpPostRequest(MultipartFormDataContent formData, Byte[] form, string path, string fileName, long fileSize, string contentType, string subsite, string MxToken, string MxAPIURL)
        {
            //string queryString = "path=%2F" + fileName + "&size=" + fileSize.ToString();

            var mapper = new SharePointMapper();
            string httpUrl = mapper.CloudElementURL + "elements/api-v2/hubs/documents/files";
            string user = mapper.GetUser();
            string uorg = mapper.GetOrg();

            string iToken = "";

            MaxApiModel obj = new MaxApiModel();
            obj.MXAPIToken = MxToken;
            obj.MXAPIURL = MxAPIURL;
            ResponsDataWithOnRequest maxResponse = mapper.GetConfigureAndInstance(obj);
            if (maxResponse.Code != -2)
            {
                var configurationSetting = maxResponse.ConfigurationSetting.Data[0];
                iToken = configurationSetting.TextValue;
            }
            string parameter = "path=" + path + "%2F" + fileName + "&size=" + fileSize.ToString();
            httpUrl += "?" + parameter;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            
            if (WebConfigurationManager.AppSettings["CloudDocumentName"] == "DROPBOXBIZ")
            {

                string namsspaceUrl = mapper.CloudElementURL + "elements/api-v2/namespaces";

                Dictionary<string, string> header = new Dictionary<string, string>();
                header.Add("Authorization", "User " + user + ", Organization " + uorg + ", Element " + iToken);
                
                if (maxResponse.Code != -2)
                {
                    var configurationSetting = maxResponse.ConfigurationSetting.Data[0];
                    header.Add("Elements-As-Team-Member", configurationSetting.Description);    //add team_member_id into header
                }
                string nameSpaceResponse = SharePoint.Class.WebClient.CreateGetHttpResponse(namsspaceUrl, "Content-Type: application/json", header);
                JArray namespaceArray = JArray.Parse(nameSpaceResponse);
                string teamSpaceId = "";
                for (var i = 0; i < namespaceArray.Count; i++)
                {
                    if (((string)namespaceArray[i]["namespace_type"][".tag"]) == "team_folder")
                    {
                        teamSpaceId = (string)namespaceArray[i]["namespace_id"];
                        break;
                    }
                }
                httpUrl += "&includeTeamSpaces=true&teamSpaceId=" + teamSpaceId;
            }

            HttpWebRequest myHttpWebRequest = (HttpWebRequest)HttpWebRequest.Create(httpUrl);
			myHttpWebRequest.Timeout = 2000000;

			myHttpWebRequest.Headers = new WebHeaderCollection();

            myHttpWebRequest.Headers.Add("Authorization",
                    "User " + user + ", " +
                    "Organization " + uorg + ", " +
                    "Element " + iToken);
            if (subsite != null&&subsite!="")
                myHttpWebRequest.Headers.Add("Subsite",subsite);

            var Team_Member_Id = "";
            var cloudDocumentName = WebConfigurationManager.AppSettings["CloudDocumentName"];
            if (cloudDocumentName.Equals("DROPBOXBIZ"))
            {
                if (maxResponse.Code != -2)
                {
                    var configurationSetting = maxResponse.ConfigurationSetting.Data[0];
                    Team_Member_Id = configurationSetting.Description;
                    myHttpWebRequest.Headers.Add("Elements-As-Team-Member", Team_Member_Id);
                }
            }
            
            Stream request_stream = null;
            HttpWebResponse myHttpWebResponse = null;
            Stream responsestream = null;
            StreamReader mystreamreader = null;
            try
            {
                string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");
                byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");

                myHttpWebRequest.ContentType = "multipart/form-data; boundary=" + boundary;
                myHttpWebRequest.MediaType = "application/json";
                myHttpWebRequest.Accept = "application/json";
                myHttpWebRequest.Method = "POST";

                //write stream
                string headerTemplate = "Content-Disposition: form-data; name=\"file\"; filename=\"" + fileName + "\"\r\nContent-Type: "+ contentType + "\r\n\r\n";
                byte[] headerBytes = System.Text.Encoding.UTF8.GetBytes(headerTemplate);
                byte[] trailer = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");

                request_stream = myHttpWebRequest.GetRequestStream();
                request_stream.Write(boundarybytes, 0, boundarybytes.Length);
                request_stream.Write(headerBytes, 0, headerBytes.Length);
                request_stream.Write(form, 0, form.Length);
                request_stream.Write(trailer, 0, trailer.Length);

                myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
                responsestream = myHttpWebResponse.GetResponseStream();
                mystreamreader = new StreamReader(responsestream, Encoding.Default);
                string pagecontent = mystreamreader.ReadToEnd();
                

                request_stream.Close();
                myHttpWebResponse.Close();
                mystreamreader.Close();
                responsestream.Close();

                JObject jobj = JsonConvert.DeserializeObject<JObject>(pagecontent);
                //return jobj["id"].ToString();
                if (jobj != null && jobj["id"] != null)
                {
                    return jobj["id"].ToString();
                    //return true;
                    //return new JObject(new JProperty("status","success"),new JProperty("response",jobj));
                }else
                {
                    return "Error:"+jobj.ToString();
                    //return new JObject(new JProperty("status", "failure"), new JProperty("response", jobj));
                }
            }
            catch (Exception ex)
            {
                string errMsg = "Error:" + ex.Message;
                return errMsg;
                //return new JObject(new JProperty("status", "failure"), new JProperty("response", errMsg));
            }
            return "Error:";
        }
    }

}
