﻿using Newtonsoft.Json.Linq;
using SharePoint.Class;
using CloudElement.Models.DataObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Configuration;

namespace SharePoint.Controllers
{
    public class readRefIdByIdController : ApiController
    {
        [HttpPost]
        public JObject readRefIdById(string id, string subsite, string MxToken, string MxAPIURL)
        {
            id = id.Replace("\\\"", "");
            id = id.Trim(new Char[] { '\\', '\"' });


            var mapper = new SharePointMapper();
            string url = mapper.CloudElementURL + "elements/api-v2/hubs/documents/files/" + id + "/metadata";
            
            JObject metadata = new JObject();
            string user = mapper.GetUser();
            string org = mapper.GetOrg();

            MaxApiModel maxApiModel = new MaxApiModel();
            maxApiModel.MXAPIToken = MxToken;
            maxApiModel.MXAPIURL = MxAPIURL;
            var instanceResponse = mapper.GetConfigureAndInstance(maxApiModel);

            if (instanceResponse.Code == 0) {
                string instance = instanceResponse.ConfigurationSetting.Data[0].TextValue;

                Dictionary<string, string> header = new Dictionary<string, string>();
                header.Add("Authorization", "User " + user + ", Organization "+ org +", Element " + instance);
                if (subsite != null && subsite != "") header.Add("Subsite", subsite);

                var Team_Member_Id = "";
                var cloudDocumentName = WebConfigurationManager.AppSettings["CloudDocumentName"];
                if (cloudDocumentName.Equals("DROPBOXBIZ"))
                {
                    Team_Member_Id = instanceResponse.ConfigurationSetting.Data[0].Description;
                    header.Add("Elements-As-Team-Member", Team_Member_Id);
                }

                try
                {
                    //For Dropbox Business, request should provide namespaceId and includeTeadSpace=true
                    if (WebConfigurationManager.AppSettings["CloudDocumentName"] == "DROPBOXBIZ")
                    {

                        string namsspaceUrl = mapper.CloudElementURL + "elements/api-v2/namespaces";
                        string nameSpaceResponse = WebClient.CreateGetHttpResponse(namsspaceUrl, "Content-Type: application/json", header);
                        JArray namespaceArray = JArray.Parse(nameSpaceResponse);
                        string teamSpaceId = "";
                        for (var i = 0; i < namespaceArray.Count; i++)
                        {
                            if (((string) namespaceArray[i]["namespace_type"][".tag"]) == "team_folder")
                            {
                                teamSpaceId = (string) namespaceArray[i]["namespace_id"];
                                break;
                            }
                        }
                        url += "?includeTeamSpaces=true&teamSpaceId=" + teamSpaceId;
                    }else if (WebConfigurationManager.AppSettings["CloudDocumentName"] == "DROPBOXBIZ")
                    {

                    }

                    //send http request 
                    string jsonStrResponse = WebClient.CreateGetHttpResponse(url, "Content-Type: application/json", header);
                    metadata = JObject.Parse(jsonStrResponse);
                }
                catch(Exception ex)
                {
                    metadata.Add("StatusCode", "Exception");
                    metadata.Add("message", ex.Message + ";" + ex.StackTrace);
                }
            }

            return metadata;
        }
    }
}