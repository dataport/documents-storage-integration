﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using System.Configuration;
namespace SharePoint.Controllers
{
    public class getVersionNumberController : ApiController
    {
        [HttpGet]
        public String getVersionNumber()
        {
            string versionNumber = "Value of version number is null.";
            if (ConfigurationManager.AppSettings["VersionNumber"] != null)
            {
                versionNumber = ConfigurationManager.AppSettings["VersionNumber"] as string;
            }
            return versionNumber;
        }
    }
}