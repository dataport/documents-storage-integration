﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SharePoint.Class;
using CloudElement.Models.DataObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Configuration;

namespace SharePoint.Controllers
{
    public class retrieveSPFilesFromFolderController : ApiController
    {
        public JArray retrieveSPFilesFromFolder(string folderPath, string subsite, string MxToken, string MxAPIURL)
        {
            var mapper = new SharePointMapper();
            //string url = mapper.CloudElmenURL + "/elements/api-v2/hubs/documents/folders/contents?path=" + folderPath;
            string user = mapper.GetUser();
            string uorg = mapper.GetOrg();

            MaxApiModel obj = new MaxApiModel();
            obj.MXAPIToken = MxToken;
            obj.MXAPIURL = MxAPIURL;
            ResponsDataWithOnRequest maxResponse = mapper.GetConfigureAndInstance(obj);
            JObject response = new JObject();
            JArray returnAry = new JArray();
            bool errorFree = true; //
            if (maxResponse.Code != -2)
            {
                var configurationSetting = maxResponse.ConfigurationSetting.Data[0];
                var InstanceToken = configurationSetting.TextValue;

                
                try
                {
                    var lastSlashIndex = folderPath.Length;
                    do
                    {
                        string httpResponse = "";
                        errorFree = true;   // not inaccessible folder
                        string url = "";
                        folderPath = folderPath.Substring(0, lastSlashIndex);

                        if (folderPath == "") url = mapper.CloudElementURL + "/elements/api-v2/hubs/documents/folders/contents?path=/";    // root path
                        else url = mapper.CloudElementURL + "/elements/api-v2/hubs/documents/folders/contents?path=" + folderPath;
                        try
                        {
                            Dictionary<string, string> header = new Dictionary<string, string>();
                            header.Add("Authorization", "User " + user + ", " + "Organization " + uorg + ", " + "Element " + InstanceToken);
                            if (subsite != null && subsite != "")
                            {
                                header.Add("Subsite", subsite);
                            }

                            var Team_Member_Id = "";
                            var cloudDocumentName = WebConfigurationManager.AppSettings["CloudDocumentName"];
                            if (cloudDocumentName.Equals("DROPBOXBIZ"))
                            {
                                Team_Member_Id = configurationSetting.Description;
                                header.Add("Elements-As-Team-Member", Team_Member_Id);
                            }

                            //For Dropbox Business, request should provide namespaceId and includeTeadSpace=true
                            if (WebConfigurationManager.AppSettings["CloudDocumentName"] == "DROPBOXBIZ")
                            {

                                string namsspaceUrl = mapper.CloudElementURL + "elements/api-v2/namespaces";
                                string nameSpaceResponse = WebClient.CreateGetHttpResponse(namsspaceUrl, "Content-Type: application/json", header);
                                JArray namespaceArray = JArray.Parse(nameSpaceResponse);
                                string teamSpaceId = "";
                                for (var i = 0; i < namespaceArray.Count; i++)
                                {
                                    if (((string)namespaceArray[i]["namespace_type"][".tag"]) == "team_folder")
                                    {
                                        teamSpaceId = (string)namespaceArray[i]["namespace_id"];
                                        break;
                                    }
                                }
                                url += "&includeTeamSpaces=true&teamSpaceId=" + teamSpaceId;
                            }

                            JObject requestJObj = new JObject();
                            requestJObj.Add(new JProperty("url", url));
                            requestJObj.Add(new JProperty("header", JsonConvert.DeserializeObject<JObject>(JsonConvert.SerializeObject(header))));

                            httpResponse = WebClient.CreateGetHttpResponse(url, "Content-Type: application/json", header);
                            returnAry = JArray.Parse(httpResponse); 
                            // cause exception if httpResponse is error received from CE because it is a JObject
                        }
                        catch (JsonReaderException je) {    // only handle error received from CE
                            // parse httpResponse to JObject then add it into a JArray
                            JObject errorObj = JObject.Parse(httpResponse);
                            errorFree = false;
                            if (lastSlashIndex == 0)
                            {  // root must exist, if it still contains error, then something is wrong
                                string responseString = @"[{}]";
                                returnAry = JArray.Parse(responseString);
                                JObject item = (JObject)returnAry[0];
                                item.Add(new JProperty("error", errorObj["message"]));
                                item.Add(new JProperty("statusCode", errorObj["StatusCode"]));
                            }
                        }
                        catch (Exception ex)
                        {
                            errorFree = false;
                            if (lastSlashIndex == 0)
                            {  // root must exist, if it still contains error, then something is wrong
                                string responseString = @"[{}]";
                                returnAry = JArray.Parse(responseString);
                                JObject item = (JObject)returnAry[0];
                                item.Add(new JProperty("error", "Error: " + ex.Message));
                            }
                        }

                        lastSlashIndex = folderPath.LastIndexOf("/");
                    } while (lastSlashIndex >= 0 && !errorFree);

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + ";" + ex.StackTrace);
                }
            }
            if (returnAry.Count <= 0) { // when there are nothing in the folder
                string responseString = @"[{}]";
                JArray jary = JArray.Parse(responseString);
                JObject item = (JObject)jary[0];
                item.Add(new JProperty("path", folderPath+"/"));
                return jary;
            }else return returnAry;
        }
    }
}