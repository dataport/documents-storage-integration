﻿using CloudElement.Models.DataObject;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using SharePoint.Class;

namespace SharePoint.Controllers
{
    public class getInstancePropertyController : ApiController
    {
        //Get element instance parameter
        //for AMAZON
        [HttpGet]
        public JObject getInstanceProperty(string InstanceID, string MxToken, string MxAPIURL)
        {

            var mapper = new SharePointMapper();
            string url = mapper.CloudElementURL + "elements/api-v2/instances/" + InstanceID;

            JObject metadata = new JObject();
            string user = mapper.GetUser();
            string org = mapper.GetOrg();

                Dictionary<string, string> header = new Dictionary<string, string>();
                header.Add("Authorization", "User " + user + ", Organization " + org );
                //if (subsite != null && subsite != "") header.Add("Subsite", subsite);

                try
                {
                    //send http request 
                    string jsonStrResponse = WebClient.CreateGetHttpResponse(url, "Content-Type: application/json", header);
                    metadata = JObject.Parse(jsonStrResponse);
                    //hide OAuth data
                    if (metadata != null && metadata["configuration"] != null)
                    {
                        if (metadata["configuration"]["filemanagement.provider.access_key"] != null)
                        {
                            metadata["configuration"]["filemanagement.provider.access_key"].Parent.Remove();
                        }
                        if (metadata["configuration"]["filemanagement.provider.secret_key"] != null)
                        {
                            metadata["configuration"]["filemanagement.provider.secret_key"].Parent.Remove();
                    }
                    metadata = metadata["configuration"] as JObject;
                }
                }
                catch (Exception ex)
                {
                    metadata.Add("StatusCode", "Exception");
                    metadata.Add("message", ex.Message + ";" + ex.StackTrace);
                }
            

            return metadata;
        }
    }
}
