﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using CloudElement.Models.DataObject;
using Newtonsoft.Json.Linq;
using SharePoint.Class;

namespace SharePoint.Controllers
{
    public class SharePointController : Controller
    { 
        // GET: /SharePoint/Index
        public ActionResult Index()
        {
            var mapper = new SharePointMapper();
            var model = new DataObjectModel();
            model = mapper.Load(model);
            return View(model);
        }

		// POST : /SharePoint/GetSPData/
		[OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        [HttpPost]
        public JObject GetSPData(string MxObj)
          {
            var mapper = new SharePointMapper();
            var obj = JsonConvert.DeserializeObject<MaxApiModel>(MxObj);
            var response = mapper.GetDocuments(obj);
            return response;
        }

        // POST : /SharePoint/GetConfigAddress/
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        [HttpPost]
        public JObject GetConfigAddress(string MxObj)
        {
            var mapper = new SharePointMapper();
            var obj = JsonConvert.DeserializeObject<MaxApiModel>(MxObj);
            var response = mapper.GetConfiguration(obj);
            return response;
        }

        // POST : /SharePoint/DownloadFile/
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        [HttpPost]
        public JObject DownloadFile(string MxObj)
        {
            var mapper = new SharePointMapper();
            var obj = JsonConvert.DeserializeObject<MaxApiModel>(MxObj);
            var response = mapper.DownloadSP(obj);
            return response;
        }
        // POST : /SharePoint/getMember/
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        [HttpPost]
        public JObject getMember(string MxObj)
        {
            ResponsDataWithOnRequest response;
            JObject result = null;
            String teamMemberID = null;
            String InstanceToken = null;
            SharePointMapper mapper;
            mapper = new SharePointMapper();
            try
            {
                var obj = JsonConvert.DeserializeObject<MaxApiModel>(MxObj);
                response = mapper.GetConfigureAndInstance(obj);
                if (response != null && response.ConfigurationSetting != null && response.ConfigurationSetting.Data!=null &&
                    response.ConfigurationSetting.Data.Count!=0)
                {
                    teamMemberID = response.ConfigurationSetting.Data[0].Description;
                    InstanceToken = response.ConfigurationSetting.Data[0].TextValue;
                }
            }
            catch (Exception ex)
            {
                result.Add("StatusCode", "Exception");
                result.Add("message", ex.Message + ";" + ex.StackTrace);
            }
            string url = mapper.CloudElementURL + "elements/api-v2/members";
            if (teamMemberID != null)
            {
                teamMemberID = teamMemberID.Replace(":", "%3A");
                url += "?where=team_member_id%3D'" + teamMemberID + "'";
            }
            JArray memberdata = new JArray();
            string user = mapper.GetUser();
            string org = mapper.GetOrg();

            Dictionary<string, string> header = new Dictionary<string, string>();
            header.Add("Authorization", "User " + user + ", Organization " + org + ", " + "Element " + InstanceToken);
            //if (subsite != null && subsite != "") header.Add("Subsite", subsite);

            try
            {
                //send http request 
                string jsonStrResponse = WebClient.CreateGetHttpResponse(url, "Content-Type: application/json", header);
                memberdata = JArray.Parse(jsonStrResponse);
                //hide OAuth data
                if (memberdata != null && memberdata.Count != 0)
                {
                    result = (JObject)memberdata.First();
                    result.Add("StatusCode", "OK");
                }
            }
            catch (Exception ex)
            {
                result.Add("StatusCode", "Exception");
                result.Add("message", ex.Message + ";" + ex.StackTrace);
            }

            return result;
        }
    }
}
