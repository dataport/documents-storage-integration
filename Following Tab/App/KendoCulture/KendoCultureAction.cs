﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace App.KendoCulture
{
    public class KendoCultureAction: ActionResult
    {
        private readonly string _content;

        public KendoCultureAction(string Culture)
        {
            _content = GetKendoScript(Culture);
        }
        public override void ExecuteResult(ControllerContext context)
        {
            context.HttpContext.Response.ContentType = "application/javascript";
            context.HttpContext.Response.Write(_content);
        }

        private string EN(string text)
        {
            return System.Web.HttpUtility.JavaScriptStringEncode(text);
        }

        private string numpattern(int pattern)
        {
            string strpattern = "(n)";
            switch (pattern)
            {
                case 1:
                    strpattern = "-n";
                    break;
                case 2:
                    strpattern = "- n";
                    break;
                case 3:
                    strpattern = "n-";
                    break;
                case 4:
                    strpattern = "n -";
                    break;
            }
            return strpattern;
        }

        private string percentPospattern(int pattern)
        {
            string strpattern = "n %";
            switch (pattern)
            {
                case 1:
                    strpattern = "n%";
                    break;
                case 2:
                    strpattern = "%n";
                    break;
                case 3:
                    strpattern = "% n";
                    break;
            }
            return strpattern;
        }

        private string percentNegpattern(int pattern)
        {
            string strpattern = "-n %";
            switch (pattern)
            {
                case 1:
                    strpattern = "-n%";
                    break;
                case 2:
                    strpattern = "-%n";
                    break;
                case 3:
                    strpattern = "%-n";
                    break;
                case 4:
                    strpattern = "%n-";
                    break;
                case 5:
                    strpattern = "n-%";
                    break;
                case 6:
                    strpattern = "n%-";
                    break;
                case 7:
                    strpattern = "-% n";
                    break;
                case 8:
                    strpattern = "n %-";
                    break;
                case 9:
                    strpattern = "% n-";
                    break;
                case 10:
                    strpattern = "% -n";
                    break;
                case 11:
                    strpattern = "n- %";
                    break;

            }
            return strpattern;
        }

        private string currencyPospattern(int pattern)
        {
            string strpattern = "$n";
            switch (pattern)
            {
                case 1:
                    strpattern = "n$";
                    break;
                case 2:
                    strpattern = "$ n";
                    break;
                case 3:
                    strpattern = "n $";
                    break;
            }
            return strpattern;
        }

        private string currencyNegpattern(int pattern)
        {
            string strpattern = "($n)";
            switch (pattern)
            {
                case 1:
                    strpattern = "-$n";
                    break;
                case 2:
                    strpattern = "$-n";
                    break;
                case 3:
                    strpattern = "$n-";
                    break;
                case 4:
                    strpattern = "(n$)";
                    break;
                case 5:
                    strpattern = "-n$";
                    break;
                case 6:
                    strpattern = "n-$";
                    break;
                case 7:
                    strpattern = "n$-";
                    break;
                case 8:
                    strpattern = "-n $";
                    break;
                case 9:
                    strpattern = "-$ n";
                    break;
                case 10:
                    strpattern = "n $-";
                    break;
                case 11:
                    strpattern = "$ n-";
                    break;
                case 12:
                    strpattern = "$ -n";
                    break;
                case 13:
                    strpattern = "n- $";
                    break;
                case 14:
                    strpattern = "($ n)";
                    break;
                case 15:
                    strpattern = "(n $)";
                    break;

            }
            return strpattern;
        }

        private string GetKendoScript(string Culture)
        {
            string culturevalue = Culture;
            System.Globalization.CultureInfo c = System.Threading.Thread.CurrentThread.CurrentCulture;
            if (!String.IsNullOrEmpty(culturevalue))
            {
                c = new System.Globalization.CultureInfo(culturevalue);
            }
            string am = c.DateTimeFormat.AMDesignator;
            string pm = c.DateTimeFormat.PMDesignator;

            List<String> dn = new List<string>();
            List<String> dna = new List<string>();
            List<String> dns = new List<string>();

            for (int i = 0; i < c.DateTimeFormat.DayNames.Length; i++)
            {
                dn.Add(EN(c.DateTimeFormat.DayNames[i]));
                dna.Add(EN(c.DateTimeFormat.AbbreviatedDayNames[i]));
                dns.Add(EN(c.DateTimeFormat.ShortestDayNames[i]));
            }

            List<String> mn = new List<string>();
            List<String> mna = new List<string>();

            for (int i = 0; i < c.DateTimeFormat.MonthNames.Length; i++)
            {
                mn.Add(EN(c.DateTimeFormat.MonthNames[i]));
                mna.Add(EN(c.DateTimeFormat.AbbreviatedMonthNames[i]));
            }

            return @"
kendo.cultures['en-US'].numberFormat = {
            pattern: ['" + numpattern(c.NumberFormat.NumberNegativePattern) + @"'],
            decimals: " + EN(c.NumberFormat.NumberDecimalDigits.ToString()) + @",
            '.': '" + EN(c.NumberFormat.NumberDecimalSeparator) + @"',
            ',': '" + EN(c.NumberFormat.NumberGroupSeparator) + @"',
            groupSize: [" + EN(c.NumberFormat.NumberGroupSizes[0].ToString()) + @"],
            percent: {
                pattern: ['" + percentNegpattern(c.NumberFormat.PercentNegativePattern) + "', '" + percentPospattern(c.NumberFormat.PercentPositivePattern) + @"'],
                decimals:" + EN(c.NumberFormat.PercentDecimalDigits.ToString()) + @",
                '.': '" + EN(c.NumberFormat.PercentDecimalSeparator) + @"',
                ',': '" + EN(c.NumberFormat.PercentGroupSeparator) + @"',
                groupSize: [" + EN(c.NumberFormat.PercentGroupSizes[0].ToString()) + @"],
                symbol: '" + EN(c.NumberFormat.PercentSymbol) + @"'
            },
            currency: {
                pattern: ['" + currencyNegpattern(c.NumberFormat.CurrencyNegativePattern) + "', '" + currencyPospattern(c.NumberFormat.CurrencyPositivePattern) + @"'],
                decimals: " + EN(c.NumberFormat.CurrencyDecimalDigits.ToString()) + @",
                '.': '" + EN(c.NumberFormat.CurrencyDecimalSeparator) + @"',
                ',': '" + EN(c.NumberFormat.CurrencyGroupSeparator) + @"',
                groupSize: [" + EN(c.NumberFormat.CurrencyGroupSizes[0].ToString()) + @"],
                symbol: '" + EN(c.NumberFormat.CurrencySymbol) + @"'
            }
},
kendo.cultures['en-US'].calendars = {
    standard:{
        days:{
            names:[
                '" + dn[0] + @"',
                '" + dn[1] + @"',
                '" + dn[2] + @"',
                '" + dn[3] + @"',
                '" + dn[4] + @"',
                '" + dn[5] + @"',
                '" + dn[6] + @"'
            ],
            namesAbbr:[
                '" + dna[0] + @"',
                '" + dna[1] + @"',
                '" + dna[2] + @"',
                '" + dna[3] + @"',
                '" + dna[4] + @"',
                '" + dna[5] + @"',
                '" + dna[6] + @"'
            ],
            namesShort:[
                '" + dns[0] + @"',
                '" + dns[1] + @"',
                '" + dns[2] + @"',
                '" + dns[3] + @"',
                '" + dns[4] + @"',
                '" + dns[5] + @"',
                '" + dns[6] + @"'
            ]
        },
        months:{
            names:[
                '" + mn[0] + @"',
                '" + mn[1] + @"',
                '" + mn[2] + @"',
                '" + mn[3] + @"',
                '" + mn[4] + @"',
                '" + mn[5] + @"',
                '" + mn[6] + @"',
                '" + mn[7] + @"',
                '" + mn[8] + @"',
                '" + mn[9] + @"',
                '" + mn[10] + @"',
                '" + mn[11] + @"',
            ''],
            namesAbbr:[
                '" + mna[0] + @"',
                '" + mna[1] + @"',
                '" + mna[2] + @"',
                '" + mna[3] + @"',
                '" + mna[4] + @"',
                '" + mna[5] + @"',
                '" + mna[6] + @"',
                '" + mna[7] + @"',
                '" + mna[8] + @"',
                '" + mna[9] + @"',
                '" + mna[10] + @"',
                '" + mna[11] + @"',
            '']
        },
        AM:['" + EN(am) + @"','" + EN(am.ToLowerInvariant()) + @"','" + EN(am.ToUpperInvariant()) + @"'],
        PM:['" + EN(pm) + @"','" + EN(pm.ToLowerInvariant()) + @"','" + EN(pm.ToUpperInvariant()) + @"'],
        patterns:{
            d:'" + EN(c.DateTimeFormat.ShortDatePattern) + @"',
            D:'" + EN(c.DateTimeFormat.LongDatePattern) + @"',
            F:'" + EN(c.DateTimeFormat.FullDateTimePattern) + @"',
            s:'" + EN(c.DateTimeFormat.SortableDateTimePattern) + @"',
            t:'" + EN(c.DateTimeFormat.ShortTimePattern) + @"',
            T:'" + EN(c.DateTimeFormat.LongTimePattern) + @"',
            u:'" + EN(c.DateTimeFormat.UniversalSortableDateTimePattern) + @"',
            y:'" + EN(c.DateTimeFormat.YearMonthPattern) + @"',
            Y:'" + EN(c.DateTimeFormat.YearMonthPattern) + @"',
            m:'" + EN(c.DateTimeFormat.MonthDayPattern) + @"',
            M:'" + EN(c.DateTimeFormat.MonthDayPattern) + @"',
            G:'" + EN(c.DateTimeFormat.ShortDatePattern) + " " + EN(c.DateTimeFormat.LongTimePattern) + @"',
            g:'" + EN(c.DateTimeFormat.ShortDatePattern) + " " + EN(c.DateTimeFormat.ShortTimePattern) + @"'
        },
        '/':'" + EN(c.DateTimeFormat.DateSeparator) + @"',
        ':':'" + EN(c.DateTimeFormat.TimeSeparator) + @"',
        firstDay:" + ((int)c.DateTimeFormat.FirstDayOfWeek).ToString() + @"
    }
}";
        }
    }
}