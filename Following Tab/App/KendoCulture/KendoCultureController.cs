﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace App.KendoCulture
{
    public class KendoCultureController : Controller
    {
        // GET api/kendoculture
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index(string Culture)
        {
            return new KendoCultureAction(Culture);

        }
    }
}
